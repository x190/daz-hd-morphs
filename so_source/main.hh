#ifndef __MAIN_H__
#define __MAIN_H__

#include "shared.hh"

extern "C" {
    //---------------------- VDISP ----------------------------
    int generate_disp_morphs( const MeshInfo* mesh_info,
                                         const MorphsInfo* morphs_info,
                                         const TextureInfo* texture_info,
                                         const short morph_base_until );

    int generate_disp_blender( const MeshInfo* mesh_info,
                                          const TextureInfo* texture_info,
                                          const char* base_name );

    //------------------------- SHAPE ----------------------------
    int generate_shapes( const MeshInfo* mesh_info,
                                    const MorphsInfo* morphed_info,
                                    const MorphsInfo* morphs_info,
                                    const BaseShapeKeysInfo* base_sks_info,
                                    const char* output_dirpath );

    int generate_hd_mesh( const MeshInfo* mesh_info,
                                     const MorphsInfo* morphs_info,
                                     const char* output_dirpath,
                                     const char* output_filename );
}
#endif // __MAIN_H__
