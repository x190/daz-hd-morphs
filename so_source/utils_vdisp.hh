#ifndef UTILS_VDISP_H_INCLUDED
#define UTILS_VDISP_H_INCLUDED

#include "mesh.hh"


class SubdivisionVdisp
{
public:
    SubdivisionVdisp(const bool create_subdividers, const dhdm::Mesh * baseMeshPointer);

    void subdivide_simple(const unsigned int level, dhdm::Mesh & mesh);

    void subdivide( const unsigned int level, dhdm::Mesh & mesh,
                    std::vector<dhdm::DhdmFile> & dhdms,
                    const int apply_edits_until );

private:
    bool create_subdividers;
    const dhdm::Mesh * baseMeshPointer;
    std::unordered_map< unsigned int, std::unique_ptr<dhdm::MeshSubdivider> > subdividers;
};

#endif // UTILS_VDISP_H_INCLUDED
