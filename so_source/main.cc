#include <iostream>
#include <fstream>
#include <future>
#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include "main.hh"
#include "diff.hh"
#include "utils_vdisp.hh"
#include "utils.hh"

#define SHAPES_USE_OBJ 1


//---------------------- VDISP ----------------------------
int generate_disp_morphs( const MeshInfo* mesh_info,
                                     const MorphsInfo* morphs_info,
                                     const TextureInfo* texture_info,
                                     const short morph_base_until )
{
    try{
        const std::string outputDirpath( texture_info->output_dirpath );
        const std::string fp_filelist = outputDirpath + "/files_list.txt";
        std::ofstream f_fileslist( fp_filelist, std::ofstream::out | std::ofstream::trunc );
        if ( !f_fileslist )
            throw std::runtime_error("Can't open file \"" + fp_filelist + "\"");

        dhdm::gScale = mesh_info->gScale;
        const std::string fp_dae = std::string(mesh_info->base_exportedf) + ".dae";
        const std::string fp_obj = std::string(mesh_info->base_exportedf) + ".obj";

        dhdm::Mesh loadedBaseMesh = dhdm::Mesh::fromDae( fp_dae.c_str(),
                                                         fp_obj.c_str(),
                                                         texture_info->uv_layer,
                                                         false,
                                                         false,
                                                         mesh_info->geograft_file );

        std::set<unsigned short> selected_tiles_set;
        load_tiles_set(selected_tiles_set, texture_info);

        SubdivisionVdisp sbd(morphs_info->groups_n > 1, &loadedBaseMesh);
        std::vector<std::future<void>> asyncs;

        MeshDiff::renderContextInit( texture_info->image_type,
                                     texture_info->texture_size,
                                     texture_info->texture_margin );

        for (unsigned int i=0; i < morphs_info->groups_n; i++) {
            std::cout << "\n--- New morphs group:\n";
            std::vector<MorphFileInfo> morphs_group_info;
            load_morphs_group_info(morphs_group_info, morphs_info, i);

            dhdm::Mesh baseMesh;
            dhdm::Mesh hdMesh = loadedBaseMesh;
            std::vector<dhdm::DhdmFile> dhdms;
            unsigned int level = applyBaseMorphs(hdMesh, morphs_group_info, dhdms);

            if (texture_info->max_subd > 0)
                level = std::min(level, (unsigned int) texture_info->max_subd);

            if (morph_base_until < 0) {
                baseMesh = loadedBaseMesh;
                sbd.subdivide_simple(level, baseMesh);
            }
            else {
                baseMesh = hdMesh;
                if (morph_base_until > 0)
                    sbd.subdivide(level, baseMesh, dhdms, morph_base_until);
                else
                    sbd.subdivide_simple(level, baseMesh);
            }
            sbd.subdivide(level, hdMesh, dhdms, -1);

            std::cout << "Triangulating...\n";
            baseMesh.triangulate();
            hdMesh.triangulate();

            MeshDiff diff( texture_info, baseMesh, hdMesh,
                           morphs_info->groups_basenames[i] );
            diff.writeToImage( outputDirpath, selected_tiles_set, asyncs, f_fileslist );
        }

        MeshDiff::renderContextFinish();

        std::cout << "Waiting...\n";
        for (auto & async : asyncs)
            async.get();
        f_fileslist.close();
        return 0;
    } catch (std::exception & e) {
        std::cout << "-Error in DLL: " << e.what() << std::endl;
        return -1;
    }
}


//------------------------- SHAPE ------------------------------
int generate_shapes( const MeshInfo* mesh_info,
                                const MorphsInfo* morphed_info,
                                const MorphsInfo* morphs_info,
                                const BaseShapeKeysInfo* base_sks_info,
                                const char* output_dirpath )
{
    try{
        dhdm::gScale = mesh_info->gScale;
        const std::string fp_dae = std::string(mesh_info->base_exportedf) + ".dae";
        const std::string fp_obj = std::string(mesh_info->base_exportedf) + ".obj";
        const std::string fp_obj_s = std::string(output_dirpath) + "/shape-";
        const std::string fp_obj_sk_s = std::string(output_dirpath) + "/base_shape_";

        dhdm::Mesh loadedBaseMesh = dhdm::Mesh::fromDae( fp_dae.c_str(),
                                                         fp_obj.c_str(),
                                                         mesh_info->load_uv_layers,
                                                         false,
                                                         false,
                                                         mesh_info->geograft_file );

        const size_t tot_shapes = morphs_info->groups_n + base_sks_info->n;
        std::unique_ptr<dhdm::MeshSubdivider> msd;
        if ( tot_shapes > 1 )
            msd.reset( new dhdm::MeshSubdivider( &loadedBaseMesh, mesh_info->hd_level ) );

        std::vector<dhdm::Vertex> base_morphs_delta;
        if ((morphed_info != nullptr) && (morphed_info->groups_n > 0))
        {
            std::vector<MorphFileInfo> morphs_group_info;
            load_morphs_group_info(morphs_group_info, morphed_info, 0);

            dhdm::Mesh baseMeshUnmorphed = loadedBaseMesh;
            dhdm::Mesh baseMeshMorphed = loadedBaseMesh;

            std::vector<dhdm::DhdmFile> dhdms;
            applyBaseMorphs( baseMeshMorphed, morphs_group_info, dhdms );

            if (msd)
            {
                msd->subdivide_simple(baseMeshUnmorphed);
                msd->subdivide(baseMeshMorphed, dhdms, -1);
            }
            else
            {
                baseMeshUnmorphed.subdivide_simple(mesh_info->hd_level);
                baseMeshMorphed.subdivide(mesh_info->hd_level, dhdms, -1);
            }
            base_morphs_delta = get_vertices_delta(baseMeshUnmorphed, baseMeshMorphed);
        }

        for (unsigned int i=0; i < base_sks_info->n; i++) {
            std::cout << "\n--- New non-daz shape key:\n";
            const std::string fp = fp_obj_sk_s + std::to_string(i) + ".obj";
            dhdm::Mesh baseShapeMesh = dhdm::Mesh::fromObj( fp.c_str(), false, false, nullptr );
            if (msd)
                msd->subdivide_simple(baseShapeMesh);
            else
                baseShapeMesh.subdivide_simple(mesh_info->hd_level);

            if (base_morphs_delta.size() > 0)
                baseShapeMesh.add_vertices_delta(base_morphs_delta);

            baseShapeMesh.writeObj(fp);
        }

        for (unsigned int i=0; i < morphs_info->groups_n; i++) {
            std::cout << "\n--- New morphs group:\n";

            std::vector<MorphFileInfo> morphs_group_info;
            load_morphs_group_info(morphs_group_info, morphs_info, i);

            dhdm::Mesh hdMesh = loadedBaseMesh;
            std::vector<dhdm::DhdmFile> dhdms;
            applyBaseMorphs( hdMesh, morphs_group_info, dhdms );

            if (msd)
                msd->subdivide(hdMesh, dhdms, -1);
            else
                hdMesh.subdivide(mesh_info->hd_level, dhdms, -1);

            if (base_morphs_delta.size() > 0)
                hdMesh.add_vertices_delta(base_morphs_delta);

            #if SHAPES_USE_OBJ > 0
            const std::string fp = fp_obj_s + std::to_string(i) + ".obj";
            hdMesh.writeObj(fp);

            #else
            const std::string fp = fp_obj_s + std::to_string(i) + ".dae";
            const std::string name = std::string("shape") + std::to_string(i);
            hdMesh.writeCollada(fp, name);

            #endif // SHAPES_USE_OBJ
        }

        return 0;
    } catch (std::exception & e) {
        std::cout << "-Error in DLL: " << e.what() << std::endl;
        return -1;
    }

}


int generate_hd_mesh( const MeshInfo* mesh_info,
                                 const MorphsInfo* morphs_info,
                                 const char* output_dirpath,
                                 const char* output_filename )
{
    try{
        dhdm::gScale = mesh_info->gScale;
        const std::string fp_dae = std::string(mesh_info->base_exportedf) + ".dae";
        const std::string fp_obj = std::string(mesh_info->base_exportedf) + ".obj";
        dhdm::Mesh loadedBaseMesh = dhdm::Mesh::fromDae( fp_dae.c_str(),
                                                         fp_obj.c_str(),
                                                         0,
                                                         true,
                                                         true,
                                                         mesh_info->geograft_file );

        applySubdivision(loadedBaseMesh, morphs_info, mesh_info->hd_level);

        const std::string filename(output_filename);
        const std::string filepath( std::string(output_dirpath) + "/" + filename + ".dae" );
        loadedBaseMesh.writeCollada( filepath, filename );
        return 0;

    } catch (std::exception & e) {
        std::cout << "-Error in DLL: " << e.what() << std::endl;
        return -1;
    }
}


//--------------------------BLENDER VDISP-----------------------------
int generate_disp_blender( const MeshInfo* mesh_info,
                          const TextureInfo* texture_info,
                          const char* base_name )
{
    try{
        const std::string outputDirpath(texture_info->output_dirpath);
        const std::string fp_filelist = outputDirpath + "/files_list.txt";
        std::ofstream f_fileslist( fp_filelist, std::ofstream::out | std::ofstream::trunc );
        if ( !f_fileslist )
            throw std::runtime_error("Can't open file \"" + fp_filelist + "\"");

        dhdm::gScale = 1;
        const std::string fp_obj_base = std::string(mesh_info->base_exportedf) + ".obj";
        const std::string fp_obj_hd = std::string(mesh_info->base_exportedf) + "_2.obj";

        dhdm::Mesh baseMesh = dhdm::Mesh::fromObj( fp_obj_base,
                                                  true,
                                                  false,
                                                  nullptr );

        dhdm::Mesh hdMesh = dhdm::Mesh::fromObj( fp_obj_hd,
                                                false,
                                                false,
                                                nullptr );

        if ( mesh_info->hd_level > 0 )
            baseMesh.subdivide_simple( mesh_info->hd_level );

        if ( baseMesh.vertices.size() != hdMesh.vertices.size() )
        {
            std::cout << "ERROR: subdivided base mesh and hd mesh don't match.\n";
            return -1;
        }

        std::set<unsigned short> selected_tiles_set;
        load_tiles_set(selected_tiles_set, texture_info);

        std::vector<std::future<void>> asyncs;

        std::cout << "Triangulating...\n";
        baseMesh.triangulate();
        hdMesh.triangulate();

        MeshDiff::renderContextInit( texture_info->image_type,
                                    texture_info->texture_size,
                                    texture_info->texture_margin );

        MeshDiff diff( texture_info, baseMesh, hdMesh,
                      base_name );

        diff.writeToImage( outputDirpath, selected_tiles_set, asyncs, f_fileslist );
        MeshDiff::renderContextFinish();

        std::cout << "Waiting...\n";
        for (auto & async : asyncs)
            async.get();
        f_fileslist.close();
        return 0;
    } catch (std::exception & e) {
        std::cout << "-Error in DLL: " << e.what() << std::endl;
        return -1;
    }
}

