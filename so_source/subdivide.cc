#include <iostream>
#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include "mesh.hh"
#include "dhdm.hh"


using namespace OpenSubdiv;


void dhdm::getDhdmMats( const Mesh * mesh,
                        std::vector< std::vector<glm::dmat3x3> > & mats,
                        std::vector<int> & firstLevelSubFaceOffset )
{
    assert( mats.size() == 0 && firstLevelSubFaceOffset.size() == 0);
    int subFaceOffset = 0;

    for (auto & face : mesh->faces) {
        const int num_face_verts = face.vertices.size();
        if (num_face_verts < 3)
        {
            throw std::runtime_error(
                fmt::format("Error: face with {} vertices is not supported.", num_face_verts) );
        }

        firstLevelSubFaceOffset.push_back(subFaceOffset);
        subFaceOffset += num_face_verts;

        std::vector< glm::dvec3 > face_coords;
        for (int i = 0; i < num_face_verts; i++)
            face_coords.push_back( mesh->vertices[face.vertices[i].vertex].pos );

        /*
        glm::dvec3 x_axis = glm::normalize( face_coords[3] - face_coords[0] );
        glm::dvec3 z_axis = glm::normalize( face_coords[1] - face_coords[0] );
        glm::dvec3 y_axis = -glm::normalize( glm::cross(x_axis, z_axis) );
        */

        std::vector<glm::dmat3x3> face_mats;
        glm::dvec3 z_axis = glm::normalize( glm::cross( face_coords[1]-face_coords[0],
                                                        face_coords[num_face_verts-1]-face_coords[0] ) );
        glm::dvec3 x_axis;
        glm::dvec3 y_axis;

        for (int i = 0; i < num_face_verts; i++)
        {
            const int prev = (i > 0) ? (i - 1) : (num_face_verts-1);
            x_axis = glm::normalize( face_coords[ prev ] - face_coords[ i ] );
            y_axis = glm::normalize( glm::cross(z_axis, x_axis) );
            face_mats.push_back( glm::dmat3x3(x_axis, z_axis, -y_axis) );
        }

        mats.push_back(std::move(face_mats));
    }
}


/*
*    "apply_edits_until" is inclusive, only levels > "apply_edits_until"
*     will be considered for dhdm details.
*     If -1, all available levels will be considered.
*/
void dhdm::Mesh::subdivide( const unsigned int level, std::vector<dhdm::DhdmFile> & dhdms,
                            const int apply_edits_until )
{
    if (dhdms.size() == 0 || apply_edits_until == 0) {
        subdivide_simple(level);
        return;
    }

    if (level == 0) return;
    std::cout << fmt::format("Subdividing to level {}...\n", level);

    std::vector< std::vector<glm::dmat3x3> > mats;
    std::vector<int> firstLevelSubFaceOffset;
    const dhdm::Mesh * mats_mesh = subd_only_deltas ? originalMesh : this;
    dhdm::getDhdmMats( mats_mesh, mats, firstLevelSubFaceOffset );

    std::unique_ptr<Far::TopologyRefiner> refiner( dhdm::createTopologyRefiner( level, *this ) );

    Far::StencilTableFactory::Options stencilOptions;
    stencilOptions.generateIntermediateLevels = true;
    stencilOptions.factorizeIntermediateLevels = false;
    stencilOptions.generateOffsets = true;
    std::unique_ptr<const Far::StencilTable> vertexStencil(
                Far::StencilTableFactory::Create(*refiner, stencilOptions));

    Far::PrimvarRefiner primvarRefiner(*refiner);

    subdivide_nonvertex(level, refiner, primvarRefiner);

    std::vector<dhdm::Vertex> vbuffer( refiner->GetNumVerticesTotal() - vertices.size() );
    dhdm::Vertex * srcVerts = vertices.data();

    int vert_start = 0, vert_end = 0;
    for (unsigned int lvl = 1; lvl <= level; ++lvl)
    {
        const bool should_apply_edits = (apply_edits_until < 0 || (int)lvl <= apply_edits_until);
        if (should_apply_edits)
            std::cout << fmt::format("  Applying edits on level {}...\n", lvl);

        auto curLevel = refiner->GetLevel(lvl);
        if (lvl > 1)
            srcVerts = vbuffer.data() + vert_start;

        vert_start = vert_end;
        vert_end += curLevel.GetNumVertices();

        vertexStencil->UpdateValues( srcVerts, vbuffer.data(), vert_start, vert_end );

        if ( should_apply_edits ) {
            // const uint32_t nrSubfaces = curLevel.GetNumFaces();
            const int subFaceOffsetFactor = ( 1 << ( 2 * (lvl-1) ) );

            for (auto & dhdm_file : dhdms) {
                auto & edits = *(dhdm_file.dhdm_data);

                if (lvl <= edits.levels.size()) {
                    for (const auto & displ : edits.levels[lvl - 1]) {
                        uint32_t displ_faceIdx = displ.faceIdx;
                        if (dhdm_file.geo_data != nullptr)
                        {
                            if ( (dhdm_file.geo_data)->count(displ_faceIdx) > 0 )
                                displ_faceIdx = (*(dhdm_file.geo_data))[displ_faceIdx];
                            else
                                continue;
                        }

                        auto currLevel_faceIdx = firstLevelSubFaceOffset[displ_faceIdx] * subFaceOffsetFactor + displ.subfaceIdx;
                        // assert(currLevel_faceIdx < nrSubfaces);
                        auto fverts = curLevel.GetFaceVertices(currLevel_faceIdx);
                        // assert(displ.vertexIdx < fverts.size());
                        int vertexIdx = fverts[displ.vertexIdx] + vert_start;
                        // assert(vertexIdx < (int) vbuffer.size());
                        const uint32_t submat_idx = uint32_t( displ.subfaceIdx / subFaceOffsetFactor );
                        vbuffer[vertexIdx].pos += dhdm_file.weight * mats[displ_faceIdx][submat_idx] * glm::dvec3(displ.x, displ.y, displ.z);
                    }
                }
            }
        }

    }

    vertices.clear();
    const size_t firstOfLastVertices = vbuffer.size() - refiner->GetLevel(level).GetNumVertices();
    for (size_t i = firstOfLastVertices; i < vbuffer.size(); i++)
        vertices.push_back( std::move(vbuffer[i]) );

}
