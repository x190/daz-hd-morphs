---------------------------------------------------
Modified and expanded from: https://github.com/edolstra/dhdm

---------------------------
Originally compiled with Qt Creator on Ubuntu 22.04.

Sources:

    Compiled libraries:
        glew32
            http://glew.sourceforge.net

        glfw3
            https://www.glfw.org

        libpng
            http://www.libpng.org/pub/png/libpng.html

        osdCPU
            https://graphics.pixar.com/opensubdiv/docs/cmake_build.html

        The iostreams part of boost:
            https://www.boost.org/doc/libs/1_75_0/more/getting_started/index.html

        System libraries

    Headers only libraries:
        https://github.com/fmtlib/fmt
        https://github.com/g-truc/glm
        https://github.com/nlohmann/json
        https://github.com/syoyo/tinyexr

---------------------------
