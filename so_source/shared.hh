#ifndef SHARED_H_INCLUDED
#define SHARED_H_INCLUDED

#include <tuple>
#include <unordered_map>

extern "C" {

struct MeshInfo
{
    float gScale;
    char* base_exportedf;
    char* geograft_file;
    unsigned short hd_level;
    short load_uv_layers;
};

struct BaseShapeKeysInfo
{
    unsigned short n;
};

struct MorphsInfo
{
    unsigned short groups_n;

    unsigned short* groups_counts;
    char*** morphs_filepaths;
    float** morphs_weights;
    char*** morphs_types;

    char** groups_basenames;
};

struct TextureInfo
{
    char target_space;
    unsigned short texture_size;
    unsigned short texture_margin;
    char image_type;
    bool auto_settings;
    float scale;
    float midlevel;
    unsigned short tiles_count;
    unsigned short* tiles;
    short uv_layer;
    short max_subd;

    char* output_dirpath;
};

}   // extern C

struct MorphFileInfo
{
    double weight;
    std::string filepath;
    bool use_base;
    bool use_hd;
};


namespace hash_tuple
{
    template <typename T>
    struct hash_f
    {
        size_t operator()(T const& v) const
        {
            return std::hash<T>()(v);
        }
    };

    namespace
    {
        template <class T>
        inline void hash_combine(size_t& seed, T const& v) {
            seed ^= hash_tuple::hash_f<T>()(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
        }

        template <class Tuple, size_t Index = std::tuple_size<Tuple>::value - 1>
        struct HashValueImpl
        {
          static void apply(size_t& seed, Tuple const& tup)
          {
            HashValueImpl<Tuple, Index-1>::apply(seed, tup);
            hash_combine(seed, std::get<Index>(tup));
          }
        };

        template <class Tuple>
        struct HashValueImpl<Tuple, 0>
        {
          static void apply(size_t& seed, Tuple const& tup)
          {
            hash_combine(seed, std::get<0>(tup));
          }
        };
    }

    template <typename ... Ts>
    struct hash_f<std::tuple<Ts...>>
    {
        size_t operator()(std::tuple<Ts...> const& tup) const
        {
            size_t seed = 0;
            HashValueImpl<std::tuple<Ts...> >::apply(seed, tup);
            return seed;
        }
    };
}

using FaceTuple = std::tuple<unsigned int, unsigned int, unsigned int, unsigned int>;
using FaceMap = std::unordered_map< FaceTuple, unsigned int, hash_tuple::hash_f<FaceTuple> >;

#endif // SHARED_H_INCLUDED
