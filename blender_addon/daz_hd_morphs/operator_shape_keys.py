import os, time
from . import dll_wrapper
from . import utils
from .operator_common import HDMorphOperator


class GenerateHDOperator(HDMorphOperator):
    """Generate HD mesh (with daz's vertex order) from base mesh"""
    bl_idname = "dazhdmorph.generatehd"
    bl_label = "Generate HD mesh"

    use_obj = True
    hd_level = None
    generate_mode = None
    base_modifiers = None
    use_morph_list = False
    import_base_sks = False
    base_sks_force_blender = False
    copy_non_hd_meshes = False

    def execute(self, context):
        if not self.check_input(context):
            return {'CANCELLED'}

        addon_props = context.scene.daz_hd_morph
        self.generate_mode = addon_props.generate_mode
        self.hd_level = addon_props.hd_level
        self.base_modifiers = addon_props.base_modifiers
        self.use_morph_list = addon_props.use_morph_list
        self.copy_non_hd_meshes = addon_props.copy_non_hd_meshes

        if self.use_morph_list:
            if not self.check_morphs_list(context, group_selector=True, can_be_empty=False):
                return {'CANCELLED'}

        t0 = time.perf_counter()
        if self.generate_mode == 'RIGGED':
            self.import_base_sks = addon_props.import_base_sks
            self.base_sks_force_blender = addon_props.base_sks_force_blender
        else:
            self.import_base_sks = False
            self.base_sks_force_blender = False
        r = self.generate_hd_mesh(context)
        self.cleanup(context)
        if not r:
            return {'CANCELLED'}

        self.report({'INFO'}, "HD mesh generated.")
        print("Elapsed: {}".format(time.perf_counter() - t0))
        return {'FINISHED'}

    def generate_hd_mesh(self, context):
        include_weights = False
        if self.generate_mode in ('RIGGED', 'MULTIRES'):
            self.base_modifiers = 'NONE'
        if self.generate_mode == 'RIGGED':
            include_weights = True

        base_exportedf = self.export_base_dae( context, self.base_modifiers,
                                               include_weights=include_weights,
                                               with_obj=True, uv_layer=-1 )

        outputDirpath = self.create_temporary_subdir()
        outputFilename = "{0}-div{1}".format(utils.makeValidFilename(self.base_ob.name), self.hd_level)

        dll_wrapper.execute_in_new_thread( "generate_hd_mesh",
                                            self.gScale, base_exportedf,
                                            self.hd_level, self.geograft_file,
                                            self.morphs_groups,
                                            outputDirpath, outputFilename )

        hd_ob = utils.import_dll_collada(os.path.join(outputDirpath, outputFilename + ".dae"))
        utils.move_to_collection(hd_ob, [self.hd_collection_name])

        self.cleanup_base_copy()
        utils.copy_uv_layers_names(self.base_ob, hd_ob)
        utils.merge_materials_by_slot(self.base_ob, hd_ob)

        # ~ context.view_layer.depsgraph.update()
        if self.generate_mode == 'MULTIRES':
            utils.create_unsubdivide_multires(hd_ob)
            utils.copy_vertex_groups(self.base_ob, hd_ob)
        if self.generate_mode in ('RIGGED', 'MULTIRES'):
            utils.copy_armature( self.base_ob, hd_ob,
                                 collection_name=self.hd_collection_name,
                                 move_to_top=True,
                                 copy_non_hd_meshes=self.copy_non_hd_meshes )
        elif self.copy_non_hd_meshes:
            utils.copy_non_hd_meshes( self.base_ob, hd_ob,
                                      collection_name=self.hd_collection_name )
        if self.generate_mode == 'RIGGED':
            utils.fix_vertex_groups_spaces(self.base_ob, hd_ob)

        utils.make_final_active(hd_ob)
        self.import_shape_keys(context, hd_ob, base_exportedf, outputDirpath)
        return True

    def import_shape_keys(self, context, hd_ob, base_exportedf, outputDirpath):
        if not self.import_base_sks:
            return True

        morphs_dirs = utils.api_get_morphs_directories(self.base_ob)
        if morphs_dirs is None:
            self.report({'ERROR'}, "Required addon \"import_daz\" not found, or api function threw an exception.")
            return False

        base_sks_names, base_sks_files, base_sks_objs = utils.get_base_sks_info(self.base_ob, morphs_dirs,
                                                                                base_sks_force_blender=self.base_sks_force_blender)
        total_new = len(base_sks_files) + len(base_sks_objs)
        if total_new == 0:
            print("Found no base shape keys.")
            return True
        print("Found {0} base shape keys. Importing them onto the generated HD mesh...".format(total_new))

        obj_sks = {}
        sk_pin = utils.ShapeKeysPin(self.base_ob)
        for i, sk_name in enumerate(base_sks_objs):
            sk_pin.pin_sk(sk_name)
            obj_name = "base_shape_{0}".format(i)
            obj_fp = self.export_base_obj(context, base_modifiers='SK',
                                          uv_layer=None, obj_name=obj_name)
            obj_sks[sk_name] = obj_fp
        sk_pin.restore()
        del sk_pin

        morphs_groups = {}
        for n, sk_id in enumerate(base_sks_files):
            morphs_groups[n] = [ (base_sks_files[sk_id], 1, 'BOTH') ]

        dll_wrapper.execute_in_new_thread( "generate_shapes",
                                           self.gScale, base_exportedf, -1,
                                           self.hd_level, self.geograft_file,
                                           morphs_groups, self.morphs_groups, obj_sks,
                                           outputDirpath )

        fp_pref = outputDirpath + "/base_shape_"
        for i, sk_name in enumerate(base_sks_objs):
            fp = fp_pref + str(i) + ".obj"
            if os.path.isfile(fp):
                utils.shapekey_from_obj( fp, hd_ob,
                                         sk_name=sk_name )

        fp_pref = outputDirpath + "/shape-"
        for i, sk_id in enumerate(base_sks_files):
            if self.use_obj:
                fp = fp_pref + str(i) + ".obj"
                if os.path.isfile(fp):
                    utils.shapekey_from_obj( fp, hd_ob,
                                             sk_name=sk_id )
            else:
                fp = fp_pref + str(i) + ".dae"
                if os.path.isfile(fp):
                    utils.shapekey_from_collada( fp, hd_ob,
                                                 sk_name=sk_id )

        all_sks_names = {**base_sks_names, **base_sks_objs}
        print("Setting up drivers...")
        utils.copy_sk_drivers(self.base_ob, hd_ob, all_sks_names)
        print("Done setting up drivers.")
        return True

class GenerateHDSKOperator(HDMorphOperator):
    bl_description = "Generate morph/s shape key/s on HD mesh (which must have daz's vertex order)"
    bl_idname = "dazhdmorph.hdwithsk"
    bl_label = "Generate shape key/s on HD mesh"

    use_obj = True
    from_morphs = None
    base_modifiers = None
    hd_level = None

    def execute(self, context):
        if not self.check_input(context, check_hd=True):
            return {'CANCELLED'}

        addon_props = context.scene.daz_hd_morph
        self.sk_source = addon_props.sk_source
        self.base_modifiers = addon_props.base_modifiers
        self.copy_base_drivers = addon_props.copy_base_drivers
        self.base_sks_force_blender = addon_props.base_sks_force_blender

        if self.sk_source == 'MORPHS':
            if not self.check_morphs_list(context):
                return {'CANCELLED'}

        t0 = time.perf_counter()
        self.hd_level = utils.get_subdivision_level(self.base_ob, self.hd_ob)
        if self.hd_level < 0:
            self.report({'ERROR'}, "Base mesh is more subdivided than hd mesh.")
            return {'CANCELLED'}

        r = self.generate_hd_sk(context)
        self.cleanup(context)
        if not r:
            return {'CANCELLED'}

        self.report({'INFO'}, "HD shape key(s) generated.")
        print("Elapsed: {}".format(time.perf_counter() - t0))
        return {'FINISHED'}

    def generate_hd_sk(self, context):
        morphs_groups_names = None
        obj_sks = None
        if self.sk_source == 'BASE_SHAPE':
            name = "base_shape-div{0}".format(self.hd_level)
            self.morphs_groups, morphs_groups_names = self.create_empty_morph_group(name)
        elif self.sk_source == 'MORPHS':
            morphs_groups_names = utils.getMorphsGroupsNames( self.morphs_groups )

            hd_sks_names = set( utils.get_sk_names(self.hd_ob) )
            to_delete = set()
            for g, morph_list in self.morphs_groups.items():
                if (len(morph_list) == 1) and (morphs_groups_names[g] in hd_sks_names):
                    to_delete.add(g)
            for g in to_delete:
                del self.morphs_groups[g]
                del morphs_groups_names[g]

            if len(self.morphs_groups) == 0:
                self.report({'WARNING'}, "Found no new morph groups.")
                return False

        else: # self.sk_source == 'BASE_MORPHS'
            morphs_dirs = utils.api_get_morphs_directories(self.base_ob)
            if morphs_dirs is None:
                self.report({'ERROR'}, "Required addon \"import_daz\" not found, or api function threw an exception.")
                return False
            _, base_sks_files, base_sks_objs = utils.get_base_sks_info(self.base_ob, morphs_dirs,
                                                                       base_sks_force_blender=self.base_sks_force_blender)

            hd_sks_names = set( utils.get_sk_names(self.hd_ob) )

            to_delete = set()
            for sk_name in base_sks_files:
                if sk_name in hd_sks_names:
                    to_delete.add(sk_name)
            for sk_name in to_delete:
                del base_sks_files[sk_name]

            to_delete = set()
            for sk_name in base_sks_objs:
                if sk_name in hd_sks_names:
                    to_delete.add(sk_name)
            for sk_name in to_delete:
                del base_sks_objs[sk_name]
            del to_delete, hd_sks_names

            total_new = len(base_sks_files) + len(base_sks_objs)
            if total_new == 0:
                self.report({'WARNING'}, "Found no new base shape keys.")
                return False

            print("Found {0} new base shape keys. Importing them onto the generated HD mesh...".format(total_new))
            self.morphs_groups = {}
            morphs_groups_names = {}
            for n, sk_id in enumerate(base_sks_files):
                self.morphs_groups[n] = [ (base_sks_files[sk_id], 1, 'BOTH') ]
                morphs_groups_names[n] = sk_id

            obj_sks = {}
            sk_pin = utils.ShapeKeysPin(self.base_ob)
            for i, sk_name in enumerate(base_sks_objs):
                sk_pin.pin_sk(sk_name)
                obj_name = "base_shape_{0}".format(i)
                obj_fp = self.export_base_obj(context, base_modifiers='SK',
                                              uv_layer=None, obj_name=obj_name)
                obj_sks[sk_name] = obj_fp
            sk_pin.restore()
            del sk_pin

        outputDirpath = self.create_temporary_subdir()
        base_exportedf = self.export_base_dae( context, self.base_modifiers,
                                               include_weights=False,
                                               with_obj=True, uv_layer=None )

        dll_wrapper.execute_in_new_thread( "generate_shapes",
                                           self.gScale, base_exportedf, -1,
                                           self.hd_level, self.geograft_file,
                                           self.morphs_groups, None, obj_sks,
                                           outputDirpath )

        if obj_sks is not None:
            fp_pref = outputDirpath + "/base_shape_"
            for i, sk_name in enumerate(obj_sks):
                fp = fp_pref + str(i) + ".obj"
                if os.path.isfile(fp):
                    utils.shapekey_from_obj( fp, self.hd_ob,
                                             sk_name=sk_name )

        fp_pref = outputDirpath + "/shape-"
        i = 0
        for g, morph_list in self.morphs_groups.items():
            sk_name = morphs_groups_names[g]

            if self.use_obj:
                fp = fp_pref + str(i) + ".obj"
                if os.path.isfile(fp):
                    utils.shapekey_from_obj( fp, self.hd_ob,
                                             sk_name=sk_name )
            else:
                fp = fp_pref + str(i) + ".dae"
                if os.path.isfile(fp):
                    utils.shapekey_from_collada( fp, self.hd_ob,
                                                 sk_name=sk_name )
            i += 1

        self.cleanup_base_copy()
        if self.sk_source != 'BASE_SHAPE' and self.copy_base_drivers != 'NO_DRIVERS':
            if self.copy_base_drivers == 'ARMATURE_DRIVERS':
                if utils.recopy_armature( self.base_ob, self.hd_ob ):
                    print("Copied armature.")
                else:
                    print("Armature not copied.")
            exclude_driven = (self.copy_base_drivers == 'ONLY_DRIVERS')
            sks_pairs = utils.get_sk_pairs(self.base_ob, self.hd_ob, exclude_driven)
            if len(sks_pairs) > 0:
                print("Found {0} matching shape keys in base mesh.".format(len(sks_pairs)))
                print("Setting up drivers...")
                utils.copy_sk_drivers(self.base_ob, self.hd_ob, sks_pairs)
                print("Done setting up drivers.")
            else:
                print("No matching shape keys found in base mesh.")
        return True


class CreateHDDrivers(HDMorphOperator):
    """Create drivers onto HD mesh from matching shape keys of the base mesh"""
    bl_idname = "dazhdmorph.createdrivers"
    bl_label = "Create drivers on HD mesh"

    def execute(self, context):
        if not self.check_input(context, check_hd=True):
            return {'CANCELLED'}
        addon_props = context.scene.daz_hd_morph
        self.copy_armature = addon_props.copy_armature

        t0 = time.perf_counter()
        r = self.create_drivers(context)
        self.cleanup(context)
        if not r:
            return {'CANCELLED'}

        self.report({'INFO'}, "Drivers created.")
        print("Elapsed: {}".format(time.perf_counter() - t0))
        return {'FINISHED'}

    def create_drivers(self, context):
        if self.copy_armature:
            if utils.recopy_armature( self.base_ob, self.hd_ob ):
                print("Copied armature.")
            else:
                print("Armature not copied.")

        sks_pairs = utils.get_sk_pairs( self.base_ob, self.hd_ob,
                                        exclude_driven= not self.copy_armature )
        if len(sks_pairs) > 0:
            print("Found {0} matching shape keys in base mesh.".format(len(sks_pairs)))
            print("Setting up drivers...")
            utils.copy_sk_drivers(self.base_ob, self.hd_ob, sks_pairs)
            return True
        else:
            self.report({'WARNING'}, "No matching shape keys found in base mesh.")
            return False
