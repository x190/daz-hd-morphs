import bpy
from os.path import basename

from . import operator_vector_disp
from . import operator_normal
from . import operator_shape_keys
from . import operator_misc
from . import operator_graft
from . import operator_material


class MorphFilesListEntry(bpy.types.PropertyGroup):
    def morph_filepath_update(self, context):
        self.name = basename(self.filepath)
        return None

    filepath: bpy.props.StringProperty(subtype="FILE_PATH", name="Morph filepath (.dsf)", update=morph_filepath_update)
    weight: bpy.props.FloatProperty(name="Weight", default=1, min=0, max=2, precision=2, subtype='FACTOR')
    group: bpy.props.IntProperty(name="Group", default=0, min=0, step=1, subtype='UNSIGNED')
    morph_component: bpy.props.EnumProperty( name = "Component",
                                             items = ( ('BOTH',  "Both", ""),
                                                       ('HD', "Only HD", ""),
                                                       ('BASE', "Only Base", ""), ),
                                             default = 'BOTH',
                                             description= "Morph component to consider" )

class MorphFilesList(bpy.types.PropertyGroup):
    entries: bpy.props.CollectionProperty(type=MorphFilesListEntry)
    index: bpy.props.IntProperty(default=-1, min=-1, name="Morph filename")

class TileListEntry(bpy.types.PropertyGroup):
    enabled: bpy.props.BoolProperty(name="Enabled", default=True)

class TilesList(bpy.types.PropertyGroup):
    entries: bpy.props.CollectionProperty(type=TileListEntry)
    index: bpy.props.IntProperty(default=-1, min=-1, name="UV tile")


class HDMorphProperties(bpy.types.PropertyGroup):
    def use_texture_tiles_update(self, context):
        if self.use_texture_tiles:
            operator_misc.initialize_tiles_list(context)

    def base_ob_update(self, context):
        try:
            from import_daz import api
            ob = context.scene.objects[self.base_ob]
            morphs_dirs = api.get_default_morph_directories(ob)
            if len(morphs_dirs) > 0:
                self.default_morph_dir = morphs_dirs[0]
        except:
            self.default_morph_dir = ""

    working_dirpath:     bpy.props.StringProperty(subtype="DIR_PATH", name="Working directory")

    unit_scale:        bpy.props.FloatProperty(name="Unit scale", default=0.01, min=0.00001,
                                description= "Scale used to convert from daz's units to Blender's units")

    base_ob:            bpy.props.StringProperty(name="Base mesh", description= "Base mesh", update=base_ob_update)

    geograft_file:     bpy.props.StringProperty(subtype="FILE_PATH", name="Geograft file")

    default_morph_dir:  bpy.props.StringProperty(name="Default morphs directory", subtype="DIR_PATH")

    morph_files_list:   bpy.props.PointerProperty(type=MorphFilesList)

    use_morph_list:     bpy.props.BoolProperty( name="With morphed base", default=False,
                                                description="Morph base mesh with the selected group's morphs" )

    selected_morphs_group: bpy.props.IntProperty( name="Selected group", default=0, min=0, step=1,
                                                  description="Selected morphs' group" )

    hd_ob:               bpy.props.StringProperty( name="HD mesh",
                                                   description= "HD mesh", )

    hd_level:            bpy.props.IntProperty(name="HD subdivisions", default=2, min=1, max=4, step=1)

    texture_size:        bpy.props.EnumProperty( name = "Size",
                                                 items = (  ('512',  "512", ""),
                                                            ('1024', "1024", ""),
                                                            ('2048', "2048", ""),
                                                            ('4096', "4096", ""), ),
                                                 default = '2048', )

    texture_uv_layer:   bpy.props.IntProperty(name="UV map index", default=0, min=0, step=1)

    texture_max_subd:   bpy.props.IntProperty(name="Max subdivisions", default=3, min=-1, max=8, step=1,
                            description= "Max subdivisions to use for baking textures.\n"
                                         "Set to -1 to use all subdivisions levels available" )

    texture_margin:     bpy.props.IntProperty(name="Margin", default=8, min=0, max=64, step=1, subtype='PIXEL',
                            description="Baked result is extended this many pixels beyond the border of each UV island, "
                                        "to soften seams in the texture" )

    normal_bake_type:   bpy.props.EnumProperty( name = "Type",
                                                items = ( ('MR_NORMAL', "Normals (multires)", ""),
                                                          ('NORMAL', "Normals", ""),
                                                          ('MR_DISP', "Displacements (multires)", "") ),
                                                default = 'MR_NORMAL' )

    normal_image_type:  bpy.props.EnumProperty( name = "Format",
                                                items = ( ('PNG8',  "8-bit PNG", ""),
                                                          ('PNG16', "16-bit PNG", ""),
                                                          ('EXR32', "32-bit EXR", ""), ),
                                                default = 'PNG8',
                                                description = "Format" )

    normal_cage_extrusion: bpy.props.FloatProperty( name="Extrusion", default=0.001, min=0, subtype='DISTANCE',
                                                    description="Extrusion used to bake normal textures in selected to active baking mode" )

    use_texture_tiles:  bpy.props.BoolProperty( name="Select UV tiles", default=False, update=use_texture_tiles_update,
                                                description="Select specific UV tiles only")

    generate_mode:   bpy.props.EnumProperty( name = "Generate",
                                             items = (  ('RIGGED', "Rigged HD", "hd mesh with rig (can import hd shape keys)"),
                                                        ('MULTIRES', "Rigged with multires", "hd multires mesh with base rig (can't import HD shape keys)"),
                                                        ('UNRIGGED', "Unrigged HD", "hd mesh without rig (can import hd shape keys)"), ),
                                             default = 'RIGGED', )

    copy_non_hd_meshes:   bpy.props.BoolProperty( name= "Copy other meshes", default=True,
                                                  description= "Copy other meshes parented to the base mesh armature (if any)" )

    import_base_sks:    bpy.props.BoolProperty( name="Import base morphs", default=True,
                                                description="Import morphs (and possibly hd components) associated with current base mesh shape keys to generated HD mesh (requires \"import_daz\" addon to be installed and enabled)" )

    base_sks_force_blender: bpy.props.BoolProperty( name="Force Blender base morphs", default=False,
                                                    description="Force base morphs on base mesh to be generated from shape keys, "
                                                                "instead of using corresponding .dsf files.\n"
                                                                "Might be needed if you projected shape keys onto geografts for example")

    copy_base_drivers: bpy.props.EnumProperty( name = "Copy drivers",
                                               items = (  ('NO_DRIVERS', "Skip", "Drivers are not created"),
                                                          ('ONLY_DRIVERS', "From base", "Copy drivers from matching base mesh shape keys (if any) "
                                                                                        "onto the HD mesh, while keeping the current HD mesh's armature"),
                                                          ('ARMATURE_DRIVERS', "From base with armature", "Copy drivers from matching base mesh shape keys (if any) "
                                                                                                          "onto the HD mesh,\n"
                                                                                                          "along with the base armature (might be necessary for all drivers to function properly)" ),
                                                        ),
                                               default = 'ARMATURE_DRIVERS', )

    copy_armature:  bpy.props.BoolProperty( name="Copy armature", default=True,
                                            description="Also copy current base mesh armature (might be necessary for all drivers to function properly)" )

    sk_source:   bpy.props.EnumProperty( name = "Source",
                                         items = ( ('MORPHS', "Morphs list", "Generate shape key/s on HD mesh from morphs files"),
                                                   ('BASE_MORPHS', "Morphs on base mesh", "Generate shape key/s on HD mesh from base mesh morph files (requires \"import_daz\" addon to be installed and enabled)"),
                                                   ('BASE_SHAPE', "Base mesh shape", "Generate shape key on HD mesh from current base mesh shape"), ),
                                         default = 'MORPHS', )

    texture_tiles:   bpy.props.PointerProperty(type=TilesList)

    vdisp_scale:     bpy.props.FloatProperty( name= "Scale", default=1.0, min=0.00001,
                             description="Set to 1.0 at first. Read recommendation in console output in "
                                         "case of scaling issues (pixel values above one or negative)" )

    vdisp_midlevel:  bpy.props.FloatProperty( name= "Midlevel", default=0.5,
                             description="Set to 0.5 at first. Read recommendation in console output in "
                                         "case of scaling issues (pixel values above one or negative)" )

    vdisp_auto_settings:   bpy.props.BoolProperty( name= "Auto scale and midlevel", default=True,
                                                   description= "Automatically set scale and midlevel to maximize range of pixel values.\n"
                                                                "Settings to use in Blender will be written in the console" )

    vdisp_image_type:      bpy.props.EnumProperty( name = "Format",
                                                   items = ( ('P', "16-bit PNG", ""),
                                                             ('X', "32-bit EXR", ""), ),
                                                    default = 'P',
                                                    description = "16-bit PNG requires adequate scale and midlevel settings.\n"
                                                                  "32-bit EXR doesn't require these settings but is bigger in size" )

    vdisp_target_space:    bpy.props.EnumProperty( name = "Space",
                                                   items = ( ('T', "Tangent", ""),
                                                             ('O', "Object", ""), ),
                                                    default = 'T',
                                                    description = "Tangent space is usually better and can be used in shaders. "
                                                                  "Object space textures can be used with Displacement modifiers "
                                                                  "(before armature modifier)" )

    morph_base_until:      bpy.props.IntProperty(name="Starting subdiv level", default=0, min=-1, max=2, step=1,
                             description= "Recommended to be left at 0 unless you understand what it does.\n\n"
                                         "If the mesh used as base mesh already has the morph's geometric details up to a certain subdiv level, "
                                         "adjust this value so the generated textures ignore those already present deformations.\n\n"
                                         "e.g. if your base mesh was subdivided once and already uses base and subdiv 1 shape keys "
                                         "for geometric HD details, set this value to 1 to genereate textures that only capture subdiv 2+ HD details."
                                         "\n\nFor vector displacements, set to -1 to capture details from all levels of the morph including base deformations (not recommended)."
                                         "\nFor normals, -1 and 0 are equivalent and will both ignore base deformations", )

    base_modifiers:         bpy.props.EnumProperty( name = "Base export modifiers",
                                                    items = ( ('NONE', "None", ""),
                                                              ('SK', "Only shape keys", ""),
                                                              ('NO_SK', "All but shape keys", ""),
                                                              ('NO_ARMATURE', "All but armature", ""),
                                                              ('ALL', "All", ""), ),
                                                    default = 'NONE',
                                                    description = "Modifiers to use when exporting base mesh "
                                                                  "(subdivision is always excluded)" )

    base_subdiv_method:     bpy.props.EnumProperty( name = "Subdiv method",
                                                    items = ( ('SUBSURF', "Applied subsurf (limit surface off)", "Blender subsurface modifier (limit surface off) has been applied to the hd mesh"),
                                                              ('SUBSURF_LIMIT', "Applied subsurf (limit surface on)", "Blender subsurface modifier (limit suface on) has been applied to the hd mesh"),
                                                              ('MULTIRES', "Applied multires", "Blender multiresolution modifier has been applied has been applied to the hd mesh"),
                                                              ('MODIFIER', "From hd modifier", "hd mesh has a (non-applied) subdivision modifier"),
                                                              ('DAZ', "daz", "hd mesh was subdivided through daz subdivision"),
                                                              ('NONE', "None", "Base mesh and hd mesh vertex count and order must match" ), ),
                                                     default = 'MULTIRES',
                                                     description = "Subdivision method you used to obtain the hd mesh from the base mesh"
                                                   )


class CUSTOM_UL_morphFiles(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        split = layout.split(factor=0.15)
        split.prop(item, "group", text="", emboss=False, translate=False)
        split = split.split(factor=0.12)
        split.prop(item, "weight", text="", emboss=False, translate=False)
        split = split.split(factor=0.2)
        split.label(text=item.morph_component, translate=False)
        split.label(text=item.name, translate=False)

    def filter_items(self, context, data, propname):
        filtered_entries = []
        sorted_entries = []

        entries = getattr(data, propname)
        helpers = bpy.types.UI_UL_list

        if len(self.filter_name) > 0:
            filtered_entries = helpers.filter_items_by_name(self.filter_name, self.bitflag_filter_item, entries, "name", reverse=False)

        if self.use_filter_sort_alpha:
            sort_data = [ (i, e) for i, e in enumerate(entries)]
            sorted_entries = helpers.sort_items_helper(sort_data, lambda e: str(e[1].group)+e[1].name, reverse=False)

        return filtered_entries, sorted_entries


class CUSTOM_UL_tiles(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        # ~ self.use_filter_show = False
        split = layout.split(factor=0.15)
        split.prop(item, "enabled", text="", emboss=True, translate=False)
        split.label(text=item.name, translate=False)

class MorphFilesListMenu(bpy.types.Menu):
    bl_idname = "OBJECT_MT_MorphFilesListMenu"
    bl_label = "Weights"

    def draw(self, context):
        layout = self.layout

        row = layout.row()
        row.operator(operator_misc.MorphFilesAddBase.bl_idname, icon='OBJECT_DATA') # text="")
        row = layout.row()
        row.operator(operator_misc.MorphFilesFavorites.bl_idname, icon='BOOKMARKS') # text="")

        row = layout.row()
        row.separator()
        row = layout.row()
        row.operator(operator_misc.MorphFilesNormalizeUniform.bl_idname, icon='NOCURVE') # text="")
        row = layout.row()
        row.operator(operator_misc.MorphFilesNormalize.bl_idname, icon='IPO_SINE') # text="")

        row = layout.row()
        row.separator()
        row = layout.row()
        row.operator(operator_misc.MorphFilesToggleComponent.bl_idname, icon='SURFACE_DATA') # text="")

        row = layout.row()
        row.separator()
        row = layout.row()
        row.operator(operator_misc.MorphFilesClear.bl_idname, icon="X") # text="")

class AddonPanel:
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "HD Morphs"

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def draw_morph_list(self, context, group_selector=False):
        # ~ box = self.layout
        box = self.layout.box()

        row = box.row(align=True)
        row.label(text="Morph Files")

        row.separator()
        row.operator(operator_misc.MorphFilesRemove.bl_idname, icon='REMOVE', text="")
        row.operator(operator_misc.MorphFilesAdd.bl_idname, icon='ADD', text="")
        row.operator(operator_misc.MorphFilesDuplicate.bl_idname, icon='DUPLICATE', text="")
        row.operator(operator_misc.MorphFilesExclusive.bl_idname, icon='PINNED', text="")
        row.operator(operator_misc.MorphFilesClearInvalid.bl_idname, icon='FILE_REFRESH', text="")
        row.separator()
        row.menu(MorphFilesListMenu.bl_idname, icon='DOWNARROW_HLT', text="")

        mfl = context.scene.daz_hd_morph.morph_files_list
        row = box.row()
        row.template_list( "CUSTOM_UL_morphFiles", "",
                            mfl, "entries",
                            mfl, "index",
                            rows=2, maxrows=8, type='DEFAULT' )

        if mfl.index >= 0:
            row = box.row()
            row.prop(mfl.entries[mfl.index], "filepath", text="Filepath", emboss=True)
            row = box.row()
            row.prop(mfl.entries[mfl.index], "morph_component", text="Component", emboss=True)

        if group_selector:
            row = box.row()
            row.prop(context.scene.daz_hd_morph, "selected_morphs_group")

    def draw_tiles_list(self, context, layout):
        # ~ layout = self.layout
        addon_props = context.scene.daz_hd_morph

        row = layout.row()
        row.prop(addon_props, "use_texture_tiles")
        if addon_props.use_texture_tiles:
            box = layout.box()
            row = box.row(align=True)
            row.label(text="UV tiles")

            row.operator(operator_misc.TilesListDisableAll.bl_idname, icon='CHECKBOX_DEHLT', text="")
            row.operator(operator_misc.TilesListEnableAll.bl_idname, icon='CHECKMARK', text="")

            row = box.row()
            row.template_list( "CUSTOM_UL_tiles", "",
                               addon_props.texture_tiles, "entries",
                               addon_props.texture_tiles, "index",
                               rows=2, maxrows=2, type='DEFAULT' )

    def draw_vdisp_settings(self, context, layout):
        addon_props = context.scene.daz_hd_morph

        # ~ layout = self.layout
        row = layout.row()
        row.prop(addon_props, "vdisp_image_type")
        row = layout.row()
        row.prop(addon_props, "texture_size")
        row = layout.row()
        row.prop(addon_props, "vdisp_target_space")
        row = layout.row()
        row.prop(addon_props, "texture_margin")
        if addon_props.vdisp_image_type == 'P':
            row = layout.row()
            row.prop(addon_props, "vdisp_auto_settings")
            if not addon_props.vdisp_auto_settings:
                row = layout.row()
                row.prop(addon_props, "vdisp_midlevel")
                row = layout.row()
                row.prop(addon_props, "vdisp_scale")
        row = layout.row()
        row.prop(addon_props, "texture_uv_layer")
        self.draw_tiles_list(context, layout)


class PANEL_PT_HDMorphPanel(AddonPanel, bpy.types.Panel):
    bl_label = "Main settings"
    bl_idname = "PANEL_PT_HDMorphPanel"

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False
        addon_props = context.scene.daz_hd_morph

        box = layout.box()
        row = box.row()
        row.prop(addon_props, "working_dirpath")
        row = box.row()
        row.prop(addon_props, "unit_scale")
        row = box.row()
        row.prop_search(addon_props, "base_ob", context.scene, "objects")

class PANEL_PT_HDMeshGraftPanel(AddonPanel, bpy.types.Panel):
    bl_parent_id = "PANEL_PT_HDMorphPanel"
    bl_idname = "PANEL_PT_HDMeshGraftPanel"
    bl_label = "Geograft"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False
        addon_props = context.scene.daz_hd_morph

        row = layout.row()
        row.prop(addon_props, "geograft_file")
        row = layout.row()
        row.operator(operator_graft.GenerateGraftInfoOperator.bl_idname)


class PANEL_PT_VectorDispMainPanel(AddonPanel, bpy.types.Panel):
    bl_idname = "PANEL_PT_VectorDispMainPanel"
    bl_label = "Vector displacements"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        return

class PANEL_PT_VectorDispTexturePanel(AddonPanel, bpy.types.Panel):
    bl_parent_id = "PANEL_PT_VectorDispMainPanel"
    bl_idname = "PANEL_PT_VectorDispTexturePanel"
    bl_label = "Texture settings"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False
        self.draw_vdisp_settings(context, layout)


class PANEL_PT_VectorDispMorphsPanel(AddonPanel, bpy.types.Panel):
    bl_parent_id = "PANEL_PT_VectorDispMainPanel"
    bl_idname = "PANEL_PT_VectorDispMorphsPanel"
    bl_label = "From morph files"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False
        addon_props = context.scene.daz_hd_morph

        self.draw_morph_list(context)
        row = layout.row()
        row.prop(addon_props, "morph_base_until")
        row = layout.row()
        row.prop(addon_props, "texture_max_subd")
        row = layout.row()
        row.prop(addon_props, "base_modifiers")
        row = layout.row()
        row.operator(operator_vector_disp.GenerateVectorDispOperator.bl_idname)

class PANEL_PT_VectorDispBlenderPanel(AddonPanel, bpy.types.Panel):
    bl_parent_id = "PANEL_PT_VectorDispMainPanel"
    bl_idname = "PANEL_PT_VectorDispBlenderPanel"
    bl_label = "From Blender mesh"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False

        row = layout.row()
        row.prop_search(context.scene.daz_hd_morph, "hd_ob", context.scene, "objects")
        row = layout.row()
        row.prop(context.scene.daz_hd_morph, "base_subdiv_method")
        row = layout.row()
        row.operator(operator_vector_disp.GenerateVectorDispObjObjOperator.bl_idname)


class PANEL_PT_NormalsPanel(AddonPanel, bpy.types.Panel):
    bl_idname = "PANEL_PT_NormalsPanel"
    bl_label = "Normals/Displacements"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        return

class PANEL_PT_NormalsTexturePanel(AddonPanel, bpy.types.Panel):
    bl_parent_id = "PANEL_PT_NormalsPanel"
    bl_idname = "PANEL_PT_NormalsTexturePanel"
    bl_label = "Texture settings"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False
        addon_props = context.scene.daz_hd_morph

        row = layout.row()
        row.prop(addon_props, "normal_bake_type")
        if addon_props.normal_bake_type == 'NORMAL':
            row = layout.row()
            row.prop(addon_props, "normal_cage_extrusion")
        row = layout.row()
        row.prop(addon_props, "normal_image_type")
        row = layout.row()
        row.prop(addon_props, "texture_size")
        row = layout.row()
        row.prop(addon_props, "texture_margin")
        row = layout.row()
        row.prop(addon_props, "texture_uv_layer")
        self.draw_tiles_list(context, layout)

class PANEL_PT_NormalsGeneratePanel(AddonPanel, bpy.types.Panel):
    bl_parent_id = "PANEL_PT_NormalsPanel"
    bl_idname = "PANEL_PT_NormalsGeneratePanel"
    bl_label = "From morph files"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False
        addon_props = context.scene.daz_hd_morph

        self.draw_morph_list(context)
        row = layout.row()
        row.prop(addon_props, "morph_base_until")
        row = layout.row()
        row.prop(addon_props, "texture_max_subd")
        row = layout.row()
        row.prop(addon_props, "base_modifiers")
        row = layout.row()
        row.operator(operator_normal.GenerateNormalsOperator.bl_idname)


class PANEL_PT_NormalsBlenderGeneratePanel(AddonPanel, bpy.types.Panel):
    bl_parent_id = "PANEL_PT_NormalsPanel"
    bl_idname = "PANEL_PT_NormalsBlenderGeneratePanel"
    bl_label = "From Blender mesh"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False

        row = layout.row()
        row.prop_search(context.scene.daz_hd_morph, "hd_ob", context.scene, "objects")
        row = layout.row()
        row.operator(operator_normal.GenerateNormalsBlenderOperator.bl_idname)


class PANEL_PT_NormalsGenerateFacsPanel(AddonPanel, bpy.types.Panel):
    bl_parent_id = "PANEL_PT_NormalsPanel"
    bl_idname = "PANEL_PT_NormalsGenerateFacsPanel"
    bl_label = "Generate facs"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False
        addon_props = context.scene.daz_hd_morph

        row = layout.row()
        row.prop(addon_props, "morph_base_until")
        row = layout.row()
        row.prop(addon_props, "texture_max_subd")
        row = layout.row()
        row.prop(addon_props, "base_modifiers")
        row = layout.row()
        row.operator(operator_normal.GenerateNormalsFacsOperator.bl_idname)


class PANEL_PT_HDMeshMainPanel(AddonPanel, bpy.types.Panel):
    bl_idname = "PANEL_PT_HDMeshMainPanel"
    bl_label = "HD mesh"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        return

class PANEL_PT_GenerateHDPanel(AddonPanel, bpy.types.Panel):
    bl_parent_id = "PANEL_PT_HDMeshMainPanel"
    bl_idname = "PANEL_PT_GenerateHDPanel"
    bl_label = "HD mesh generation"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False
        addon_props = context.scene.daz_hd_morph

        row=layout.row()
        row.prop(addon_props, "generate_mode")
        row = layout.row()
        row.prop(addon_props, "hd_level")
        if addon_props.generate_mode == 'RIGGED':
            row = layout.row()
            row.prop(addon_props, "import_base_sks")
            if addon_props.import_base_sks:
                row = layout.row()
                row.prop(addon_props, "base_sks_force_blender")
        row = layout.row()
        row.prop(addon_props, "copy_non_hd_meshes")
        row = layout.row()
        row.prop(addon_props, "use_morph_list")
        if addon_props.use_morph_list:
            self.draw_morph_list(context, group_selector=True)
        if addon_props.generate_mode == 'UNRIGGED':
            row = layout.row()
            row.prop(addon_props, "base_modifiers")
        row = layout.row()
        row.operator(operator_shape_keys.GenerateHDOperator.bl_idname)

class PANEL_PT_ImportSKPanel(AddonPanel, bpy.types.Panel):
    bl_parent_id = "PANEL_PT_HDMeshMainPanel"
    bl_idname = "PANEL_PT_ImportSKPanel"
    bl_label = "HD shape key"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False
        addon_props = context.scene.daz_hd_morph

        row = layout.row()
        row.prop_search(addon_props, "hd_ob", context.scene, "objects")
        row = layout.row()
        row.prop(addon_props, "sk_source")
        if addon_props.sk_source in ('MORPHS', 'BASE_MORPHS'):
            if addon_props.sk_source == 'BASE_MORPHS':
                row = layout.row()
                row.prop(addon_props, "base_sks_force_blender")
            elif addon_props.sk_source == 'MORPHS':
                self.draw_morph_list(context)
            row = layout.row()
            row.prop(addon_props, "copy_base_drivers")
        row = layout.row()
        row.prop(addon_props, "base_modifiers")
        row = layout.row()
        row.operator(operator_shape_keys.GenerateHDSKOperator.bl_idname)

class PANEL_PT_CreateDriversPanel(AddonPanel, bpy.types.Panel):
    bl_parent_id = "PANEL_PT_HDMeshMainPanel"
    bl_idname = "PANEL_PT_CreateDriversPanel"
    bl_label = "Create drivers"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False
        addon_props = context.scene.daz_hd_morph

        row = layout.row()
        row.prop_search(addon_props, "hd_ob", context.scene, "objects")
        row = layout.row()
        row.prop(addon_props, "copy_armature")
        row = layout.row()
        row.operator(operator_shape_keys.CreateHDDrivers.bl_idname)


class PANEL_PT_MaterialPanel(AddonPanel, bpy.types.Panel):
    bl_idname = "PANEL_PT_MaterialPanel"
    bl_label = "Material"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        return

class PANEL_PT_MaterialUtilities(AddonPanel, bpy.types.Panel):
    bl_parent_id = "PANEL_PT_MaterialPanel"
    bl_idname = "PANEL_PT_MaterialUtilities"
    bl_label = "Utilities"
    bl_options = {"DEFAULT_CLOSED"}

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False
        addon_props = context.scene.daz_hd_morph

        row = layout.row()
        row.operator(operator_material.MaterialAddVDTextures.bl_idname)
        row = layout.row()
        row.operator(operator_material.MaterialAddNMTextures.bl_idname)
        row = layout.row()
        row.operator(operator_material.MaterialAddDrivers.bl_idname)



class dazHDCustomPreferences(bpy.types.AddonPreferences):
    bl_idname = __name__

    delete_temporary_files:  bpy.props.BoolProperty(
                                name="Delete temporary files", default=True,
                                description="Delete .obj/.mtl/.dae files in the working directory after operators finish"
                             )
    def draw(self, context):
        box = self.layout.box()
        row = box.row()
        row.prop(self, "delete_temporary_files")


classes = (
    MorphFilesListMenu,
    MorphFilesListEntry,
    MorphFilesList,
    TileListEntry,
    TilesList,
    HDMorphProperties,
    dazHDCustomPreferences,

    operator_vector_disp.GenerateVectorDispOperator,
    operator_vector_disp.GenerateVectorDispObjObjOperator,

    operator_normal.GenerateNormalsOperator,
    operator_normal.GenerateNormalsBlenderOperator,
    # ~ operator_normal.GenerateNormalsFacsOperator,

    operator_shape_keys.GenerateHDOperator,
    operator_shape_keys.GenerateHDSKOperator,
    operator_shape_keys.CreateHDDrivers,

    operator_graft.GenerateGraftInfoOperator,

    operator_material.MaterialAddVDTextures,
    operator_material.MaterialAddNMTextures,
    operator_material.MaterialAddDrivers,

    operator_misc.MorphFilesDuplicate,
    operator_misc.MorphFilesAdd,
    operator_misc.MorphFilesRemove,
    operator_misc.MorphFilesClear,
    operator_misc.MorphFilesNormalize,
    operator_misc.MorphFilesNormalizeUniform,
    operator_misc.MorphFilesExclusive,
    operator_misc.TilesListEnableAll,
    operator_misc.TilesListDisableAll,
    operator_misc.MorphFilesFavorites,
    operator_misc.MorphFilesToggleComponent,
    operator_misc.MorphFilesClearInvalid,
    operator_misc.MorphFilesAddBase,

    CUSTOM_UL_morphFiles,
    CUSTOM_UL_tiles,

    PANEL_PT_HDMorphPanel,

    PANEL_PT_VectorDispMainPanel,
    PANEL_PT_VectorDispTexturePanel,
    PANEL_PT_VectorDispMorphsPanel,
    PANEL_PT_VectorDispBlenderPanel,

    PANEL_PT_NormalsPanel,
    PANEL_PT_NormalsTexturePanel,
    PANEL_PT_NormalsGeneratePanel,
    PANEL_PT_NormalsBlenderGeneratePanel,
    # ~ PANEL_PT_NormalsGenerateFacsPanel,

    PANEL_PT_HDMeshMainPanel,
    PANEL_PT_GenerateHDPanel,
    PANEL_PT_ImportSKPanel,
    PANEL_PT_CreateDriversPanel,
    PANEL_PT_HDMeshGraftPanel,

    PANEL_PT_MaterialPanel,
    PANEL_PT_MaterialUtilities,
)

def register():
    for c in classes:
        bpy.utils.register_class(c)
    bpy.types.Scene.daz_hd_morph = bpy.props.PointerProperty(type=HDMorphProperties)

def unregister():
    del bpy.types.Scene.daz_hd_morph
    for c in reversed(classes):
        bpy.utils.unregister_class(c)
