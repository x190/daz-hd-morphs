import bpy, os, json, time
from collections.abc import Mapping
from . import dll_wrapper
from . import utils
from .operator_common import HDMorphOperator


class UDIM_Baker:
    bake_types = { 'NORMAL':    {"prefix": 'NM',     "setting": 'NORMAL'},
                   'MR_NORMAL': {"prefix": "mrNM",   "setting": 'NORMALS'},
                   'MR_DISP':   {"prefix": "mrDISP", "setting": 'DISPLACEMENT'}, }
    image_size = {"512", "1024", "2048", "4096"}
    image_types = {'PNG8', 'PNG16', 'EXR32'}

    def __init__(self):
        self.output_dir = None
        self.bake_type = None
        self.unsubdivide_times = None
        self.tiles_to_bake = None

        self.image_basename = None
        self.imageSize = None
        self.texture_margin = None
        self.image_type = None
        self.use_float_buffer = None
        self.image_ext = None
        self.use_multires = None
        self.last_tiles_set = None
        self.uv_layer = None
        self.cage_extrusion = None

        self.saved_bake_settings = None
        self.img_float_scene = None

        self.saved_materials = None
        self.saved_materials_indices = None

        self.saved_uv_layer_index = None

    def set_tiles_to_bake(self, tiles):
        if tiles is None:
            self.tiles_to_bake = None
        else:
            self.tiles_to_bake = set(tiles)

    def set_uv_layer(self, layer_n):
        self.uv_layer = layer_n

    def set_bake_type(self, bake_type):
        if bake_type not in self.bake_types:
            raise ValueError("Invalid bake_type: \"{}\".".format(bake_type))
        self.bake_type = bake_type
        self.use_multires = bake_type.startswith("MR_")

    def set_image_size(self, image_size):
        if image_size not in self.image_size:
            raise ValueError("Invalid imageSize")
        self.imageSize = image_size

    def set_texture_margin(self, margin):
        self.texture_margin = margin

    def set_image_type(self, image_type):
        if image_type not in self.image_types:
            raise ValueError("Invalid image_type")
        self.image_type = image_type
        self.use_float_buffer = ( self.image_type in ('PNG16', 'EXR32') )
        if self.image_type != 'EXR32':
            self.image_ext = "png"
        else:
            self.image_ext = "exr"

    def set_output_dirpath(self, output_dir):
        if self.bake_type is None:
            raise RuntimeError("bake_type not set")
        self.output_dir = os.path.join(output_dir, self.bake_type)
        if not os.path.exists(self.output_dir):
            os.mkdir(self.output_dir)
        return self.output_dir

    def apply_multires(self, ob):
        if len(ob.modifiers) != 0 or ob.data.shape_keys is not None:
            raise RuntimeError("Object has modifiers or shape keys")
        # ~ ob.modifiers.clear()
        # ~ utils.remove_shape_keys(ob)

        mr = ob.modifiers.new("daz_hd_morphs_multires_bake", 'MULTIRES')
        mr.show_only_control_edges = True
        mr.quality = 4
        # ~ mr.subdivision_type = 'CATMULL_CLARK'
        mr.uv_smooth = 'PRESERVE_CORNERS'
        mr.boundary_smooth = 'ALL'
        mr.use_creases = True
        mr.use_custom_normals = False

        utils.make_single_active(ob)
        if self.unsubdivide_times < 0:
            print("Rebuilding subdivisions with multiresolution modifier...")
            bpy.ops.object.multires_rebuild_subdiv(modifier=mr.name)
        else:
            print("Unsubdividing {0} times with multiresolution modifier...".format(self.unsubdivide_times))
            for _ in range(0, self.unsubdivide_times):
                bpy.ops.object.multires_unsubdivide(modifier=mr.name)
        mr.levels = 0

    def get_multires_bake_settings(self):
        bake_settings = {}

        bake_settings["render"] = {}
        bake_settings["render"]["engine"] = 'CYCLES'
        bake_settings["render"]["use_bake_multires"] = True
        bake_settings["render"]["bake_type"] = self.bake_types[self.bake_type]["setting"]
        bake_settings["render"]["use_bake_clear"] = True
        bake_settings["render"]["use_bake_lores_mesh"] = False
        bake_settings["render"]["bake_margin"] = self.texture_margin

        bake_settings["cycles"] = {}
        bake_settings["cycles"]["samples"] = 1    # 512

        return bake_settings

    def get_bake_settings(self):
        bake_settings = {}

        bake_settings["render"] = {}
        bake_settings["render"]["engine"] = 'CYCLES'
        bake_settings["render"]["use_bake_multires"] = False

        bake_settings["render"]["bake"] = {}
        bake_settings["render"]["bake"]["normal_space"] = 'TANGENT'
        bake_settings["render"]["bake"]["normal_r"] = 'POS_X'
        bake_settings["render"]["bake"]["normal_g"] = 'POS_Y'
        bake_settings["render"]["bake"]["normal_b"] = 'POS_Z'

        bake_settings["render"]["bake"]["use_selected_to_active"] = True
        bake_settings["render"]["bake"]["use_cage"] = False
        bake_settings["render"]["bake"]["cage_extrusion"] = self.cage_extrusion
        bake_settings["render"]["bake"]["max_ray_distance"] = 0.0
        bake_settings["render"]["bake"]["target"] = 'IMAGE_TEXTURES'
        bake_settings["render"]["bake"]["use_clear"] = True
        bake_settings["render"]["bake"]["margin"] = self.texture_margin

        bake_settings["cycles"] = {}
        bake_settings["cycles"]["samples"] = 1    # 512
        bake_settings["cycles"]["bake_type"] = self.bake_types[self.bake_type]["setting"]

        return bake_settings

    @classmethod
    def unwind_settings(cls, d, obj, d2):
        for k, v in d.items():
            if isinstance(v, Mapping):
                d2[k] = {}
                obj2 = getattr(obj, k)
                cls.unwind_settings(v, obj2, d2[k])
            else:
                d2[k] = getattr(obj, k)
                setattr(obj, k, v)

    def set_scene(self):
        self.restore_scene()
        if self.bake_type is None or self.use_multires is None:
            raise RuntimeError("Uninitialized 'bake_type' and 'use_multires'")

        scn = bpy.context.scene
        self.saved_bake_settings = {}
        if not self.use_multires:
            bake_settings = self.get_bake_settings()
        else:
            bake_settings = self.get_multires_bake_settings()
        self.unwind_settings(bake_settings, scn, self.saved_bake_settings)

        if self.use_float_buffer:
            scn = bpy.data.scenes.new('daz_hd_morphs_img_float')
            scn.render.use_file_extension = True
            scn.render.use_render_cache = False
            if self.image_type == 'PNG16':
                scn.render.image_settings.file_format = 'PNG'
                scn.render.image_settings.color_mode = 'RGB'
                scn.render.image_settings.color_depth = '16'
                scn.render.image_settings.compression = 15
            elif self.image_type == 'EXR32':
                scn.render.image_settings.file_format = 'OPEN_EXR'
                scn.render.image_settings.color_mode = 'RGB'
                scn.render.image_settings.color_depth = '32'
                scn.render.image_settings.exr_codec = 'ZIP'
                # scn.render.image_settings.use_zbuffer = False
                scn.render.image_settings.use_preview = False
            else:
                raise ValueError("Invalid image_type")
            scn.render.use_overwrite = True
            scn.render.use_placeholder = False
            self.img_float_scene = scn

    def restore_scene(self):
        if self.saved_bake_settings is not None:
            scn = bpy.context.scene
            self.unwind_settings(self.saved_bake_settings, scn, {})
            self.saved_bake_settings = None

        if self.img_float_scene is not None:
            bpy.data.scenes.remove(self.img_float_scene, do_unlink=True)
            self.img_float_scene = None

    def store_materials(self, ob):
        self.saved_materials = None
        self.saved_materials_indices = None
        if len(ob.data.materials) > 0:
            self.saved_materials_indices = [f.material_index for f in ob.data.polygons]
            self.saved_materials = list(ob.data.materials)
            for _ in self.saved_materials:
                ob.data.materials.pop()

    def restore_materials(self, ob):
        if self.saved_materials is None or self.saved_materials_indices is None:
            return
        for _ in list(ob.data.materials):
            ob.data.materials.pop()
        for mat in self.saved_materials:
            ob.data.materials.append(mat)
        for fi, mi in enumerate(self.saved_materials_indices):
            ob.data.polygons[fi].material_index = mi
        self.saved_materials = None
        self.saved_materials_indices = None

    def store_active_uv_index(self, ob):
        self.saved_uv_layer_index = None
        active_uv_layer = utils.get_active_uv_layer(ob)
        if active_uv_layer != self.uv_layer:
            self.saved_uv_layer_index = active_uv_layer
            utils.set_active_uv_layer(ob, self.uv_layer)

    def restore_active_uv_index(self, ob):
        if self.saved_uv_layer_index is not None:
            utils.set_active_uv_layer(ob, self.saved_uv_layer_index)
            self.saved_uv_layer_index = None

    def store_status(self, ob):
        self.store_materials(ob)
        self.store_active_uv_index(ob)
        self.set_scene()

    def restore_status(self, ob):
        self.restore_scene()
        self.restore_active_uv_index(ob)
        self.restore_materials(ob)

    def create_image(self, tile):
        image_filename = "{0}-{1}-{2}_{3}.{4}".format( self.image_basename,
                                                       self.bake_types[self.bake_type]["prefix"],
                                                       self.imageSize, tile, self.image_ext )
        size = int(self.imageSize)
        img = bpy.data.images.new( image_filename, size, size,
                                   alpha=False, is_data=True,
                                   float_buffer=self.use_float_buffer )
        img.colorspace_settings.name = 'Non-Color'
        img.file_format = 'PNG'
        img.filepath = os.path.join(self.output_dir, image_filename)
        return img

    @staticmethod
    def create_material(ob, img):
        mat = bpy.data.materials.new(img.name)
        ob.data.materials.append(mat)
        ob.active_material = mat
        mat.use_nodes = True
        tree = mat.node_tree
        tree.nodes.clear()
        texco = tree.nodes.new(type = "ShaderNodeTexCoord")
        # ~ texco.location = (0, 0)
        node = tree.nodes.new(type = "ShaderNodeTexImage")
        # ~ node.location = (200,0)
        node.image = img
        node.extension = 'CLIP'
        node.select = True
        tree.nodes.active = node
        tree.links.new(texco.outputs["UV"], node.inputs["Vector"])
        return mat

    @staticmethod
    def get_face_tile(f, uvloop):
        n = len(f.vertices)
        rx = sum( [uvloop.data[k].uv[0] for k in f.loop_indices] ) / n
        ry = sum( [uvloop.data[k].uv[1] for k in f.loop_indices] ) / n
        i = max(0, int(round(rx-0.5)))
        j = max(0, int(round(ry-0.5)))
        tile = 1001 + 10 * j + i
        return tile

    def generate_tiles_set(self, ob):
        uvloop = ob.data.uv_layers[self.uv_layer]
        tiles = set()
        for f in ob.data.polygons:
            tiles.add(self.get_face_tile(f, uvloop))
        return tiles

    def translate_tile(self, ob, tile, sign):
        j = ( tile - 1001 ) // 10
        i = ( tile - 1001 - 10*j ) % 10
        dx = sign*i
        dy = sign*j
        uvloop = ob.data.uv_layers[self.uv_layer]
        for f in ob.data.polygons:
            for n in f.loop_indices:
                uvloop.data[n].uv[0] += dx
                uvloop.data[n].uv[1] += dy

    def bake(self, base_ob, hd_ob, restore_status, reuse_tiles_set):
        if not self.use_multires:
            baked_ob = base_ob
            print("Baking \"{0}\" onto \"{1}\".".format(hd_ob.name, base_ob.name))
        else:
            baked_ob = hd_ob
            self.apply_multires(hd_ob)
            print("Baking \"{0}\" with multires.".format(hd_ob.name))

        if restore_status:
            self.store_status(baked_ob)
        else:
            utils.delete_object_materials(baked_ob)

        tiles_set = None
        if reuse_tiles_set and self.last_tiles_set is not None:
            tiles_set = self.last_tiles_set
        else:
            tiles_set = self.generate_tiles_set(baked_ob)
            if reuse_tiles_set:
                self.last_tiles_set = tiles_set

        saved_imgs = []
        for tile_n in tiles_set:
            if (self.tiles_to_bake is not None) and (tile_n not in self.tiles_to_bake):
                continue

            img = self.create_image(tile_n)
            mat = self.create_material(baked_ob, img)
            self.translate_tile(baked_ob, tile_n, -1)

            utils.make_single_active(baked_ob)
            print("Baking...", end="", flush=True)
            if not self.use_multires:
                hd_ob.select_set(True)
                bpy.ops.object.bake(type=self.bake_types[self.bake_type]["setting"])
            else:
                bpy.ops.object.bake_image()

            if self.use_float_buffer:
                img.save_render(img.filepath, scene=self.img_float_scene)
            else:
                img.save()

            print("Saved \"{0}\".".format(img.filepath))
            saved_imgs.append( (img.filepath, tile_n) )
            self.translate_tile(baked_ob, tile_n, 1)
            bpy.data.images.remove(img, do_unlink=True, do_id_user=True, do_ui_user=True)
            baked_ob.data.materials.pop()
            bpy.data.materials.remove(mat, do_unlink=True, do_id_user=True, do_ui_user=True)

        if restore_status:
            self.restore_status(baked_ob)
        return saved_imgs


class GenerateNormalsCommon(HDMorphOperator):
    # from_morphs
    use_obj = True
    max_subd = None
    base_modifiers = None
    morph_base_until = None
    morphs_groups_names = None

    # from Blender
    base_name = None

    # common
    bake_type = None
    cage_extrusion = None
    texture_size = None
    texture_margin = None
    image_type = None
    uv_layer = None

    textures_output_dirpath = None

    def load_common_variables(self, context, check_hd=False):
        if not self.check_input(context, check_hd=check_hd):
            return False
        if not self.check_texture_tiles(context):
            return False
        addon_props = context.scene.daz_hd_morph
        self.bake_type = addon_props.normal_bake_type
        if self.bake_type == 'NORMAL':
            self.cage_extrusion = addon_props.normal_cage_extrusion
        self.texture_size = addon_props.texture_size
        self.texture_margin = addon_props.texture_margin
        self.image_type = addon_props.normal_image_type
        self.uv_layer = addon_props.texture_uv_layer
        if not self.check_uv_layer(self.uv_layer):
            return False
        return True

    def set_common_ub_settings(self, ub):
        ub.set_bake_type(self.bake_type)
        ub.set_image_size(self.texture_size)
        ub.set_texture_margin(self.texture_margin)
        ub.set_image_type(self.image_type)
        ub.set_tiles_to_bake(self.tiles)
        ub.cage_extrusion = self.cage_extrusion

    def generate_normals_json(self, saved_textures):
        for g, t_list in saved_textures.items():
            morphs_list = self.morphs_groups.get(g)
            f_id = self.morphs_groups_names.get(g)
            if morphs_list is None or f_id is None:
                raise ValueError("Invalid group number.")

            d = {}
            d["id"] = f_id
            d["format"] = self.image_type
            d["size"] = int(self.texture_size)
            d["margin"] = self.texture_margin
            d["tile"] = None
            d["morphs_filepaths"] = [t[0] for t in morphs_list]
            d["morphs_weights"] = [t[1] for t in morphs_list]
            d["morphs_components"] = [t[2] for t in morphs_list]
            d["uv_layer_name"] = self.uv_layer_name
            d["bake_type"] = self.bake_type
            if self.bake_type == 'NORMAL':
                d["bake_extrusion"] = self.cage_extrusion

            for fp, tile_n in t_list:
                if not os.path.isfile(fp):
                    raise ValueError("Texture \"{0}\" does not exist.".format(fp))
                d["tile"] = tile_n
                fp_json = fp + ".json"
                with open(fp_json, "w", encoding="utf-8") as f:
                    json.dump(d, f, indent=4)

        return True

    def generate_normals_blender_json(self, saved_imgs):
        for fp, tile_n in saved_imgs:
            if not os.path.isfile(fp):
                continue

            d = {}
            d["id"] = self.base_name
            d["format"] = self.image_type
            d["size"] = int(self.texture_size)
            d["margin"] = self.texture_margin
            d["tile"] = tile_n
            d["base_mesh_name"] = self.base_ob.name
            d["hd_mesh_name"] = self.hd_ob.name
            d["uv_layer_name"] = self.uv_layer_name
            d["bake_type"] = self.bake_type
            if self.bake_type == 'NORMAL':
                d["bake_extrusion"] = self.cage_extrusion

            fp_json = fp + ".json"
            with open(fp_json, "w", encoding="utf-8") as f:
                json.dump(d, f, indent=4)

        return True

    def generate_maps_from_morphs(self, context):
        is_mr_bake = self.bake_type.startswith("MR_")

        base_exportedf = self.export_base_dae( context, self.base_modifiers,
                                               include_weights=False,
                                               with_obj=True, uv_layer=self.uv_layer )
        morph_groups_levels, tot_groups = utils.get_morph_groups_levels(self.morphs_groups, self.max_subd)

        ub = UDIM_Baker()
        self.set_common_ub_settings(ub)

        if is_mr_bake:
            ub.set_uv_layer(0)
            ub.set_scene()
        else:
            ub.set_uv_layer(self.uv_layer)
            ub.store_status(self.base_ob)

        ub.set_output_dirpath(self.textures_output_dirpath)

        meshesDirpath = self.create_temporary_subdir()
        fp_pref = meshesDirpath + "/shape-"

        try:
            saved_textures = {}
            for level, groups in morph_groups_levels:
                self.cleanup_shape_files()

                morphs_groups2 = {}
                morphs_groups_names2 = {}
                for g in groups:
                    morphs_groups2[g] = self.morphs_groups[g]
                    morphs_groups_names2[g] = self.morphs_groups_names[g]

                dll_wrapper.execute_in_new_thread( "generate_shapes",
                                                   self.gScale, base_exportedf, 1,
                                                   level, self.geograft_file,
                                                   morphs_groups2, None, None,
                                                   meshesDirpath )

                for i, g in enumerate(morphs_groups2):
                    morph_name = morphs_groups_names2[g]

                    if self.use_obj:
                        fp = fp_pref + str(i) + ".obj"
                        hd_ob_morphed = utils.import_dll_obj(fp)
                    else:
                        fp = fp_pref + str(i) + ".dae"
                        hd_ob_morphed = utils.import_dll_collada(fp)

                    print("\n--Baking of morph group \"{0}\":".format(morph_name))

                    morph_levels = utils.get_subdivision_level(self.base_ob, hd_ob_morphed)
                    unsubdivide_times = -1
                    if self.morph_base_until > 0:
                        unsubdivide_times = morph_levels - self.morph_base_until
                        if unsubdivide_times <= 0:
                            self.report({'ERROR'}, "Morph max level is smaller or equal than \"morph base until\".")
                            return False

                    if not is_mr_bake:
                        hd_ob_morphed.matrix_world.translation = self.base_ob.matrix_world.translation
                    else:
                        ub.unsubdivide_times = unsubdivide_times

                    ub.image_basename = morph_name
                    saved_imgs = ub.bake(self.base_ob, hd_ob_morphed, restore_status=False, reuse_tiles_set=True)
                    saved_textures[g] = saved_imgs
                    utils.delete_object(hd_ob_morphed)

        except Exception as e:
            raise e
        finally:
            if not is_mr_bake:
                ub.restore_status(self.base_ob)
            else:
                ub.restore_scene()

        self.generate_normals_json(saved_textures)
        return True

    def generate_maps_from_blender(self, context):
        is_mr_bake = self.bake_type.startswith("MR_")

        hd_ob_copy = utils.copy_object(self.hd_ob)
        if is_mr_bake:
            utils.ob_apply_modifiers(hd_ob_copy, modifiers='ALL') #'NO_ARMATURE')
            utils.set_active_uv_layer(hd_ob_copy, self.uv_layer)
            restore_status = False
        else:
            utils.ob_apply_modifiers(hd_ob_copy, modifiers='ALL')
            hd_ob_copy.matrix_world.translation = self.base_ob.matrix_world.translation
            restore_status = True

        ub = UDIM_Baker()
        self.set_common_ub_settings(ub)
        ub.set_uv_layer(self.uv_layer)
        ub.set_output_dirpath(self.textures_output_dirpath)

        if is_mr_bake:
            diff_levels = utils.get_subdivision_level(self.base_ob, hd_ob_copy)
            if diff_levels <= 0:
                self.report({'ERROR'}, "hd mesh is not more subdivided than base mesh")
                return False
            ub.unsubdivide_times = diff_levels
        if not restore_status:
            ub.set_scene()
        ub.image_basename = self.base_name

        try:
            saved_imgs = ub.bake(self.base_ob, hd_ob_copy, restore_status=restore_status, reuse_tiles_set=False)
        except Exception as e:
            raise e
        finally:
            utils.delete_object(hd_ob_copy)
            if not restore_status:
                ub.restore_scene()

        self.generate_normals_blender_json(saved_imgs)
        return True



class GenerateNormalsOperator(GenerateNormalsCommon):
    """Generate baked normal/grayscale displacement textures from morph/s"""
    bl_idname = "dazhdmorph.normals"
    bl_label = "Generate baked textures"

    def execute(self, context):
        if not self.load_common_variables(context):
            return {'CANCELLED'}
        if not self.check_morphs_list(context):
            return {'CANCELLED'}

        addon_props = context.scene.daz_hd_morph
        self.max_subd = addon_props.texture_max_subd
        self.base_modifiers = addon_props.base_modifiers
        self.morph_base_until = addon_props.morph_base_until

        t0 = time.perf_counter()

        self.morphs_groups_names = utils.getMorphsGroupsNames(self.morphs_groups)
        self.textures_output_dirpath = self.create_normal_subdir()

        r = self.generate_maps_from_morphs(context)
        self.cleanup(context)
        if not r:
            return {'CANCELLED'}
        if self.bake_type != 'MR_DISP':
            self.report({'INFO'}, "Normal maps generated.")
        else:
            self.report({'INFO'}, "Grayscale displacement maps generated.")

        print("Elapsed: {}".format(time.perf_counter() - t0))
        return {'FINISHED'}


class GenerateNormalsBlenderOperator(GenerateNormalsCommon):
    """Generate baked normal/grayscale displacement textures from hd mesh"""
    bl_idname = "dazhdmorph.blender_normals"
    bl_label = "Generate baked textures"

    def execute(self, context):
        if not self.load_common_variables(context, check_hd=True):
            return {'CANCELLED'}
        self.base_name = "blender_{0}".format(utils.makeValidFilename(self.hd_ob.name))
        self.textures_output_dirpath = self.create_normal_subdir()

        t0 = time.perf_counter()
        r = self.generate_maps_from_blender(context)
        self.cleanup(context)
        if not r:
            return {'CANCELLED'}
        if self.bake_type != 'MR_DISP':
            self.report({'INFO'}, "Normal maps generated.")
        else:
            self.report({'INFO'}, "Grayscale displacement maps generated.")

        print("Elapsed: {}".format(time.perf_counter() - t0))
        return {'FINISHED'}


class GenerateNormalsFacsOperator(GenerateNormalsCommon):
    """Generate normal textures for facs shape keys currently on base mesh"""
    bl_idname = "dazhdmorph.normals_facs"
    bl_label = "Generate facs normals"

    facs_dir = None

    def execute(self, context):
        if not self.load_common_variables(context):
            return {'CANCELLED'}
        if not self.check_facs(context):
            return {'CANCELLED'}

        addon_props = context.scene.daz_hd_morph
        self.max_subd = addon_props.texture_max_subd
        self.base_modifiers = addon_props.base_modifiers
        self.morph_base_until = addon_props.morph_base_until

        t0 = time.perf_counter()

        if not self.setup(context):
            return {'CANCELLED'}

        r = self.generate_maps_from_morphs(context)
        self.cleanup(context)
        if not r:
            return {'CANCELLED'}
        if self.bake_type != 'MR_DISP':
            self.report({'INFO'}, "Normal maps generated.")
        else:
            self.report({'INFO'}, "Grayscale displacement maps generated.")

        print("Elapsed: {}".format(time.perf_counter() - t0))
        return {'FINISHED'}

    def check_facs(self, context):
        morphs_dirs = utils.api_get_morphs_directories(self.base_ob)
        if morphs_dirs is None:
            self.report({'ERROR'}, "Required addon \"import_daz\" not found, or api function threw an exception.")
            return False

        for d in morphs_dirs:
            if os.path.basename(d).strip().lower() == 'facs' and os.path.isdir(d):
                self.facs_dir = d
                return True
        self.report({'ERROR'}, "Base mesh has no facs loaded.")
        return False

    def setup(self, context):
        base_sks = utils.get_sk_names(self.base_ob)
        if len(base_sks) == 0:
            return False
        base_sks_map = {}
        for k in base_sks:
            base_sks_map[k.lower()] = k
        del base_sks

        base_hd_facs = []
        for root, dirs, files in os.walk(self.facs_dir):
            for f in files:
                if utils.has_extension(f, "dsf"):
                    fp = os.path.join(root, f)
                    dsf_id = utils.read_dsf_id(fp, only_with_dhdm=True)
                    if dsf_id is not None:
                        src_sk_name = base_sks_map.get( dsf_id.lower(), None )

                        if not src_sk_name.startswith("facs_bs_Mouth") or "Left" not in src_sk_name:
                            continue

                        if src_sk_name is not None:
                            base_hd_facs.append( (src_sk_name, fp) )
            break

        if len(base_hd_facs) == 0:
            self.report({'ERROR'}, "Found no base HD facs shape keys.")
            return False
        else:
            print("- Found {0} base HD facs shape keys. Generating textures...".format(len(base_hd_facs)))

        morphs_groups = {}
        morphs_groups_names = {}
        for i, t in enumerate(base_hd_facs):
            morphs_groups[i] = []
            morphs_groups[i].append( (t[1], 1) )
            morphs_groups_names[i] = t[0]

        self.morphs_groups, self.morphs_groups_names = morphs_groups, morphs_groups_names
        normals_dir = self.create_normal_subdir()
        facs_normals_dir = os.path.join(normals_dir, "facs")
        if not os.path.isdir(facs_normals_dir):
            os.mkdir(facs_normals_dir)
        self.textures_output_dirpath = facs_normals_dir
        return True
