import bpy, mathutils, os, json
from . import utils


class GenerateGraftInfoOperator(bpy.types.Operator):
    """Generate file to properly handle geografts.\nActive object should be the merged mesh, while the original body and geograft are selected.\nOnce the file is generated, Base mesh should be the merged mesh"""
    bl_idname = "dazhdmorph.graft_file"
    bl_label = "Generate geograft file"

    geos = None
    body = None
    merged = None
    working_dirpath = None
    d = {}
    kd = None
    merged_faces = None
    filename = None

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def invoke(self, context, event):
        return self.execute(context)

    def execute(self, context):
        self.merged = context.active_object
        if self.merged is None or self.merged.type != 'MESH':
            self.report({'ERROR'}, "Invalid selection.")
            return {'CANCELLED'}

        meshes = utils.get_selected_meshes(context)
        self.geos = []
        for m in meshes:
            if m is self.merged:
                continue
            if not m.data.DazGraftGroup:
                self.body = m
                continue
            self.geos.append(m)

        if (len(self.geos) == 0) or (self.body is None):
            self.report({'ERROR'}, "Invalid selection.")
            return {'CANCELLED'}

        addon_props = context.scene.daz_hd_morph
        working_dirpath = addon_props.working_dirpath
        if not working_dirpath.strip():
            self.report({'ERROR'}, "Working directory not set.")
            return False
        working_dirpath = os.path.abspath( bpy.path.abspath(working_dirpath) )
        if not os.path.isdir(working_dirpath):
            self.report({'ERROR'}, "Working directory not found.")
            return False
        self.working_dirpath = working_dirpath
        self.filename = self.get_filename()

        if not self.generate_file(context):
            return {'CANCELLED'}

        self.report({'INFO'}, "File \"{}\" generated".format(self.filename))
        return {'FINISHED'}

    def get_filename(self):
        p0 = self.body.name.replace(" ", "_")
        for g in self.geos:
            p0 = "{}-{}".format(p0, g.name.replace(" ", "_"))
        return utils.makeValidFilename(p0) + ".json"

    def write_file(self):
        geo_dir = os.path.join(self.working_dirpath, "geo_files")
        if not os.path.isdir(geo_dir):
            os.mkdir(geo_dir)
        geo_file = os.path.join(geo_dir, self.filename)
        with open(geo_file, "w", encoding="utf-8") as f:
            json.dump(self.d, f)
        return geo_file

    def initialize_kd(self):
        self.kd = mathutils.kdtree.KDTree(len(self.merged.data.vertices))
        for i, v in enumerate(self.merged.data.vertices):
            self.kd.insert(v.co, i)
        self.kd.balance()

    def find_face(self, vertices, d_vn):
        t = []
        for vn in vertices:
            if vn not in d_vn:
                return None
            t.append(d_vn[vn])
        t.sort()
        t = tuple(t)
        if t in self.merged_faces:
            return self.merged_faces[t]
        return None

    def find_matches(self, ob, ignore):
        nverts = len(ob.data.vertices)
        self.d[nverts] = {}

        d_vn = {}
        for i, v in enumerate(ob.data.vertices):
            if i in ignore:
                continue
            co, j, dist = self.kd.find(v.co)
            if dist > 1e-4:
                print("WARNING: vertex matching during geograft file generation wasn't optimal.")
            d_vn[i] = j

        p_fn = []
        for fn, f in enumerate(ob.data.polygons):
            fn2 = self.find_face(f.vertices, d_vn)
            if fn2 is not None:
                p_fn.append((fn, fn2))

        self.d[nverts]["vn"] = list(d_vn.items())
        self.d[nverts]["fn"] = p_fn

    def generate_file(self, context):
        self.initialize_kd()

        self.merged_faces = {}
        for fn, f in enumerate(self.merged.data.polygons):
            t = tuple( sorted([vn for vn in f.vertices]) )
            self.merged_faces[t] = fn

        body_ignore = set()
        for g in self.geos:
            g_ignore = set()
            for e in g.data.DazMaskGroup:
                fverts = self.body.data.polygons[e.a].vertices
                for vn in fverts:
                    body_ignore.add(vn)
            for p in g.data.DazGraftGroup:
                g_ignore.add(p.a)
                if p.b in body_ignore:
                    body_ignore.remove(p.b)
            self.find_matches(g, g_ignore)

        self.find_matches(self.body, body_ignore)

        fp = self.write_file()
        addon_props = context.scene.daz_hd_morph
        addon_props.geograft_file = fp
        addon_props.base_ob = self.merged.name
        return True
