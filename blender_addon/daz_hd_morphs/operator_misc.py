import bpy, os, json, io
from collections.abc import Mapping
from urllib.parse import unquote
from . import utils


def initialize_tiles_list(context):
    tl = context.scene.daz_hd_morph.texture_tiles
    if len(tl.entries) > 0:
        return
    tl.entries.clear()
    for tn in range(1001, 1011):
        entry = tl.entries.add()
        entry.name = str(tn)
        entry.enabled = True
    tl.index = 0


def get_empty_groups_gen(entries):
    groups = set([e.group for e in entries])
    def generator():
        g = 0
        while True:
            if g not in groups:
                yield g
            g += 1
    return generator()


class TilesListDisableAll(bpy.types.Operator):
    bl_idname = "dazhdmorph.tiles_op_disable_all"
    bl_label = "Disable all tiles"
    bl_description = "Disable all tiles"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        tl = context.scene.daz_hd_morph.texture_tiles
        for entry in tl.entries:
            entry.enabled = False
        return {"FINISHED"}

class TilesListEnableAll(bpy.types.Operator):
    bl_idname = "dazhdmorph.tiles_op_enable_all"
    bl_label = "Enable all tiles"
    bl_description = "Enable all tiles"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        tl = context.scene.daz_hd_morph.texture_tiles
        for entry in tl.entries:
            entry.enabled = True
        return {"FINISHED"}


class MorphFilesDuplicate(bpy.types.Operator):
    bl_idname = "dazhdmorph.morph_files_op_dup"
    bl_label = "Duplicate active item"
    bl_description = "Duplicate active list item"

    @classmethod
    def poll(cls, context):
        return context.scene.daz_hd_morph.morph_files_list.index >= 0

    def execute(self, context):
        mfl = context.scene.daz_hd_morph.morph_files_list
        active_index = mfl.index
        active_item = mfl.entries[active_index]

        entry = mfl.entries.add()
        entry.filepath = active_item.filepath
        entry.group = active_item.group
        entry.weight = active_item.weight
        entry.morph_component = active_item.morph_component

        new_index = active_index + 1
        mfl.entries.move(len(mfl.entries)-1, new_index)
        mfl.index = new_index

        return {"FINISHED"}


class MorphFilesAdd(bpy.types.Operator):
    bl_idname = "dazhdmorph.morph_files_op_add"
    bl_label = "Add file"
    bl_description = "Add file to list"

    files : bpy.props.CollectionProperty( name="Filepaths",
                        type=bpy.types.OperatorFileListElement )

    directory : bpy.props.StringProperty( subtype='DIR_PATH' )

    filter_glob : bpy.props.StringProperty( default="*.dsf",
                                            options={'HIDDEN'} )

    @classmethod
    def poll(cls, context):
        return True

    def invoke(self, context, event):
        default_dir = context.scene.daz_hd_morph.default_morph_dir
        if default_dir and os.path.isdir(default_dir):
            self.directory = default_dir
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

    def execute(self, context):
        if len(self.files) == 0:
            return {'CANCELLED'}
        mfl = context.scene.daz_hd_morph.morph_files_list
        empty_g = get_empty_groups_gen(mfl.entries)
        for f in self.files:
            entry = mfl.entries.add()
            entry.filepath = os.path.normpath( os.path.join(self.directory, f.name) )
            entry.group = next(empty_g)
        mfl.index = len(mfl.entries)-1
        return {"FINISHED"}

class MorphFilesRemove(bpy.types.Operator):
    bl_idname = "dazhdmorph.morph_files_op_remove"
    bl_label = "Remove file"
    bl_description = "Remove selected file from list"

    @classmethod
    def poll(cls, context):
        return context.scene.daz_hd_morph.morph_files_list.index >= 0

    def execute(self, context):
        mfl = context.scene.daz_hd_morph.morph_files_list
        mfl.entries.remove(mfl.index)
        mfl.index -= 1
        return {"FINISHED"}


class MorphFilesClear(bpy.types.Operator):
    bl_idname = "dazhdmorph.morph_files_op_clear"
    bl_label = "Remove all files"
    bl_description = "Remove all files from list"

    @classmethod
    def poll(cls, context):
        return len(context.scene.daz_hd_morph.morph_files_list.entries) > 0

    def execute(self, context):
        mfl = context.scene.daz_hd_morph.morph_files_list
        mfl.entries.clear()
        mfl.index = -1
        return {"FINISHED"}

def get_mfl_groups_dict(entries):
    groups = {}
    for e in entries:
        if e.group not in groups:
            groups[e.group] = [0, 0]
        groups[e.group][0] += e.weight
        groups[e.group][1] += 1
    return groups

class MorphFilesNormalize(bpy.types.Operator):
    bl_idname = "dazhdmorph.morph_files_op_normalize"
    bl_label = "Normalize weights"
    bl_description = "Normalize all weights"

    @classmethod
    def poll(cls, context):
        return len(context.scene.daz_hd_morph.morph_files_list.entries) > 0

    def execute(self, context):
        mfl = context.scene.daz_hd_morph.morph_files_list
        groups = get_mfl_groups_dict(mfl.entries)
        for e in mfl.entries:
            g_sum, g_len = groups[e.group]
            if g_sum < 1e-4:
                e.weight = 1.0/g_len
            else:
                e.weight /= g_sum
        return {"FINISHED"}

class MorphFilesNormalizeUniform(bpy.types.Operator):
    bl_idname = "dazhdmorph.morph_files_op_normalize_unif"
    bl_label = "Uniform weights"
    bl_description = "Uniform normalized weights"

    @classmethod
    def poll(cls, context):
        return len(context.scene.daz_hd_morph.morph_files_list.entries) > 0

    def execute(self, context):
        mfl = context.scene.daz_hd_morph.morph_files_list
        groups = get_mfl_groups_dict(mfl.entries)
        for e in mfl.entries:
            e.weight = 1.0/groups[e.group][1]
        return {"FINISHED"}

class MorphFilesExclusive(bpy.types.Operator):
    bl_idname = "dazhdmorph.morph_files_op_exclusive"
    bl_label = "Enable only selected"
    bl_description = "Set weight of selected to 1 and all others to 0"

    @classmethod
    def poll(cls, context):
        return context.scene.daz_hd_morph.morph_files_list.index >= 0

    def execute(self, context):
        mfl = context.scene.daz_hd_morph.morph_files_list
        for entry in mfl.entries:
            entry.weight = 0
        mfl.entries[mfl.index].weight = 1
        return {"FINISHED"}

def check_base_ob(self, context):
    scn = context.scene
    addon_props = scn.daz_hd_morph
    if addon_props.base_ob not in scn.objects:
        self.report({'ERROR'}, "Base mesh not found in the scene.")
        return None
    base_ob = scn.objects[ addon_props.base_ob ]
    if not base_ob or base_ob.type != 'MESH':
        self.report({'ERROR'}, "Invalid base mesh.")
        return None
    return base_ob

def get_geograft_vcounts(self, context):
    geograft_file = context.scene.daz_hd_morph.geograft_file
    if not geograft_file.strip():
        return True, None

    geograft_file = os.path.abspath( bpy.path.abspath(geograft_file) )
    if not os.path.isfile(geograft_file) or not utils.has_extension(geograft_file, "json"):
        self.report({'ERROR'}, "Invalid geograft file.")
        return False, None

    vcounts = utils.geograft_file_vcounts(geograft_file)
    return True, vcounts

def get_mesh_data(self, context):
    base_ob = check_base_ob(self, context)
    if base_ob is None:
        return None, None
    r, geo_vcounts = get_geograft_vcounts(self, context)
    if not r:
        return None, None
    return base_ob, geo_vcounts


class MorphFilesAddBase(bpy.types.Operator):
    bl_idname = "dazhdmorph.morph_files_add_base"
    bl_label = "Add base mesh morph files"
    bl_description = "Add morph files corresponding to morphs imported on the base mesh (requires \"import_daz\" addon to be installed and enabled)"

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        base_ob = check_base_ob(self, context)
        if base_ob is None:
            return {'CANCELLED'}

        morphs_dirs = utils.api_get_morphs_directories(base_ob)
        if morphs_dirs is None:
            self.report({'ERROR'}, "Required addon \"import_daz\" not found, or api function threw an exception.")
            return {'CANCELLED'}

        base_sks_names, base_sks_files, _ = utils.get_base_sks_info(base_ob, morphs_dirs)
        if len(base_sks_files) == 0:
            self.report({'WARNING'}, "Found no morphs imported on base mesh.")
            return {'CANCELLED'}

        mfl = context.scene.daz_hd_morph.morph_files_list
        empty_g = get_empty_groups_gen(mfl.entries)
        for sk_name, fp in base_sks_files.items():
            entry = mfl.entries.add()
            entry.filepath = os.path.normpath(fp)
            entry.group = next(empty_g)

        self.report({'INFO'}, "{0} morph files added.".format(len(base_sks_files)))
        return {"FINISHED"}


class MorphFilesFavorites(bpy.types.Operator):
    bl_idname = "dazhdmorph.morph_files_op_favorites"
    bl_label = "Add favorite morph files"
    bl_description = "Add morph files from favorite morphs file created by import_daz"

    files : bpy.props.CollectionProperty( name="Filepaths",
                        type=bpy.types.OperatorFileListElement )

    directory : bpy.props.StringProperty( subtype='DIR_PATH' )

    filter_glob : bpy.props.StringProperty( default="*.json",
                                            options={'HIDDEN'} )

    @classmethod
    def poll(cls, context):
        return True

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

    def execute(self, context):
        if len(self.files) == 0:
            return {'CANCELLED'}
        mfl = context.scene.daz_hd_morph.morph_files_list
        empty_g = get_empty_groups_gen(mfl.entries)

        base_ob, geo_vcounts = get_mesh_data(self, context)
        if base_ob is None:
            return {'CANCELLED'}
        base_vcount = len(base_ob.data.vertices)
        base_fingerprint = utils.get_fingerprint(base_ob)

        added_fps = set()
        for f in self.files:
            print("Adding matching morph files in \"{0}\"...".format(f.name))
            fp = os.path.join(self.directory, f.name)
            j = json.load( io.open(fp, 'r', encoding='utf-8-sig') )
            # with open(fp, "r", encoding="utf-8") as f:
            #     j = json.loads(f.read())

            root_paths = None
            if "root_paths" in j:
                root_paths = j["root_paths"]

            morphs_fps = []
            for k, v in j.items():
                if isinstance(v, Mapping) and ("morphs" in v):
                    fingerprint = v.get("finger_print")
                    if not utils.is_fingerprint_match(fingerprint, base_fingerprint, geo_vcounts):
                        continue
                    for category, morph_lst in v["morphs"].items():
                        for e in morph_lst:
                            fp = unquote( e[0] )
                            fp = self.get_existing_fp(root_paths, fp)
                            if fp is None:
                                continue
                            fp = utils.check_dsf_file( fp, base_vcount, geo_vcounts, with_print=True )
                            if (fp is not None) and (fp not in added_fps):
                                morphs_fps.append(fp)
                                added_fps.add(fp)

            for fp in morphs_fps:
                entry = mfl.entries.add()
                entry.filepath = os.path.normpath(fp)
                entry.group = next(empty_g)
            print()

        mfl.index = len(mfl.entries)-1
        self.report({'INFO'}, "{0} morph files added.".format(len(added_fps)))
        return {"FINISHED"}

    @staticmethod
    def get_existing_fp(root_paths, fp):
        if os.path.isfile(fp):
            return fp
        if root_paths is not None:
            if fp.startswith(("/", "\\")):
                fp = fp[1:]
            for dirpath in root_paths:
                fp2 = os.path.join(dirpath, fp)
                if os.path.isfile(fp2):
                    return fp2
        return None


class MorphFilesToggleComponent(bpy.types.Operator):
    bl_idname = "dazhdmorph.morph_files_op_toggle_comp"
    bl_label = "Toggle components"
    bl_description = "Toggle components between 'Both' and 'Only HD'"

    @classmethod
    def poll(cls, context):
        return context.scene.daz_hd_morph.morph_files_list.index >= 0

    def execute(self, context):
        mfl = context.scene.daz_hd_morph.morph_files_list
        for entry in mfl.entries:
            if entry.morph_component == 'HD':
                entry.morph_component = 'BOTH'
                continue
            if entry.morph_component == 'BOTH':
                entry.morph_component = 'HD'
                continue

        return {"FINISHED"}


class MorphFilesClearInvalid(bpy.types.Operator):
    bl_idname = "dazhdmorph.morph_files_op_clear_invalid"
    bl_label = "Clear invalid"
    bl_description = "Remove files from the list which don't match the current base mesh and geograft file"

    @classmethod
    def poll(cls, context):
        return context.scene.daz_hd_morph.morph_files_list.index >= 0

    def execute(self, context):
        base_ob, geo_vcounts = get_mesh_data(self, context)
        if base_ob is None:
            return {'CANCELLED'}
        base_vcount = len(base_ob.data.vertices)

        mfl = context.scene.daz_hd_morph.morph_files_list
        morphs_groups = {}
        to_delete = set()
        for i, e in enumerate(mfl.entries):
            fp = utils.check_dsf_file(e.filepath, base_vcount, geo_vcounts)
            if fp is None:
                to_delete.add(i)
                continue

            if e.group not in morphs_groups:
                morphs_groups[e.group] = []
            for t in morphs_groups[e.group]:
                fp2 = t[0]
                if fp2 == fp:
                    print("  File \"{0}\" listed more than once in the same group.".format(fp))
                    to_delete.add(i)
                    continue
            morphs_groups[e.group].append( [fp, i] )

        single_morphs = set()
        for g, morphs_fps in morphs_groups.items():
            if len(morphs_fps) == 1:
                mfn = os.path.basename( morphs_fps[0][0] )
                if mfn in single_morphs:
                    print("  File \"{0}\" listed more than once in different single-morph groups.".format(mfn))
                    to_delete.add( morphs_fps[0][1] )
                else:
                    single_morphs.add(mfn)
        del single_morphs

        if len(to_delete) > 0:
            to_delete = sorted( list(to_delete), reverse=True )
            for i in to_delete:
                mfl.entries.remove(i)
            mfl.index = 0 if len(mfl.entries) > 0 else -1
            self.report({'INFO'}, "{0} invalid morph files removed (see console).".format(len(to_delete)))
        else:
            self.report({'INFO'}, "No invalid morphs files found.")
        return {"FINISHED"}
