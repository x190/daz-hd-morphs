import bpy, os
from . import utils

def generate_simple_hd_mesh(context, ob, hd_level, gScale, outputDirpath):
    addon_props = context.scene.daz_hd_morph

    settings = {"unit_scale": gScale,
                "working_dirpath": outputDirpath,
                "base_ob": ob.name,
                "generate_mode": 'UNRIGGED',
                "hd_level": hd_level,
                "copy_non_hd_meshes": False,
                "use_morph_list": False,
                "base_modifiers": 'NONE'}

    settings_saved = {}
    for k, v in settings.items():
        settings_saved[k] = getattr(addon_props, k)
        setattr(addon_props, k, v)

    try:
        cs_names = [c_name for c_name in bpy.data.collections]
        r = bpy.ops.dazhdmorph.generatehd()
        if ('FINISHED' in r):
            hd_ob = context.active_object
            if hd_ob.type != 'MESH':
                print("bpy.ops.dazhdmorph.generatehd() did not finish successfully.")
                return None
            utils.move_to_collection(hd_ob, None)
            cs_to_remove = [c_name for c_name in bpy.data.collections if c_name not in cs_names]
            for c_name in cs_to_remove:
                bpy.data.collections.remove(c_name)
            return hd_ob
        else:
            print("bpy.ops.dazhdmorph.generatehd() did not finish successfully.")
            return None
    except Exception as e:
        print("bpy.ops.dazhdmorph.generatehd() threw an exception.")
        return None
    finally:
        for k, v in settings_saved.items():
            setattr(addon_props, k, v)
