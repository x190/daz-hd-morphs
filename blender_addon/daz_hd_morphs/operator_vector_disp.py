import bpy, os, json, time
from . import dll_wrapper
from . import utils
from .operator_common import HDMorphOperator


class GenerateVectorDispOperator(HDMorphOperator):
    """Generate vector displacement textures from morph/s"""
    bl_idname = "dazhdmorph.vecdisp"
    bl_label = "Generate textures"

    base_modifiers = None
    morph_base_until = None

    def execute(self, context):
        if not self.check_input(context):
            return {'CANCELLED'}
        if not self.check_morphs_list(context):
            return {'CANCELLED'}

        addon_props = context.scene.daz_hd_morph
        self.base_modifiers = addon_props.base_modifiers
        self.morph_base_until = addon_props.morph_base_until
        if not self.check_input_vdisp(context):
            return {'CANCELLED'}

        t0 = time.perf_counter()
        r = self.generate_maps(context)
        self.cleanup(context)
        if not r:
            return {'CANCELLED'}

        self.report({'INFO'}, "Vector displacement maps generated.")
        print("Elapsed: {}".format(time.perf_counter() - t0))
        return {'FINISHED'}

    def finish_vdisp_json(self, outputDirpath, morphs_groups_basenames):
        basenames_group = {}
        for g, base_name in morphs_groups_basenames.items():
            assert( base_name not in basenames_group )
            basenames_group[base_name] = g

        fps = self.get_filelist_fps(outputDirpath)
        for fp in fps:
            if not os.path.isfile(fp):
                continue
            fp_json = fp + ".json"
            if not os.path.isfile(fp_json):
                continue
            with open(fp_json, "r", encoding="utf-8") as f:
                d = json.load(f)

            f_id = d.get("id")
            if f_id is None:
                raise RuntimeError("File \"{0}\" is invalid.".format(fp_json))
            g = basenames_group.get(f_id)
            if g is None:
                raise ValueError("id of \"{0}\" (\"{1}\") is invalid morph group name.".format(fp_json, f_id))
            morphs_list = self.morphs_groups[g]
            d["morphs_filepaths"] = [t[0] for t in morphs_list]
            d["morphs_weights"] = [t[1] for t in morphs_list]
            d["morphs_components"] = [t[2] for t in morphs_list]
            d["uv_layer_name"] = self.uv_layer_name

            with open(fp_json, "w", encoding="utf-8") as f:
                json.dump(d, f, indent=4)

        return True

    def generate_maps(self, context):
        base_exportedf = self.export_base_dae( context, self.base_modifiers,
                                               include_weights=False,
                                               with_obj=True,
                                               uv_layer=self.vdisp_ts.uv_layer )

        outputDirpath = self.create_vdm_subdir()

        morphs_groups_basenames = utils.getMorphsGroupsNames( self.morphs_groups )

        dll_wrapper.execute_in_new_thread( "generate_disp_morphs",
                                           self.gScale, base_exportedf, self.geograft_file,
                                           self.morphs_groups, morphs_groups_basenames,
                                           self.vdisp_ts, outputDirpath,
                                           self.morph_base_until )

        if not self.finish_vdisp_json(outputDirpath, morphs_groups_basenames):
            return False
        return True


class GenerateVectorDispObjObjOperator(HDMorphOperator):
    bl_description = "Generate vector displacement textures from HD mesh's shape.\n"\
                     "If the base mesh and the HD mesh have the same vertex count, they must have the same vertex order.\n"\
                     "Otherwise, the HD mesh must have Blender's subdivision vertex order"
    bl_idname = "dazhdmorph.vecdispobjobj"
    bl_label = "Generate textures"

    base_subd_method = None

    def execute(self, context):
        if not self.check_input(context, check_hd=True):
            return {'CANCELLED'}

        addon_props = context.scene.daz_hd_morph
        self.base_subd_method = addon_props.base_subdiv_method
        if not self.check_input_vdisp(context):
            return {'CANCELLED'}

        t0 = time.perf_counter()
        if self.base_subd_method != 'DAZ':
            r = self.generate_maps(context)
        else:
            r = self.generate_maps_daz(context)
        self.cleanup(context)
        if not r:
            return {'CANCELLED'}

        self.report({'INFO'}, "Vector displacement maps generated.")
        print("Elapsed: {}".format(time.perf_counter() - t0))
        return {'FINISHED'}

    def finish_vdisp_json(self, outputDirpath, base_name):
        fps = self.get_filelist_fps(outputDirpath)
        for fp in fps:
            if not os.path.isfile(fp):
                continue
            fp_json = fp + ".json"
            if not os.path.isfile(fp_json):
                continue
            with open(fp_json, "r", encoding="utf-8") as f:
                d = json.load(f)

            f_id = d.get("id")
            if f_id is None:
                raise RuntimeError("File \"{0}\" is invalid.".format(fp_json))
            if f_id != base_name:
                raise ValueError("id of \"{0}\" (\"{1}\") is invalid.".format(fp_json, f_id))
            d["subdiv_method"] = self.base_subd_method
            d["base_mesh_name"] = self.base_ob.name
            d["hd_mesh_name"] = self.hd_ob.name
            d["uv_layer_name"] = self.uv_layer_name

            with open(fp_json, "w", encoding="utf-8") as f:
                json.dump(d, f, indent=4)

        return True

    def get_level_subdivision_modifier(self, ob):
        subd_level = 0
        subd_m = None
        for m in ob.modifiers:
            if m.type in ('SUBSURF', 'MULTIRES'):
                if subd_m is not None:
                    self.report({'ERROR'}, "hd mesh has more than 1 subdivision modifier.")
                    return -1, None
                if m.type == 'SUBSURF':
                    subd_level = m.levels
                else:
                    m.levels = m.total_levels
                    subd_level = m.total_levels
                subd_m = m
        return subd_level, subd_m

    def generate_maps(self, context):
        subd_diff = utils.get_subdivision_level(self.base_ob, self.hd_ob)
        if subd_diff < 0:
            self.report({'ERROR'}, "Base mesh is more subdivided than hd mesh.")
            return False
        hd_mod_subd_level, hd_subd_m = self.get_level_subdivision_modifier(self.hd_ob)
        if hd_mod_subd_level < 0:
            return False

        base2_ob = None
        base_ms = None
        if self.base_subd_method == 'NONE':
            if subd_diff != 0:
                self.report({'ERROR'}, "Base mesh and hd mesh vertex count don't match.")
                return False
            if hd_subd_m is not None:
                self.report({'ERROR'}, "hd mesh has subdivision modifier.")
                return False
            base2_ob = self.base_ob
            base_ms = utils.ModifiersStatus(base2_ob, 'DISABLE', m_types={'ALL'})
        else:
            subd_type = None
            subd_levels = None
            subd_limit_surf = None
            if self.base_subd_method == 'MODIFIER':
                if hd_subd_m is None:
                    self.report({'ERROR'}, "hd mesh has no subdivision modifier.")
                    return False
                if subd_diff != 0:
                    self.report({'ERROR'}, "hd mesh is subdivided and also has a subdivision modifier.")
                    return False
                subd_type = hd_subd_m.type
                subd_levels = hd_mod_subd_level
                if subd_type != 'MULTIRES':
                    subd_limit_surf = (hd_subd_m.use_limit_surface)
            else:   # SUBSURF_LIMIT, MULTIRES, SUBSURF
                if hd_subd_m is not None:
                    self.report({'ERROR'}, "hd mesh has subdivision modifier.")
                    return False
                if subd_diff == 0:
                    self.report({'ERROR'}, "hd mesh is not subdivided.")
                    return False
                subd_type = self.base_subd_method
                subd_levels = subd_diff
                subd_limit_surf = (self.base_subd_method == 'SUBSURF_LIMIT')

            base2_ob = utils.copy_object(self.base_ob)
            # ~ utils.remove_division_modifiers(base2_ob)
            base2_ob.modifiers.clear()
            if subd_type == 'MULTIRES':
                mr = utils.create_multires_modifier(base2_ob)
                utils.make_single_active(base2_ob)
                for _ in range(0, subd_levels):
                    bpy.ops.object.multires_subdivide(modifier=mr.name, mode='CATMULL_CLARK')
                assert(mr.total_levels == subd_levels)
            else:
                sd = utils.create_subsurf_modifier(base2_ob, use_limit=subd_limit_surf)
                sd.levels = subd_levels

        f_name = "base_hd"
        base_hd_exportedf = self.export_ob_obj( base2_ob, f_name, apply_modifiers=True,
                                                uv_layer=self.vdisp_ts.uv_layer )

        hd_ob_ms = utils.ModifiersStatus(self.hd_ob, 'ENABLE_ONLY', m_types={'SUBDIV'})
        f_name += "_2"
        hd2_exportedf = self.export_ob_obj( self.hd_ob, f_name, apply_modifiers=True,
                                            uv_layer=None )
        hd_ob_ms.restore()
        del hd_ob_ms

        outputDirpath = self.create_vdm_subdir()
        base_name = "blender_{0}".format(utils.makeValidFilename(self.hd_ob.name))
        dll_wrapper.execute_in_new_thread( "generate_disp_blender",
                                           self.gScale, base_hd_exportedf, 0,
                                           self.vdisp_ts, outputDirpath,
                                           base_name )

        if self.base_subd_method != 'NONE':
            utils.delete_object(base2_ob)
        else:
            base_ms.restore()

        if not self.finish_vdisp_json(outputDirpath, base_name):
            return False
        return True

    def generate_maps_daz(self, context):
        subd_diff = utils.get_subdivision_level(self.base_ob, self.hd_ob)
        if subd_diff < 0:
            self.report({'ERROR'}, "Base mesh is more subdivided than hd mesh.")
            return False
        _, hd_subd_m = self.get_level_subdivision_modifier(self.hd_ob)
        if hd_subd_m is not None:
            self.report({'ERROR'}, "hd mesh has subdivision modifier.")
            return False

        base_exportedf = self.export_base_obj( context, 'SK', uv_layer=self.vdisp_ts.uv_layer )

        hd_ob_ms = utils.ModifiersStatus(self.hd_ob, 'DISABLE', m_types={'ALL'})
        f_name = os.path.basename(base_exportedf).rsplit(".", 1)[0] + "_2"
        hd_exportedf = self.export_ob_obj( self.hd_ob, f_name, apply_modifiers=True,
                                           uv_layer=None )
        hd_ob_ms.restore()
        del hd_ob_ms

        outputDirpath = self.create_vdm_subdir()
        base_name = "blender_{0}".format(utils.makeValidFilename(self.hd_ob.name))

        dll_wrapper.execute_in_new_thread( "generate_disp_blender",
                                           self.gScale, base_exportedf, subd_diff,
                                           self.vdisp_ts, outputDirpath,
                                           base_name )

        if not self.finish_vdisp_json(outputDirpath, base_name):
            return False
        return True
