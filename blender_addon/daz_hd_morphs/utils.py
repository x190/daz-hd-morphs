import bpy, os, math, time, gzip, json, re
from urllib.parse import unquote
from mathutils import Vector


def has_extension(filepath, *exts):
    assert(len(exts) > 0)
    t = os.path.basename(filepath).rsplit(".", 1)
    return (len(t) == 2 and t[1].lower() in exts)

def get_extension(filepath):
    t = os.path.basename(filepath).rsplit(".", 1)
    if len(t) != 2:
        return None
    return t[1].lower()

def remove_extension(filename):
    t = filename.rsplit(".", 1)
    return t[0]

def makeValidFilename(name):
    name = name.strip().lower()
    return "".join( c for c in name if (c.isalnum() or c in "._- ") )

def getMorphName(morph_filepath):
    n = os.path.basename(morph_filepath)
    t = n.rsplit(".", 1)
    return makeValidFilename( "_".join(t) )

def getMorphsGroupsNames(morphs_groups):
    names_created = set()
    morphs_groups_names = {}
    for g, morph_list in morphs_groups.items():
        if len(morph_list) == 0:
            raise ValueError("Morph group {0} is empty.".format(g))
        elif len(morph_list) == 1:
            name = get_file_id( morph_list[0][0] )
        else:
            name = "morphs_group_{0}".format(g)
        morphs_groups_names[g] = name
        if name in names_created:
            raise ValueError("Two or more morphs groups share the same morph group name: \"{0}\".".format(name))
        names_created.add(name)
    return morphs_groups_names

def delete_object(ob):
    assert(ob.type == 'MESH')
    ob_data = ob.data
    ob_mats = set(ob_data.materials)
    bpy.data.objects.remove(ob, do_unlink=True, do_id_user=True, do_ui_user=True)
    if ob_data is not None and ob_data.users == 0:
        bpy.data.meshes.remove(ob_data, do_unlink=True, do_id_user=True, do_ui_user=True)
    for m in ob_mats:
        if m is not None and m.users == 0:
            bpy.data.materials.remove(m, do_unlink=True, do_id_user=True, do_ui_user=True)

def delete_armature(ob):
    assert(ob.type == 'ARMATURE')
    ob_data = ob.data
    bpy.data.objects.remove(ob, do_unlink=True, do_id_user=True, do_ui_user=True)
    if ob_data is not None and ob_data.users == 0:
        bpy.data.armatures.remove(ob_data, do_unlink=True, do_id_user=True, do_ui_user=True)

def delete_object_materials(ob):
    mats = set(ob.data.materials)
    ob.data.materials.clear()
    for m in mats:
        if m is not None and m.users == 0:
            bpy.data.materials.remove(m, do_unlink=True, do_id_user=True, do_ui_user=True)

def make_single_active(ob):
    bpy.ops.object.select_all(action='DESELECT')
    ob.select_set(True)
    bpy.context.view_layer.objects.active = ob

def make_final_active(ob):
    arm = get_armature(ob)
    if arm:
        make_single_active(arm)
    else:
        make_single_active(ob)

def import_dll_obj(fp):
    if not has_extension(fp, "obj"):
        raise ValueError("File \"{0}\" is not a .obj file.".format(fp))
    if not os.path.isfile(fp):
        raise ValueError("File \"{0}\" not found.".format(fp))
    bpy.ops.object.select_all(action='DESELECT')
    bpy.ops.wm.obj_import( filepath=fp, check_existing=False,
                           import_vertex_groups=False, validate_meshes=False,
                           forward_axis='Y', up_axis='Z' )
    hd_ob = bpy.context.selected_objects[0]
    bpy.ops.object.shade_smooth()
    return hd_ob

def import_dll_collada(fp):
    if not has_extension(fp, "dae"):
        raise ValueError("File \"{0}\" is not a .dae file.".format(fp))
    if not os.path.isfile(fp):
        raise ValueError("File \"{0}\" not found.".format(fp))
    bpy.ops.object.select_all(action='DESELECT')
    bpy.ops.wm.collada_import( filepath=fp, import_units=False, custom_normals=False,
                               fix_orientation=False, find_chains=False, auto_connect=False,
                               min_chain_length=0, keep_bind_info=False )
    hd_ob = None
    for ob in bpy.context.selected_objects:
        if ob.type == 'ARMATURE':
            delete_armature(ob)
        else:
            remove_armature_modifiers(ob)
            hd_ob = ob
    bpy.ops.object.shade_smooth()
    return hd_ob

def add_collection(c_name):
    if c_name not in bpy.data.collections:
        bpy.data.collections.new(c_name)
    c = bpy.data.collections[c_name]
    if c.name not in bpy.context.scene.collection.children:
        bpy.context.scene.collection.children.link(c)
    return c

def move_to_collection(ob, c_names):
    c_dests = []
    if c_names is not None and len(c_names) > 0 and c_names[0] is not None:
        for c_name in c_names:
            c = add_collection(c_name)
            c_dests.append(c)
    else:
        c_dests.append( bpy.context.window.view_layer.layer_collection.collection )
    for c in ob.users_collection:
        c.objects.unlink(ob)
    for c in c_dests:
        c.objects.link(ob)

def copy_object(ob, collection_name=None):
    ob2 = ob.copy()
    ob2.data = ob.data.copy()
    move_to_collection(ob2, [collection_name])
    return ob2

def get_armature(ob):
    for m in ob.modifiers:
        if m.type == 'ARMATURE':
            return m.object
    return None

def parent_to(ob_h, ob_p):
    mat_world = ob_h.matrix_world.copy()
    ob_h.parent = ob_p
    ob_h.matrix_world = mat_world

def copy_armature( src, dst,
                   collection_name=None,
                   move_to_top=False,
                   copy_non_hd_meshes=True ):
    arm = get_armature(src)
    if arm is not None:
        make_single_active(arm)
        if copy_non_hd_meshes:
            for ob in arm.children_recursive:
                if ob.type == 'MESH' and ob != src:
                    ob.select_set(True)

        # ~ arm2 = copy_object(arm, collection_name)
        bpy.ops.object.duplicate(linked=False)

        obs_copied = []
        arm2 = None
        for ob in bpy.context.selected_objects:
            if ob.type == 'ARMATURE':
                arm2 = ob
            elif ob.type == 'MESH':
                obs_copied.append(ob)
            else:
                raise ValueError("Unexpected object type: \"{0}\".".format(ob.type))

        assert(arm2 is not None)
        move_to_collection(arm2, [collection_name])
        arm2.location = dst.location
        for ob in obs_copied:
            move_to_collection(ob, [collection_name])

        make_single_active(arm2)
        dst.select_set(True)
        bpy.ops.object.parent_set(type='ARMATURE')
        make_single_active(dst)
        for m in dst.modifiers:
            if m.type == 'ARMATURE':
                m.use_deform_preserve_volume = True
                if move_to_top:
                    bpy.ops.object.modifier_move_to_index(modifier=m.name, index=0)

def copy_non_hd_meshes( src, dst, collection_name=None ):
    arm = get_armature(src)
    if arm is not None:
        bpy.ops.object.select_all(action='DESELECT')
        for ob in arm.children_recursive:
            if ob.type == 'MESH' and ob != src:
                ob.select_set(True)

        # ~ diff = Vector(dst.location) - Vector(src.location)
        bpy.ops.object.duplicate(linked=False)
        obs_copied = [ ob for ob in bpy.context.selected_objects ]
        for ob in obs_copied:
            make_single_active(ob)
            if ob.type == 'MESH' and ob.data.shape_keys is not None:
                bpy.ops.object.shape_key_remove(all=True, apply_mix=True)
            move_to_collection(ob, [collection_name])

            has_armature = False
            to_apply_names = []
            for m in ob.modifiers:
                to_apply_names.append(m.name)
                if m.type == 'ARMATURE':
                    has_armature = True
                    break

            if has_armature:
                # ~ make_single_active(ob)
                for m_name in to_apply_names:
                    bpy.ops.object.modifier_apply(modifier = m_name)

            if ob.parent == arm or ob.parent == src:
                # ~ ob.location = Vector(ob.location) + diff
                parent_to( ob, dst )

def create_unsubdivide_multires(ob):
    if len(ob.modifiers) != 0 or ob.data.shape_keys is not None:
            raise RuntimeError("Object has modifiers or shape keys")
        # ~ ob.modifiers.clear()
        # ~ remove_shape_keys(ob)

    mr = ob.modifiers.new("hd_multires", 'MULTIRES')

    mr.show_only_control_edges = True
    mr.quality = 4
    # ~ mr.subdivision_type = 'CATMULL_CLARK'
    mr.uv_smooth = 'PRESERVE_CORNERS'
    mr.boundary_smooth = 'ALL'
    mr.use_creases = True
    mr.use_custom_normals = False

    make_single_active(ob)
    bpy.ops.object.multires_rebuild_subdiv(modifier=mr.name)
    # ~ bpy.ops.object.multires_unsubdivide(modifier=mr.name)
    # ~ mr.levels = 0
    bpy.ops.object.select_all(action='DESELECT')

def create_subsurf_modifier(ob, use_limit=False):
    sd = ob.modifiers.new("daz_hd_morphs_subsurf", 'SUBSURF')
    sd.show_only_control_edges = True
    sd.use_limit_surface = use_limit
    sd.uv_smooth = 'PRESERVE_CORNERS'
    sd.boundary_smooth = 'ALL'
    sd.use_creases = True
    sd.use_custom_normals = False
    return sd

def create_multires_modifier(ob):
    mr = ob.modifiers.new("daz_hd_morphs_multires", 'MULTIRES')
    mr.show_only_control_edges = True
    mr.quality = 4
    # ~ mr.subdivision_type = 'CATMULL_CLARK'
    mr.uv_smooth = 'PRESERVE_CORNERS'
    mr.boundary_smooth = 'ALL'
    mr.use_creases = True
    mr.use_custom_normals = False
    return mr

def merge_materials_by_slot(src_ob, dst_ob):
    src_mats = {}
    for i, ms in enumerate(src_ob.material_slots):
        src_mats[i] = ms.material

    for ms in dst_ob.material_slots:
        if ms.name.startswith("SLOT_"):
            slot = ms.name[5:].strip().rsplit(".",1)[0]
            if slot.isdigit():
                slot = int(slot)
                if slot in src_mats:
                    mat = ms.material
                    ms.material = src_mats[slot]
                    #if mat.users == 0:
                    bpy.data.materials.remove(mat, do_unlink=True, do_id_user=True, do_ui_user=True)

def get_subdivision_level(base_ob, hd_ob):
    # ~ nt = 0
    # ~ nq = 0
    # ~ for p in base_ob.data.polygons:
        # ~ nv = len(p.vertices)
        # ~ if nv == 4:
            # ~ nq += 1
        # ~ elif nv == 3:
            # ~ nt += 1

    # ~ nq2 = len(hd_ob.data.polygons)
    # ~ level = int( math.log(nq2/(4*nq + 3*nt)) / math.log(4) ) + 1

    # this only works right for mixtures of triangles and quads (no n-gons):
    nq = len(base_ob.data.polygons)
    nq2 = len(hd_ob.data.polygons)
    level = round( math.log(nq2/nq) / math.log(4) )

    return level

def remove_armature_modifiers(ob):
    for m in ob.modifiers:
        if m.type == 'ARMATURE':
            ob.modifiers.remove(m)

def remove_division_modifiers(ob):
    for m in ob.modifiers:
        if m.type in ('SUBSURF', 'MULTIRES'):
            ob.modifiers.remove(m)

def remove_shape_keys(ob):
    if ob.data.shape_keys is not None and len(ob.data.shape_keys.key_blocks) > 0:
        ob.active_shape_key_index = 0
        ob.shape_key_clear()

def read_dhdm_level(dhdm_fp):
    if not os.path.isfile(dhdm_fp):
        raise ValueError("File \"{}\" not found.".format(dhdm_fp))
    with open(dhdm_fp, "rb") as f:
        h = f.read(8)
    return int.from_bytes(h[4:], byteorder='little', signed=False)

def is_gzip_file(fp, strict=True):
    with open(fp, "rb") as f:
        if strict:
            return f.read(3) == b'\x1f\x8b\x08'
        return f.read(2) == b'\x1f\x8b'

def get_dsf_json(dsf_fp):
    print("Reading file \"{0}\"...".format(dsf_fp))
    try:
        with gzip.open(dsf_fp, "rt", encoding="utf-8") as f:
            return json.loads(f.read())
    except gzip.BadGzipFile:
        try:
            with open(dsf_fp, "r", encoding="utf-8") as f:
                return json.loads(f.read())
        except OSError as e:
            print("Error while reading file \"{0}\".".format(dsf_fp))
            raise e

def read_dsf_level(dsf_fp):
    j = get_dsf_json(dsf_fp)
    try:
        dhdm = j["modifier_library"][0]["morph"]["hd_url"]
        dhdm_fp = os.path.join( os.path.dirname(dsf_fp), os.path.basename(dhdm) )
        dhdm_fp = unquote(dhdm_fp)
        return read_dhdm_level(dhdm_fp)
    except KeyError:
        return 0

def read_dsf_id(dsf_fp, only_with_dhdm=False):
    j = get_dsf_json(dsf_fp)
    try:
        if only_with_dhdm:
            dhdm = j["modifier_library"][0]["morph"]["hd_url"]
        j["modifier_library"][0]["morph"]
        return j["modifier_library"][0]["id"]
    except KeyError:
        return None

def dsf_vcount(dsf_fp):
    j = get_dsf_json(dsf_fp)
    try:
        v_count = j["modifier_library"][0]["morph"]["vertex_count"]
        return v_count
    except KeyError:
        return None

def geograft_file_vcounts(geograft_file):
    with open(geograft_file, "r", encoding="utf-8") as f:
        d = json.load(f)
        return set([int(k) for k in d])

def get_morph_groups_levels(morph_groups, max_subd):
    tot_groups = 0
    groups_levels = {}
    for g, tuples in morph_groups.items():
        level = 0
        for t in tuples:
            fp = t[0]
            if has_extension(fp, "dhdm"):
                level = max(level, read_dhdm_level(fp))
            else:
                level = max(level, read_dsf_level(fp))
        if max_subd > 0:
            level = min(level, max_subd)
        if level == 0:
            continue
        if level not in groups_levels:
            groups_levels[level] = []
        groups_levels[level].append(g)
        tot_groups += 1

    groups_by_level = []
    for level in sorted(groups_levels.keys()):
        groups_by_level.append( (level, groups_levels[level]) )
    return groups_by_level, tot_groups

def get_file_id(filepath):
    filename = os.path.basename(filepath).rsplit(".",1)[0]

    if has_extension(filepath, "dhdm"):
        dsf_fp = os.path.join(os.path.dirname(filepath), filename + ".dsf")
        if not os.path.isfile(dsf_fp):
            return filename
        filepath = dsf_fp
    elif not has_extension(filepath, "dsf"):
        raise ValueError("File \"{0}\" is not .dsf or .dhdm file.".format(filepath))

    dsf_id = read_dsf_id(filepath)
    if dsf_id is not None:
        return dsf_id
    return filename

def get_driven_sks_names(ob):
    driven_sks_names = set()
    ob_sk = ob.data.shape_keys
    if ob_sk is not None and ob_sk.animation_data is not None:
        exp = re.compile(r"key_blocks\[\"([^\"]+)\"\]\.value", re.IGNORECASE)
        for fcu in ob_sk.animation_data.drivers:
            r = exp.search(fcu.data_path)
            if r is not None:
                driven_sks_names.add( r.group(1) )
    return driven_sks_names

def get_sk_names(ob, exclude_driven=False):
    if ob.data.shape_keys is None:
        return []

    driven_sks_names = set()
    if exclude_driven:
        driven_sks_names = get_driven_sks_names(ob)

    sks = []
    for sk in ob.data.shape_keys.key_blocks[1:]:
        if not exclude_driven or sk.name not in driven_sks_names:
            sks.append(sk.name)

    return sks

def get_sk_pairs(src_ob, dst_ob, exclude_driven=False):
    dst_sks = get_sk_names(dst_ob, exclude_driven)
    src_sks = get_sk_names(src_ob)
    if len(src_sks) == 0 or len(dst_sks) == 0:
        return {}
    src_sks_map = {}
    for sk_name in src_sks:
        src_sks_map[sk_name.lower()] = sk_name
    pairs = {}
    for sk_name in dst_sks:
        sk_name2 = sk_name.lower()
        src_sk_name = src_sks_map.get(sk_name2)
        if src_sk_name is not None:
            pairs[src_sk_name] = sk_name
    return pairs

def copy_sk_drivers(src_ob, dst_ob, sks_pairs):
    def get_sk_data_path(sk_name):
        return "key_blocks[\"{0}\"].value".format(sk_name)

    if len(sks_pairs) == 0:
        return

    src_armat = get_armature(src_ob)
    dst_armat = get_armature(dst_ob)
    if src_armat is None or dst_armat is None:
        return

    src_ob_sk = src_ob.data.shape_keys
    dst_ob_sk = dst_ob.data.shape_keys
    if src_ob_sk is None or src_ob_sk.animation_data is None:
        return
    if dst_ob_sk is None:
        return

    dst_fcu_dps = set()
    if dst_ob_sk.animation_data is None:
        dst_ob_sk.animation_data_create()
    else:
        for fcu in dst_ob_sk.animation_data.drivers:
            dst_fcu_dps.add(fcu.data_path)

    sk_dps = {}
    for src_sk_name, dst_sk_name in sks_pairs.items():
        src_sk_dp = get_sk_data_path(src_sk_name)
        dst_sk_dp = get_sk_data_path(dst_sk_name)
        sk_dps[src_sk_dp] = dst_sk_dp

        src_sk_dp_mute = src_sk_dp[:-5] + "mute"
        dst_sk_dp_mute = dst_sk_dp[:-5] + "mute"
        sk_dps[src_sk_dp_mute] = dst_sk_dp_mute

    for fcu in src_ob_sk.animation_data.drivers:
        if fcu.data_path in sk_dps:
            dst_fcu_dp = sk_dps[fcu.data_path]
            if dst_fcu_dp in dst_fcu_dps:
                continue
            dst_fcu = dst_ob_sk.animation_data.drivers.from_existing(src_driver=fcu)
            dst_fcu.data_path = dst_fcu_dp
            for v in dst_fcu.driver.variables:
                for trg in v.targets:
                    if trg.id_type == 'OBJECT' and trg.id == src_armat:
                        trg.id = dst_armat
                    elif trg.id_type == 'ARMATURE' and trg.id == src_armat.data:
                        trg.id = dst_armat.data


def copy_uv_layers_names(base_ob, hd_ob):
    for i, layer in enumerate(base_ob.data.uv_layers):
        if i >= len(hd_ob.data.uv_layers):
            return
        hd_ob.data.uv_layers[i].name = layer.name

def get_uv_layer_name(ob, uv_index):
    return ob.data.uv_layers[uv_index].name

def get_selected_meshes(context):
    return [ ob for ob in context.view_layer.objects
             if ob.select_get() and ob.type == 'MESH'
                and not (ob.hide_get() or ob.hide_viewport) ]

def set_active_uv_layer(ob, n):
    if n >= len(ob.data.uv_layers):
        raise ValueError("Object \"{}\" has no uv layer with index {}".format(ob.name, n))
    ob.data.uv_layers.active_index = n

def get_active_uv_layer(ob):
    return ob.data.uv_layers.active_index

def delete_uv_layers(ob, preserve=[]):
    preserve = set(preserve)
    to_remove = []
    for i, layer in enumerate(ob.data.uv_layers):
        if i not in preserve:
            to_remove.append(layer)
    for layer in to_remove:
        ob.data.uv_layers.remove(layer)

def copy_vertex_groups(ob1, ob2):
    make_single_active(ob2)
    ob1.select_set(True)
    bpy.context.view_layer.objects.active = ob1
    bpy.ops.object.vertex_group_copy_to_selected()

def shapekey_from_ob(ob, ob2, sk_name=None):
    make_single_active(ob2)
    ob.select_set(True)
    bpy.context.view_layer.objects.active = ob
    bpy.ops.object.join_shapes()
    if sk_name is not None:
        ob.data.shape_keys.key_blocks[-1].name = sk_name

def shapekey_from_obj(filepath, ob, sk_name=None):
    ob2 = import_dll_obj(filepath)
    shapekey_from_ob(ob, ob2, sk_name=sk_name)
    delete_object(ob2)

def shapekey_from_collada(filepath, ob, sk_name=None):
    ob2 = import_dll_collada(filepath)
    shapekey_from_ob(ob, ob2, sk_name=sk_name)
    delete_object(ob2)

def fix_vertex_groups_spaces( ob_base, ob_hd ):
    armature_base = get_armature(ob_base)
    if armature_base is None:
        return True

    has_spaces = False
    b_names = set()
    for b in armature_base.data.bones:
        b_names.add(b.name)
        if not has_spaces and ' ' in b.name:
            has_spaces = True

    if not has_spaces:
        return True

    exported_vgs = []
    for vg in ob_base.vertex_groups:
        if vg.name in b_names:
            exported_vgs.append(vg.name)

    if len(exported_vgs) != len(ob_hd.vertex_groups):
        print("Vertex groups' name matching was unsuccessful, "
              "some vertex groups on the hd mesh "
              "might require manual renaming to match "
              "the corresonding bones' names (look for differences "
              "in spaces and underscores).")
        return False

    for i, vg in enumerate(ob_hd.vertex_groups):
        vg.name = exported_vgs[i]
    return True

def get_fingerprint(ob):
    if ob.type != 'MESH':
        raise ValueError("Object \"{0}\" is not a mesh.".format(ob.name))
    return "{0}-{1}-{2}".format( len(ob.data.vertices), len(ob.data.edges), len(ob.data.polygons) )

def is_fingerprint_match(fingerprint, base_fingerprint, geo_vcounts):
    if fingerprint is None:
        return False
    if fingerprint == base_fingerprint:
        return True
    if geo_vcounts is None:
        return False
    try:
        fingerprint_vcount = fingerprint.split("-", 1)[0]
        return fingerprint_vcount in geo_vcounts
    except Exception as e:
        return False

def check_dsf_file(fp, base_vcount, geo_vcounts, with_print=True):
    fp = fp.strip()
    if not fp:
        return None
    if not has_extension(fp, "dsf"):
        if with_print:
            print("  File \"{0}\" has no .dsf extension.".format(fp))
        return None
    fp = os.path.abspath( bpy.path.abspath( fp ) )
    if not os.path.isfile(fp):
        if with_print:
            print("  File \"{0}\" not found.".format(fp))
        return None

    v_count = dsf_vcount(fp)
    if v_count is None:
        if with_print:
            print("  File \"{0}\" describes no geometry.".format(fp))
        return None
    if v_count >= 0:
        if geo_vcounts is None:
            if v_count != base_vcount:
                if with_print:
                    print("  File \"{0}\" vertex count mismatch with base mesh.".format(fp))
                return None
        elif v_count not in geo_vcounts:
            if with_print:
                print("  File \"{0}\" vertex count mismatch with all geografts.".format(fp))
            return None
    return fp

def get_base_sks_info(base_ob, morphs_dirs, base_sks_force_blender=False):
    base_sks = get_sk_names(base_ob)
    if len(base_sks) == 0:
        return {}, {}, {}
    base_sks_map = {}
    for k in base_sks:
        base_sks_map[k.lower()] = k
    del base_sks

    base_sks_names = {}
    base_sks_files = {}
    daz_sks_k = set()
    for d in morphs_dirs:
        if not os.path.isdir(d):
            print("Directory \"{0}\" does not exist.".format(d))
            continue

        for root, dirs, files in os.walk(d):
            for f in files:
                if has_extension(f, "dsf"):
                    fp = os.path.join(root, f)
                    dsf_id = read_dsf_id(fp, only_with_dhdm=base_sks_force_blender)
                    if dsf_id is not None:
                        k = dsf_id.lower()
                        src_sk_name = base_sks_map.get( k, None )
                        if src_sk_name is not None:
                            daz_sks_k.add(k)
                            base_sks_files[src_sk_name] = fp
                            base_sks_names[src_sk_name] = src_sk_name
            break

    base_sks_objs = {}
    if len(daz_sks_k) != len(base_sks_map):
        for k, sk_name in base_sks_map.items():
            if k not in daz_sks_k:
                base_sks_objs[sk_name] = sk_name
    return base_sks_names, base_sks_files, base_sks_objs


def recopy_armature(base_ob, hd_ob):
    base_arm = get_armature(base_ob)
    hd_arm = get_armature(hd_ob)

    if base_arm is None or hd_arm is None:
        return False

    hd_arm_name_0 = str(hd_arm.name)
    hd_arm_data_name_0 = str(hd_arm.data.name)

    hd_arm.name = hd_arm.name + "_0"
    hd_arm.data.name = hd_arm.data.name + "_0"

    make_single_active(base_arm)
    bpy.ops.object.duplicate(linked=False)
    hd_arm2 = bpy.context.selected_objects[0]

    hd_arm2.location = hd_arm.location
    hd_arm2.name = hd_arm_name_0
    hd_arm2.data.name = hd_arm_data_name_0
    move_to_collection( hd_arm2, [ c.name for c in hd_arm.users_collection ] )

    obs_children = [ob for ob in hd_arm.children]
    obs_all_children = [ob for ob in hd_arm.children_recursive]

    for ob in obs_children:
        ob.parent = hd_arm2
    del obs_children

    for ob in obs_all_children:
        if ob.type != 'MESH':
            continue

        for m in ob.modifiers:
            if m.type == 'ARMATURE' and m.object == hd_arm:
                m.object = hd_arm2

        sks = ob.data.shape_keys
        if sks is None or sks.animation_data is None:
            continue

        for fcu in sks.animation_data.drivers:
            for v in fcu.driver.variables:
                for trg in v.targets:
                    if trg.id_type == 'OBJECT' and trg.id == hd_arm:
                        trg.id = hd_arm2
                    elif trg.id_type == 'ARMATURE' and trg.id == hd_arm.data:
                        trg.id = hd_arm2.data
    del obs_all_children

    delete_armature(hd_arm)
    return True


def api_get_morphs_directories(ob):
    try:
        from import_daz import api
        morphs_dirs = api.get_morph_directories(ob)
        return morphs_dirs
    except ModuleNotFoundError as e:
        from bl_ext.user_default.import_daz import api
        morphs_dirs = api.get_morph_directories(ob)
        return morphs_dirs
    except Exception as e:
        # ~ print("Required addon \"import_daz\" not found, or api function threw an exception.")
        return None


class ModifiersStatus:
    def __init__(self, ob, mode, m_types):
        assert( mode in {'ENABLE', 'DISABLE', 'ENABLE_ONLY', 'DISABLE_ONLY'} )

        new_status = mode.startswith("ENABLE")
        change_rest_opposite = mode.endswith("_ONLY")

        change_all = False
        types_to_change = set()
        for t in m_types:
            if t ==  'ALL':
                if len(m_types) > 1:
                    raise ValueError("Invalid \"m_types\" parameter: \"{0}\".".format(m_types))
                change_all = True
                break
            elif t == 'ARMATURE':
                types_to_change.add('ARMATURE')
            elif t == 'SUBDIV':
                types_to_change.update(('SUBSURF', 'MULTIRES'))
            else:
                raise ValueError("Invalid \"m_types\" parameter: \"{0}\".".format(m_types))

        self.ob = ob
        self.m_n = len(self.ob.modifiers)
        self.m_status = {}
        for i, m in enumerate(self.ob.modifiers):
            if change_all:
                self.m_status[i] = m.show_viewport
                m.show_viewport = new_status
            else:
                if m.type in types_to_change:
                    self.m_status[i] = m.show_viewport
                    m.show_viewport = new_status
                elif change_rest_opposite:
                    self.m_status[i] = m.show_viewport
                    m.show_viewport = not new_status

    def restore(self):
        assert( len(self.ob.modifiers) == self.m_n )
        for i, m in enumerate(self.ob.modifiers):
            if i in self.m_status:
                m.show_viewport = self.m_status[i]
        self.ob = None
        self.m_n = None
        self.m_status = None

def apply_shape_keys(ob):
    if ob.data.shape_keys is not None:
        make_single_active(ob)
        bpy.ops.object.shape_key_remove(all=True, apply_mix=True)

def apply_modifiers_stack(ob):
    make_single_active(ob)
    for m in ob.modifiers:
        bpy.ops.object.modifier_apply(modifier=m.name)

def ob_apply_modifiers(ob, modifiers='ALL'):
    assert( ob.type == 'MESH' )
    assert( modifiers in {'SK', 'NO_SK', 'NO_ARMATURE', 'ALL'} )
    if modifiers == 'SK':
        ob.modifiers.clear()
    elif modifiers == 'NO_ARMATURE':
        remove_armature_modifiers(ob)
    elif modifiers == 'NO_SK':
        remove_shape_keys(ob)
    apply_shape_keys(ob)
    apply_modifiers_stack(ob)


class ShapeKeysPin:
    def __init__(self, ob):
        assert (ob.data.shape_keys is not None) and (len(ob.data.shape_keys.key_blocks) > 0)
        self.ob = ob
        self.init_active_sk = self.ob.active_shape_key_index
        self.init_show_only_sk = bpy.context.object.show_only_shape_key

    def pin_sk(self, sk_name):
        make_single_active(self.ob)
        for i, sk in enumerate(self.ob.data.shape_keys.key_blocks):
            if sk.name == sk_name:
                self.ob.active_shape_key_index = i
                bpy.context.object.show_only_shape_key = True

    def restore(self):
        make_single_active(self.ob)
        self.ob.active_shape_key_index = self.init_active_sk
        bpy.context.object.show_only_shape_key = self.init_show_only_sk
