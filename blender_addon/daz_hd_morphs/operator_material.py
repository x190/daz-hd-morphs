import bpy, os, json
from . import utils
from . import utils_mat


class MaterialCommon(bpy.types.Operator):
    op_type = None # {'NORMAL', 'VDISP'}

    ob = None
    material = None
    st = None
    unit_scale = None

    files_data = None
    files_dir = None

    add_drivers = None

    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def check_input(self, context):
        ob = context.active_object
        if ob is None:
            self.report({'ERROR'}, "No active object selected.")
            return False
        if ob.type != 'MESH':
            self.report({'ERROR'}, "Invalid active object.")
            return False
        self.ob = ob

        material = ob.active_material
        if material is None:
            self.report({'ERROR'}, "Active object has no active material.")
            return False
        self.material = material
        self.st = utils_mat.ShaderTree(self.ob, self.material)

        self.unit_scale = context.scene.daz_hd_morph.unit_scale
        return True

    def check_files(self, files, directory):
        assert( self.op_type in ('VDISP', 'NORMAL') )
        if len(files) == 0:
            return False
        self.files_dir = bpy.path.abspath(directory)
        filepaths = [ os.path.join(self.files_dir, f.name) for f in files ]

        self.files_data = {}
        for fp in filepaths:
            fdata = {}
            self.files_data[fp] = fdata

            filename = os.path.basename(fp)
            ext = utils.get_extension(filename)
            if ext is None or ext not in ("png", "exr"):
                self.report({'ERROR'}, "Invalid file: {0}.".format(fp))
                return False
            fdata["ext"] = ext
            is_png = (ext == 'png')

            info_json = utils_mat.get_texture_json_info(fp)
            if self.op_type == 'VDISP':
                if info_json is not None:
                    fdata["id"] = info_json["id"]
                    fdata["tile"] = info_json["tile"]
                    fdata["space"] = info_json["space"]
                    fdata["midlevel"] = info_json["blender_settings"][0]
                    fdata["scale"] = info_json["blender_settings"][1]
                    fdata["uv_layer_name"] = info_json["uv_layer_name"]
                    if is_png and info_json["png_out_of_bounds"]:
                        print("WARNING: texture \"{0}\" wasn't generated optimally, try generating it "
                              "with \'Auto scale and midlevel\' enabled.".format(fp) )
                else:
                    info_name = utils_mat.get_vdisp_texture_name_info(filename)
                    if info_name is None:
                        fdata["id"] = utils.remove_extension(filename)
                    else:
                        fdata["id"] = info_name.id
                        fdata["tile"] = info_name.tile
                        fdata["space"] = info_name.space

                    if not is_png:
                        fdata["midlevel"] = 0
                        fdata["scale"] = self.unit_scale

            else: # self.op_type == 'NORMAL'
                if info_json is not None:
                    fdata["id"] = info_json["id"]
                    fdata["tile"] = info_json["tile"]
                    fdata["uv_layer_name"] = info_json["uv_layer_name"]
                else:
                    info_name = utils_mat.get_normal_texture_name_info(filename)
                    if info_name is None:
                        fdata["id"] = utils.remove_extension(filename)
                    else:
                        fdata["id"] = info_name.id
                        fdata["tile"] = info_name.tile

        return True

class MaterialAddVDTextures(MaterialCommon):
    """Add vector displacement textures to the active material of the active mesh"""
    bl_idname = "dazhdmorph.material_addvd"
    bl_label = "Add vector displacement textures"

    files : bpy.props.CollectionProperty( name="Filepaths",
                        type=bpy.types.OperatorFileListElement )

    directory : bpy.props.StringProperty( subtype='DIR_PATH' )

    filter_glob : bpy.props.StringProperty( default="*.png;*.exr",
                                            options={'HIDDEN'} )

    add_drivers:  bpy.props.BoolProperty( name="Create drivers", default=True,
                                          description="When a shape key match is found, drives the Value nodes accordingly" )

    def invoke(self, context, event):
        default_dir = ""
        if default_dir and os.path.isdir(default_dir):
            self.directory = default_dir
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

    def execute(self, context):
        self.op_type = 'VDISP'
        if not self.check_input(context):
            return {'CANCELLED'}
        if not self.check_files(self.files, self.directory):
            return {'CANCELLED'}

        addon_props = context.scene.daz_hd_morph
        r = self.add_nodes(context)
        if not r:
            self.report({'INFO'}, "No nodes added.")
            return {'CANCELLED'}
        self.report({'INFO'}, "Nodes added to material \"{0}\" of \"{1}\".".format(self.material.name, self.ob.name))
        return {'FINISHED'}

    def add_nodes(self, context):
        self.st.set_mat_displacement()
        self.st.build_vd_chain(self.files_data, add_drivers=self.add_drivers)
        return True


class MaterialAddNMTextures(MaterialCommon):
    """Add normal textures to the active material of the active mesh"""
    bl_idname = "dazhdmorph.material_addnm"
    bl_label = "Add normal textures"

    files : bpy.props.CollectionProperty( name="Filepaths",
                        type=bpy.types.OperatorFileListElement )

    directory : bpy.props.StringProperty( subtype='DIR_PATH' )

    filter_glob : bpy.props.StringProperty( default="*.png",
                                            options={'HIDDEN'} )

    add_drivers:  bpy.props.BoolProperty( name="Create drivers", default=True,
                                          description="When a shape key match is found, drives the Value nodes accordingly" )

    def invoke(self, context, event):
        default_dir = ""
        if default_dir and os.path.isdir(default_dir):
            self.directory = default_dir
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

    def execute(self, context):
        self.op_type = 'NORMAL'
        if not self.check_input(context):
            return {'CANCELLED'}
        if not self.check_files(self.files, self.directory):
            return {'CANCELLED'}

        addon_props = context.scene.daz_hd_morph
        r = self.add_nodes(context)
        if not r:
            self.report({'INFO'}, "No nodes added.")
            return {'CANCELLED'}
        self.report({'INFO'}, "Nodes added to material \"{0}\" of \"{1}\".".format(self.material.name, self.ob.name))
        return {'FINISHED'}

    def add_nodes(self, context):
        self.st.build_nm_chain(self.files_data, add_drivers=self.add_drivers)
        return True


class MaterialAddDrivers(MaterialCommon):
    """Create drivers for matching Value nodes of the active material of the active mesh"""
    bl_idname = "dazhdmorph.material_createdrivers"
    bl_label = "Create drivers"

    def execute(self, context):
        if not self.check_input(context):
            return {'CANCELLED'}

        r = self.add_drivers(context)
        if not r or r == 0:
            self.report({'INFO'}, "No drivers created.")
            return {'CANCELLED'}
        self.report({'INFO'}, "{0} drivers created for Value nodes of material \"{1}\" (see console).".format(r, self.material.name))
        return {'FINISHED'}

    def add_drivers(self, context):
        n = self.st.add_drivers(print_matches=True)
        return n
