import bpy, re, json, os
from mathutils import Vector
from . import utils


class TextureFilenameInfo:
    def __init__(self):
        self.id = None
        self.size = None
        self.tile = None
        self.ext = None
        self.space = None       # only vector displacements
        self.bake_type = None   # only normals

def get_vdisp_texture_name_info(name):
    r = re.search(r'(.+)-VDISP-([TO])-(\d+)\_(\d\d\d\d)\.(.+)', name)
    if r is None:
        return None
    info = TextureFilenameInfo()
    info.id = r.group(1)
    info.space = r.group(2)
    info.size = int(r.group(3))
    info.tile = int(r.group(4))
    info.ext = r.group(5)
    return info

def get_normal_texture_name_info(name):
    r = re.search(r'(.+)-([a-zA-Z]+)-(\d+)\_(\d\d\d\d)\.(.+)', name)
    if r is None:
        return None
    if r.group(2) not in ('NM', 'mrNM', 'mrDISP'):
        return None
    info = TextureFilenameInfo()
    info.id = r.group(1)
    info.bake_type = r.group(2)
    info.size = int(r.group(3))
    info.tile = int(r.group(4))
    info.ext = r.group(5)
    return info

def get_texture_json_info(fp_texture):
    fp_json = fp_texture + ".json"
    if not os.path.isfile(fp_json):
        return None
    with open(fp_json, "r", encoding="utf-8") as f:
        d = json.load(f)
        return d

class NodesBbox:
    def __init__(self, nodes):
        assert(len(nodes)>0)

        n0_loc = nodes[0].location
        r_x = Vector(( n0_loc[0], n0_loc[0] ))
        r_y = Vector(( n0_loc[1], n0_loc[1] ))
        for n in nodes[1:]:
            n_loc = n.location
            r_x[0] = min(r_x[0], n_loc[0])
            r_x[1] = max(r_x[1], n_loc[0])
            r_y[0] = min(r_y[0], n_loc[1])
            r_y[1] = max(r_y[1], n_loc[1])

        self.pos = Vector((r_x[0], r_y[0]))
        self.edge_x = Vector((r_x[1]-r_x[0], 0))
        self.edge_y = Vector((0, r_y[1]-r_y[0]))

    def get_center(self):
        return self.pos + 0.5 * (self.edge_x + self.edge_y)

    def get_left_side_center(self):
        return self.pos + 0.5 * self.edge_y

    def get_corner(self, i, j):
        return self.pos + i * self.edge_x + j * self.edge_y

class ShaderTree:
    normal_color = (0.5, 0.5, 1.0, 1)

    def __init__(self, ob, material):
        self.ob = ob
        material.use_nodes = True
        self.material = material
        self.tree = self.material.node_tree
        self.sk_names = None

    @staticmethod
    def new_loc(loc, dx, dy):
        return Vector(( loc[0]+dx, loc[1]+dy ))

    @classmethod
    def move_nodes(cls, nodes, dx, dy, loc0=None):
        if loc0 is None:
            for n in nodes:
                n.location = cls.new_loc(n.location, dx, dy)
        else:
            b = NodesBbox(nodes)
            diff = loc0 - b.get_corner(1, 1)
            diff = diff + Vector((dx, dy))
            for n in nodes:
                n.location = cls.new_loc(n.location, diff[0], diff[1])

    def set_mat_displacement(self):
        if self.material.displacement_method == 'BUMP':
            self.material.displacement_method = 'DISPLACEMENT'
            # ~ self.material.displacement_method = 'BOTH'

    def get_node_active_output(self):
        for n in self.tree.nodes:
            if n.type == 'OUTPUT_MATERIAL' and n.is_active_output:
                return n

    def get_node_with_normal_input(self):
        principled_nodes = []
        for n in self.tree.nodes:
            if n.type == 'BSDF_PRINCIPLED':
                principled_nodes.append(n)
        if len(principled_nodes) > 0:
            return principled_nodes[0]
        return None

    @staticmethod
    def load_image(fp):
        img = bpy.data.images.load(fp)
        # ~ img.source = 'FILE'
        img.colorspace_settings.name = 'Non-Color'
        return img

    @classmethod
    def get_all_connected_nodes_rec(cls, n, nodes_seen, nodes):
        nodes_seen.add(n.name)
        nodes.append(n)
        for inp in n.inputs:
            if not inp.is_linked:
                continue
            for link in inp.links:
                n_inp = link.from_node
                # ~ print(l.from_socket)
                if n_inp.name not in nodes_seen:
                    cls.get_all_connected_nodes_rec(n_inp, nodes_seen, nodes)

    def get_all_connected_nodes(self, n):
        nodes_seen = set()
        nodes = []
        self.get_all_connected_nodes_rec(n, nodes_seen, nodes)
        return nodes

    def replace_output_socket_links(self, socket_old, socket_new):
        for link in socket_old.links:
            if link.from_socket == socket_old:
                target_node = link.to_node
                target_socket_name = link.to_socket.name
                self.tree.links.remove(link)
                self.tree.links.new(socket_new, target_node.inputs[target_socket_name] )

    def get_output_socket_info(self, socket, remove_links):
        output_info = []
        for link in socket.links:
            if link.from_socket == socket:
                output_info.append( (link.to_node, link.to_socket.name) )
                if remove_links:
                    self.tree.links.remove(link)
        return output_info

    def link_socket( self, socket, output_info ):
        for n, socket_name in output_info:
            self.tree.links.new( socket, n.inputs[socket_name] )

    def is_value_driven(self, v):
        if self.tree.animation_data is None:
            return False
        val_dp = "nodes[\"{0}\"].outputs[0].default_value".format(v.name)
        for fcu in self.tree.animation_data.drivers:
            if fcu.data_path == val_dp:
                return True
        return False

    def drive_value(self, v, f_id, sk_directly=True):
        if self.ob.data.shape_keys is None:
            return False
        val_dp = "nodes[\"{0}\"].outputs[0].default_value".format(v.name)

        if not sk_directly:
            if self.ob.data.shape_keys.animation_data is None:
                return False
            sk_dp = "key_blocks[\"{0}\"].value".format(f_id)
            for fcu in self.ob.data.shape_keys.animation_data.drivers:
                if fcu.data_path == sk_dp:
                    if self.tree.animation_data is None:
                        self.tree.animation_data_create()
                    dst_fcu = self.tree.animation_data.drivers.from_existing(src_driver=fcu)
                    dst_fcu.data_path = val_dp
                    return True
        else:
            if self.sk_names is None:
                self.sk_names = utils.get_sk_names(self.ob)
            if f_id in self.sk_names:
                if self.tree.animation_data is None:
                    self.tree.animation_data_create()
                fcu = self.tree.animation_data.drivers.new(val_dp)
                fcu.driver.type = 'SCRIPTED'
                var = fcu.driver.variables.new()
                var.type = 'SINGLE_PROP'
                var.name = "x"
                trg = var.targets[0]
                trg.id_type = 'MESH'
                trg.id = self.ob.data
                trg.data_path = "shape_keys.key_blocks[\"{0}\"].value".format(f_id)
                fcu.driver.expression = "x"
                return True
        return False

    def add_drivers(self, print_matches):
        tot = 0
        value_nodes = {}
        for n in self.tree.nodes:
            if n.type != 'VALUE' or not n.label or self.is_value_driven(n):
                continue
            if n.label not in value_nodes:
                value_nodes[n.label] = []
            value_nodes[n.label].append(n)

        for label, n_list in value_nodes.items():
            for n in n_list:
                if self.drive_value(n, label, sk_directly=True):
                    if print_matches:
                        print("Driver created for node with label \"{0}\".".format(label))
                    tot += 1
        return tot

    @staticmethod
    def get_tex_coord_node(nodes):
        for n in nodes:
            if n.type == 'TEX_COORD' and n.outputs["UV"].is_linked:
                return n

    @staticmethod
    def hide_unused_output_sockets(node):
        for socket in node.outputs:
            if not socket.is_linked:
                socket.hide = True

    def link_tex_coord(self, nodes, n_tex_coord):
        is_new = False
        if n_tex_coord is None:
            n_tex_coord = self.tree.nodes.new(type='ShaderNodeTexCoord')
            b = NodesBbox(nodes)
            blc = b.get_left_side_center()
            n_tex_coord.location = self.new_loc( blc, -200, 0 )
            is_new = True

        n_tex_coord.outputs["UV"].hide = False
        for n in nodes:
            if n.type == 'TEX_IMAGE' and not n.inputs["Vector"].is_linked:
                self.tree.links.new( n_tex_coord.outputs["UV"], n.inputs["Vector"] )

        if is_new:
            self.hide_unused_output_sockets(n_tex_coord)

    def get_vd_chain_append(self):
        n_active = self.tree.nodes.active
        if n_active is None:
            return None
        if n_active.type != 'VECT_MATH' or n_active.operation != 'ADD':
            return None
        n_add = n_active
        return self.get_all_connected_nodes(n_add)

    def build_vd_member(self, fp, fdata, i, add_driver):
        nodes = []

        vs = self.tree.nodes.new(type='ShaderNodeVectorMath')
        vs.operation = 'SCALE'
        nodes.append(vs)

        v = self.tree.nodes.new(type='ShaderNodeValue')
        v.label = fdata["id"]
        # ~ v.name = "{0}_value".format(v.label)
        v.location = self.new_loc(vs.location, -200, -150)
        if add_driver:
            self.drive_value(v, fdata["id"], sk_directly=True)
        nodes.append(v)

        self.tree.links.new(v.outputs[0], vs.inputs["Scale"])

        vd = self.tree.nodes.new(type='ShaderNodeVectorDisplacement')
        vd.location = self.new_loc(vs.location, -200, 50)
        if "scale" in fdata:
            vd.inputs["Midlevel"].default_value = fdata["midlevel"]
            vd.inputs["Scale"].default_value = fdata["scale"]
        nodes.append(vd)

        self.tree.links.new(vd.outputs[0], vs.inputs[0])

        tx = self.tree.nodes.new(type="ShaderNodeTexImage")
        tx.label = fdata["id"]
        tx.location = self.new_loc(vd.location, -300, 100)
        tx.image = self.load_image(fp)
        nodes.append(tx)

        self.tree.links.new(tx.outputs["Color"], vd.inputs["Vector"])

        return nodes

    def build_vd_chain(self, files_data, add_drivers):
        all_nodes = []
        prev_last_n = None

        prev_chain = self.get_vd_chain_append()
        initial_last_add = None
        initial_last_add_output_info = None
        initial_loc = None
        i_offset = 0
        n_tex_coord = None
        if prev_chain is not None:
            prev_last_n = prev_chain[0]
            initial_last_add = prev_chain[0]
            initial_last_add_output_info = self.get_output_socket_info( initial_last_add.outputs["Vector"],
                                                                        remove_links=True )
            initial_loc = self.new_loc( initial_last_add.location, -200, 150 )
            all_nodes.extend(prev_chain)
            i_offset = 1
            n_tex_coord = self.get_tex_coord_node(prev_chain)
        del prev_chain

        for i, t in enumerate(files_data.items(), start=1):
            fp, fdata = t
            nodes = self.build_vd_member(fp, fdata, i, add_drivers)
            all_nodes.extend(nodes)
            if prev_last_n is None:
                prev_last_n = nodes[0]
                continue

            self.move_nodes( nodes, 0, 300 * (i+i_offset-1), loc0=initial_loc )
            last_n = nodes[0]

            va = self.tree.nodes.new(type='ShaderNodeVectorMath')
            va.operation = 'ADD'
            if prev_last_n.operation == 'ADD':
                va.location = self.new_loc(prev_last_n.location, 0, 300)
            else:
                va.location = self.new_loc(prev_last_n.location, 200, 300)
            all_nodes.append(va)

            self.tree.links.new(prev_last_n.outputs["Vector"], va.inputs[1])
            self.tree.links.new(last_n.outputs["Vector"], va.inputs[0])

            prev_last_n = va

        if prev_last_n is not None:
            if initial_last_add is None:
                n_output = self.get_node_active_output()
                if n_output is not None:
                    self.move_nodes(all_nodes, -300, -200, loc0=n_output.location)
                    self.tree.links.new(prev_last_n.outputs["Vector"], n_output.inputs["Displacement"])
            else:
                self.move_nodes( all_nodes, 0, 150, loc0=initial_last_add.location )
                self.link_socket( prev_last_n.outputs["Vector"], initial_last_add_output_info )

        self.link_tex_coord(all_nodes, n_tex_coord)


    def get_nm_chain_append(self):
        n_active = self.tree.nodes.active
        if n_active is None:
            return None, None
        if n_active.type != 'NORMAL_MAP':
            return None, None
        prev_chain = None
        n_normal = n_active
        if n_normal.inputs["Color"].is_linked:
            n_normal_input_link = n_normal.inputs["Color"].links[0]
            n_normal_input = n_normal_input_link.from_node
            self.tree.links.remove(n_normal_input_link)
            prev_chain = self.get_all_connected_nodes(n_normal_input)
        return n_normal, prev_chain

    def build_nm_member(self, fp, fdata, i, add_driver):
        nodes = []

        ov = self.tree.nodes.new(type='ShaderNodeMix')
        ov.data_type = 'RGBA'
        ov.blend_type = 'OVERLAY'
        ov.inputs[6].default_value = self.normal_color
        ov.inputs[7].default_value = self.normal_color
        nodes.append(ov)

        v = self.tree.nodes.new(type='ShaderNodeValue')
        v.label = fdata["id"]
        v.location = self.new_loc(ov.location, -178, -10)
        if add_driver:
            self.drive_value(v, fdata["id"], sk_directly=True)
        nodes.append(v)

        self.tree.links.new(v.outputs[0], ov.inputs[0])

        tx = self.tree.nodes.new(type="ShaderNodeTexImage")
        tx.label = fdata["id"]
        tx.location = self.new_loc(ov.location, -280, -100)
        tx.image = self.load_image(fp)
        nodes.append(tx)

        self.tree.links.new(tx.outputs["Color"], ov.inputs[7])

        return nodes

    def build_nm_chain(self, files_data, add_drivers):
        prev_last_n = None
        all_nodes = []

        n_normal, prev_chain = self.get_nm_chain_append()
        # ~ n_normal_output_info = None
        initial_loc = None
        i_offset = 0
        n_tex_coord = None
        if prev_chain is not None:
            prev_last_n = prev_chain[0]
            # ~ n_normal_output_info = self.get_output_socket_info( n_normal.outputs[0],
                                                                # ~ remove_links=True )
            initial_loc = self.new_loc( n_normal.location, -200, 0 )
            all_nodes.extend(prev_chain)
            i_offset = 1
            n_tex_coord = self.get_tex_coord_node(prev_chain)
        del prev_chain

        for i, t in enumerate(files_data.items(), start=1):
            fp, fdata = t
            nodes = self.build_nm_member(fp, fdata, i, add_drivers)
            all_nodes.extend(nodes)
            if prev_last_n is None:
                prev_last_n = nodes[0]
                continue

            self.move_nodes(nodes, 0, 380 * (i+i_offset-1), loc0=initial_loc)
            last_n = nodes[0]

            self.tree.links.new(prev_last_n.outputs[2], last_n.inputs[6])

            prev_last_n = last_n

        if prev_last_n is not None:
            if n_normal is None:
                nm = self.tree.nodes.new(type='ShaderNodeNormalMap')
                nm.space = 'TANGENT'
                nm.inputs[0].default_value = 1.0
                nm.location = self.new_loc(prev_last_n.location, 200, 0)
                all_nodes.append(nm)

                self.tree.links.new(prev_last_n.outputs[2], nm.inputs["Color"])

                n_with_normal_input = self.get_node_with_normal_input()
                if n_with_normal_input is not None:
                    self.move_nodes(all_nodes, -300, -550, loc0=n_with_normal_input.location)
                    # ~ self.tree.links.new(nm.outputs[0], n_with_normal_input.inputs["Normal"])
            else:
                self.tree.links.new(prev_last_n.outputs[2], n_normal.inputs["Color"])
                self.move_nodes( all_nodes, -200, 0, loc0=n_normal.location )

        self.link_tex_coord(all_nodes, n_tex_coord)
