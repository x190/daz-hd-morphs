import bpy, os, re
from mathutils import Vector
from . import utils

class VDispTextureSettings:
    def __init__(self, context, tiles):
        addon_props = context.scene.daz_hd_morph
        self.target_space = addon_props.vdisp_target_space
        self.texture_size = int(addon_props.texture_size)
        self.texture_margin = addon_props.texture_margin
        self.image_type = addon_props.vdisp_image_type
        self.uv_layer = addon_props.texture_uv_layer
        self.max_subd = addon_props.texture_max_subd

        self.scale = 1
        self.midlevel = 0
        if self.image_type == 'P':
            self.auto_settings = addon_props.vdisp_auto_settings
            if not self.auto_settings:
                self.scale = addon_props.vdisp_scale
                self.midlevel = addon_props.vdisp_midlevel
        else:
            self.auto_settings = False

        self.tiles = tiles

class HDMorphOperator(bpy.types.Operator):
    working_dirpath = None
    gScale = None
    geograft_file = None

    vdm_subdirname = "vector_disp_maps"
    fileslist_name = "files_list.txt"

    normal_subdirname = "normal_maps"
    temporary_subdirname = "_temporary"

    base_ob = None
    base_ob_copy = None

    cleanup_files = None

    morphs_groups = None
    hd_ob = None
    hd_collection_name = "hd_collection"

    vdisp_ts = None
    tiles = None
    uv_layer_name = None

    saved_settings = None


    @classmethod
    def poll(cls, context):
        return context.mode == 'OBJECT'

    def invoke(self, context, event):
        return self.execute(context)

    def check_input(self, context, check_hd=False):
        scn = context.scene
        addon_props = scn.daz_hd_morph

        if addon_props.base_ob not in scn.objects:
            self.report({'ERROR'}, "Base mesh not found in the scene.")
            return False
        base_ob = scn.objects[ addon_props.base_ob ]
        if not base_ob or base_ob.type != 'MESH':
            self.report({'ERROR'}, "Invalid base mesh.")
            return False
        self.base_ob = base_ob

        self.gScale = addon_props.unit_scale

        if check_hd:
            if addon_props.hd_ob not in scn.objects:
                self.report({'ERROR'}, "HD object not found in the scene.")
                return False
            hd_ob = scn.objects[ addon_props.hd_ob ]
            if not hd_ob or hd_ob.type != 'MESH':
                self.report({'ERROR'}, "Invalid HD object.")
                return False
            if len(hd_ob.data.vertices) < len(self.base_ob.data.vertices):
                self.report({'ERROR'}, "HD mesh has fewer vertices than base mesh.")
                return False
            self.hd_ob = hd_ob

        working_dirpath = addon_props.working_dirpath
        if not working_dirpath.strip():
            self.report({'ERROR'}, "Working directory not set.")
            return False
        working_dirpath = os.path.abspath( bpy.path.abspath(working_dirpath) )
        if not os.path.isdir(working_dirpath):
            self.report({'ERROR'}, "Working directory not found.")
            return False
        self.working_dirpath = os.path.join(working_dirpath, utils.makeValidFilename(self.base_ob.name))
        if not os.path.isdir(self.working_dirpath):
            os.mkdir(self.working_dirpath)
        self.cleanup_files = context.preferences.addons[__package__].preferences.delete_temporary_files

        geograft_file = addon_props.geograft_file
        if geograft_file.strip():
            geograft_file = os.path.abspath( bpy.path.abspath(geograft_file) )
            if not os.path.isfile(geograft_file) or not utils.has_extension(geograft_file, "json"):
                self.report({'ERROR'}, "Invalid geograft file.")
                return False
            self.geograft_file = geograft_file
        else:
            self.geograft_file = None

        self.save_settings(context)

        return True

    def save_settings(self, context):
        s = {}
        if context.scene.render.use_simplify:
            s["simplify"] = True
            context.scene.render.use_simplify = False
        self.saved_settings = s

    def restore_settings(self, context):
        if self.saved_settings is None:
            return
        if "simplify" in self.saved_settings:
            context.scene.render.use_simplify = self.saved_settings["simplify"]
        self.saved_settings = None

    def check_morphs_list(self, context, group_selector=False, can_be_empty=False):
        addon_props = context.scene.daz_hd_morph
        base_vcount = len(self.base_ob.data.vertices)
        geo_vcounts = None
        if self.geograft_file is not None:
            geo_vcounts = utils.geograft_file_vcounts(self.geograft_file)

        morphs_entries = addon_props.morph_files_list.entries
        morphs_groups = {}
        for e in morphs_entries:
            if group_selector and e.group != addon_props.selected_morphs_group:
                continue
            if e.weight < 1e-3:
                continue
            fp = utils.check_dsf_file(e.filepath, base_vcount, geo_vcounts)
            if fp is None:
                self.report({'ERROR'}, "File \"{0}\" is invalid. See console output.".format(e.filepath))
                return False

            if e.group not in morphs_groups:
                morphs_groups[e.group] = []
            for t in morphs_groups[e.group]:
                if t[0] == fp:
                    self.report({'ERROR'}, "File \"{0}\" listed more than once in the same group.".format(fp))
                    return False
            morphs_groups[e.group].append((fp, e.weight, e.morph_component))

        single_morphs = set()
        for g, morph_tups in morphs_groups.items():
            if len(morph_tups) == 1:
                mfn = os.path.basename(morph_tups[0][0])
                if mfn in single_morphs:
                    self.report({'ERROR'}, "File \"{0}\" listed more than once in different single-morph groups.".format(mfn))
                    return False
                else:
                    single_morphs.add(mfn)
        del single_morphs

        if len(morphs_groups) == 0:
            if not can_be_empty:
                self.report({'ERROR'}, "No valid morph files selected.")
                return False
            else:
                self.morphs_groups = None
                return True

        self.morphs_groups = morphs_groups
        return True

    @staticmethod
    def create_empty_morph_group(name):
        morphs_groups = {}
        morphs_groups[0] = []
        morphs_groups_names = {}
        morphs_groups_names[0] = name
        return morphs_groups, morphs_groups_names

    def check_input_vdisp(self, context):
        if not self.check_texture_tiles(context):
            return False
        self.vdisp_ts = VDispTextureSettings(context, self.tiles)
        if not self.check_uv_layer(self.vdisp_ts.uv_layer):
            return False
        return True

    def check_texture_tiles(self, context):
        addon_props = context.scene.daz_hd_morph
        if addon_props.use_texture_tiles:
            self.tiles = []
            for e in addon_props.texture_tiles.entries:
                if e.enabled:
                    self.tiles.append(int(e.name))
            if len(self.tiles) == 0:
                self.report({'ERROR'}, "No UV tiles selected.")
                return False
        else:
            self.tiles = None
        return True

    def check_uv_layer(self, n):
        if n < 0 or not self.base_ob or n >= len(self.base_ob.data.uv_layers):
            self.report({'ERROR'}, "Invalid uv layer.")
            return False
        self.uv_layer_name = utils.get_uv_layer_name(self.base_ob, n)
        return True

    def create_subdir(self, subdirpath):
        if not os.path.isdir(subdirpath):
            os.mkdir(subdirpath)
        return subdirpath

    def create_vdm_subdir(self):
        subdirpath = self.create_subdir( os.path.join(self.working_dirpath, self.vdm_subdirname) )
        filelist_fp = os.path.join( subdirpath, self.fileslist_name )
        if os.path.isfile(filelist_fp):
            os.remove(filelist_fp)
        return subdirpath

    def get_filelist_fps(self, outputDirpath):
        fps = []
        filelist_fp = os.path.join( outputDirpath, self.fileslist_name )
        if os.path.isfile(filelist_fp):
            with open(filelist_fp, "r", encoding='utf-8') as f:
                fps = f.read().split(";")
            os.remove(filelist_fp)
        return fps

    def create_normal_subdir(self):
        return self.create_subdir( os.path.join(self.working_dirpath, self.normal_subdirname) )

    def create_temporary_subdir(self):
        return self.create_subdir(self.get_temporary_subdir())

    def get_temporary_subdir(self):
        return os.path.join(self.working_dirpath, self.temporary_subdirname)

    @staticmethod
    def export_set_uv( ob, uv_layer ):
        export_uvs = None
        active_uv_only = None
        prev_active_uv_layer = -1

        if uv_layer is None:
            export_uvs = False
            active_uv_only = True
        elif uv_layer < 0:
            export_uvs = True
            active_uv_only = False
        elif uv_layer >= 0:
            export_uvs = True
            active_uv_only = True
            prev_active_uv_layer = utils.get_active_uv_layer(ob)
            utils.set_active_uv_layer(ob, uv_layer)

        return export_uvs, active_uv_only, prev_active_uv_layer

    def export_ob_obj( self, ob, name, apply_modifiers, uv_layer ):
        export_uvs, _, prev_active_uv_layer = self.export_set_uv( ob, uv_layer )

        tmp_dir = self.create_temporary_subdir()
        fp_base = os.path.join(tmp_dir, name)
        fp = fp_base + ".obj"

        ob_mw_trans_prev = Vector(ob.matrix_world.translation)
        ob.matrix_world.translation = (0, 0, 0)
        utils.make_single_active(ob)
        bpy.ops.wm.obj_export( filepath=fp, check_existing=False,
                               export_animation=False, apply_modifiers=apply_modifiers,
                               export_eval_mode='DAG_EVAL_VIEWPORT',
                               export_selected_objects=True, export_uv=export_uvs,
                               export_normals=False, export_colors=False,
                               export_materials=False, export_pbr_extensions=False,
                               export_triangulated_mesh=False, export_material_groups=False,
                               export_vertex_groups=False, export_smooth_groups=False,
                               forward_axis='Y', up_axis='Z')

        ob.matrix_world.translation = ob_mw_trans_prev
        if prev_active_uv_layer >= 0:
            utils.set_active_uv_layer(ob, prev_active_uv_layer)

        return fp_base

    def export_ob_dae( self, ob, name, apply_modifiers,
                       include_weights, with_obj, uv_layer ):
        export_uvs, active_uv_only, prev_active_uv_layer = self.export_set_uv( ob, uv_layer )

        tmp_dir = self.create_temporary_subdir()
        fp_base = os.path.join(tmp_dir, name)
        fp = fp_base + ".dae"

        utils.make_single_active(ob)
        bpy.ops.wm.collada_export( filepath=fp, check_existing=False,
                                   apply_modifiers = apply_modifiers,
                                   export_mesh_type_selection = 'view',
                                   selected = True,
                                   include_children = False,
                                   include_armatures = include_weights,
                                   include_shapekeys = False,
                                   deform_bones_only = False,
                                   include_animations = False,
                                   include_all_actions = False,
                                   active_uv_only = active_uv_only,
                                   use_texture_copies = False,
                                   triangulate = False,
                                   use_blender_profile = False,
                                   limit_precision = False,
                                   keep_bind_info = False )

        if with_obj:
            fp = fp_base + ".obj"
            bpy.ops.wm.obj_export( filepath=fp, check_existing=False,
                                   export_animation=False, apply_modifiers=apply_modifiers,
                                   export_eval_mode='DAG_EVAL_VIEWPORT',
                                   export_selected_objects=True, export_uv=False,
                                   export_normals=False, export_colors=False,
                                   export_materials=False, export_pbr_extensions=False,
                                   export_triangulated_mesh=False, export_material_groups=False,
                                   export_vertex_groups=False, export_smooth_groups=False )

        if prev_active_uv_layer >= 0:
            utils.set_active_uv_layer(ob, prev_active_uv_layer)

        return fp_base

    def get_ob_to_export(self, base_modifiers):
        assert( base_modifiers in {'NONE', 'SK', 'NO_SK', 'NO_ARMATURE', 'ALL'} )
        ob_to_export = None
        apply_modifiers = None

        if base_modifiers == 'NONE':
            ob_to_export = self.base_ob
            apply_modifiers = False
        else:
            self.base_ob_copy = utils.copy_object(self.base_ob)
            ob_to_export = self.base_ob_copy
            apply_modifiers = True
            if base_modifiers == 'SK':
                self.base_ob_copy.modifiers.clear()
            else:
                utils.remove_division_modifiers(self.base_ob_copy)
                if base_modifiers == 'NO_ARMATURE':
                    utils.remove_armature_modifiers(self.base_ob_copy)
                elif base_modifiers == 'NO_SK':
                    utils.remove_shape_keys(self.base_ob_copy)

        return ob_to_export, apply_modifiers

    def export_base_obj( self, context, base_modifiers, uv_layer, obj_name="base" ):
        ob_to_export, apply_modifiers = self.get_ob_to_export(base_modifiers)
        fp_base = self.export_ob_obj( ob_to_export, obj_name, apply_modifiers,
                                      uv_layer )
        self.cleanup_base_copy()
        return fp_base

    def export_base_dae( self, context, base_modifiers,
                         include_weights, with_obj, uv_layer ):
        ob_to_export, apply_modifiers = self.get_ob_to_export(base_modifiers)
        fp_base = self.export_ob_dae( ob_to_export, "base", apply_modifiers,
                                      include_weights, with_obj, uv_layer )
        self.cleanup_base_copy()
        return fp_base

    def cleanup_base_copy(self):
        if self.base_ob_copy is not None:
            utils.delete_object(self.base_ob_copy)
            self.base_ob_copy = None

    def cleanup(self, context):
        self.cleanup_base_copy()
        self.restore_settings(context)
        if self.cleanup_files:
            tmp_dir = self.get_temporary_subdir()
            if not os.path.isdir(tmp_dir):
                return
            fp_to_delete = []
            for fn in os.listdir(tmp_dir):
                fp = os.path.join(tmp_dir, fn)
                if os.path.isfile(fp) and utils.has_extension(fn, "obj", "mtl", "dae"):
                    fp_to_delete.append(fp)
            for fp in fp_to_delete:
                os.remove(fp)

    def cleanup_shape_files(self):
        tmp_dir = self.get_temporary_subdir()
        if not os.path.isdir(tmp_dir):
            return
        exp = re.compile(r"shape-\d+\.dae")
        fp_to_delete = []
        for fn in os.listdir(tmp_dir):
            fp = os.path.join(tmp_dir, fn)
            if os.path.isfile(fp) and exp.match(fn):
                fp_to_delete.append(fp)
        for fp in fp_to_delete:
            os.remove(fp)
