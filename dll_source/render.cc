#include <cassert>
#include <fmt/format.h>
#include <png.h>

#define TINYEXR_IMPLEMENTATION
#include "tinyexr.h"
#include "diff.hh"

#define RENDER_TO_WINDOW    0


const char * vertexSource = R"glsl(
#version 150 core

uniform float tile;

in vec2 position;
in vec3 color;

out vec3 Color;

void main()
{
    Color = color;
    gl_Position = vec4(2.0 * (position.x - 0.5 - tile), 2.0 * (position.y - 0.5), 0.0, 1.0);
}
)glsl";


const char* fragmentSource = R"glsl(
#version 150 core

in  vec3 Color;
out vec4 outColor;

void main()
{
    outColor = vec4(Color, 1.0);
}
)glsl";


void MeshDiff::renderContextInit( const char image_type,
                                  const unsigned short width,
                                  const unsigned short margin )
{
    render_context.width = width;
    render_context.texture_margin = margin;
    switch(image_type) {
        case 'P': { render_context.image_type = RenderContext::ImageType::PNG;
                    break;
                  }
        case 'X': { render_context.image_type = RenderContext::ImageType::EXR;
                    break;
                  }
        default:  { throw std::runtime_error(fmt::format("Invalid image_type: '{}'", image_type));
                    break;
                  }
    }
    render_context.initializeGL();
    render_context.is_initialized = true;
}

void MeshDiff::renderContextFinish()
{
    render_context.terminateGL();
}


MeshDiff::RenderContext::RenderContext() :
    is_initialized(false), is_GLinitialized(false)
{
}

MeshDiff::RenderContext::~RenderContext()
{
    is_initialized = false;
    terminateGL();
}


void MeshDiff::RenderContext::initializeGL()
{
    if (is_GLinitialized)
        throw std::runtime_error("RenderContextGL already initialized");

    if (!glfwInit())
        throw std::runtime_error("failed to initialize GLFW");

    glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // We don't want the old OpenGL
    #if !RENDER_TO_WINDOW
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
    #endif

    // Open a window and create its OpenGL context
    GLFWwindow * window = glfwCreateWindow(width, width, "MeshDiff_RenderContext", nullptr, nullptr);
    if (!window)
        throw std::runtime_error("failed to open GLFW window");
    glfwMakeContextCurrent(window); // Initialize GLEW

    glewExperimental = true; // Needed in core profile
    if (glewInit() != GLEW_OK)
        throw std::runtime_error("failed to initialize GLEW");

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);

    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexSource, nullptr);
    glCompileShader(vertexShader);
    GLint status;
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);
    assert(status == GL_TRUE);

    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentSource, nullptr);
    glCompileShader(fragmentShader);
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);
    assert(status == GL_TRUE);

    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glBindFragDataLocation(shaderProgram, 0, "outColor");
    glLinkProgram(shaderProgram);
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    glUseProgram(shaderProgram);

    posAttrib = glGetAttribLocation(shaderProgram, "position");
    colAttrib = glGetAttribLocation(shaderProgram, "color");
    tileAttrib = glGetUniformLocation(shaderProgram, "tile");

    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(MeshDiff::GLVertex), (void*) 0);
    glEnableVertexAttribArray(posAttrib);

    glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(MeshDiff::GLVertex), (void*) (2 * sizeof(float)));
    glEnableVertexAttribArray(colAttrib);

    /* framebuffer */
    glGenFramebuffers(1, &frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

    /* color attachment texture */
    glGenTextures(1, &texColorBuffer);
    glBindTexture(GL_TEXTURE_2D, texColorBuffer);
    if (image_type == ImageType::PNG)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, width, 0, GL_RGBA, GL_UNSIGNED_SHORT, nullptr);
    else
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width, width, 0, GL_RGBA, GL_FLOAT, nullptr);
    if (auto err = glGetError())
        throw std::runtime_error("GL error: " + std::to_string(err));
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texColorBuffer, 0);

    /* stencil-depth */
    glGenRenderbuffers(1, &renderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, renderBuffer);
    // use a single renderbuffer object for both a depth AND stencil buffer:
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, width);
    // now attach it:
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, renderBuffer);
    // now that we created the framebuffer and added all attachments we want to check if it is actually complete:
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        throw std::runtime_error("frameBuffer is not complete.");
    glDisable(GL_DEPTH_TEST);
    #if !RENDER_TO_WINDOW
    glViewport(0, 0, width, width);
    #endif
    is_GLinitialized = true;
}

void MeshDiff::RenderContext::updateVertexBuffer(const std::vector<MeshDiff::GLTriangle>& triangles)
{
    if (!is_GLinitialized)
        throw std::runtime_error("Uninitialized RenderContextGL in call to updateVertexBuffer()");
    glBufferData( GL_ARRAY_BUFFER, triangles.size() * sizeof(MeshDiff::GLTriangle),
                  triangles.data(), GL_STATIC_DRAW );
    num_elements = triangles.size() * 3;
}

void MeshDiff::RenderContext::terminateGL()
{
    if (is_GLinitialized) {
        glDeleteRenderbuffers(1, &renderBuffer);
        glDeleteTextures(1, &texColorBuffer);
        glDeleteFramebuffers(1, &frameBuffer);
        glDeleteProgram(shaderProgram);
        glDeleteBuffers(1, &vertexBuffer);
        glDeleteVertexArrays(1, &vao);
        glfwTerminate();
        is_GLinitialized = false;
    }
}

void MeshDiff::RenderContext::renderTile(const unsigned short tile, GLvoid* data)
{
    if (!is_GLinitialized)
        throw std::runtime_error("Uninitialized RenderContextGL in call to renderTile()");
    #if RENDER_TO_WINDOW
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    while(!glfwWindowShouldClose(window)) {
    #endif // RENDER_TO_WINDOW

    glUniform1f(tileAttrib, tile);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    #if !RENDER_TO_WINDOW
    std::cout << fmt::format("Rendering tile {}...\n", tile + 1001);
    #endif

    glDrawArrays(GL_TRIANGLES, 0, num_elements);
    if (auto err = glGetError())
        throw std::runtime_error("GL error: " + std::to_string(err));

    #if RENDER_TO_WINDOW
    glfwSwapBuffers(window);
    glfwPollEvents();
    }
    throw std::runtime_error("Window closed.");
    #endif // RENDER_TO_WINDOW

    glReadBuffer(GL_COLOR_ATTACHMENT0);

    if (image_type == ImageType::PNG) {
        glReadPixels(0, 0, width, width, GL_RGB, GL_UNSIGNED_SHORT, data);
        /*---- Alternative available from OpenGL 4.5 onwards: */
        // glReadnPixels(0, 0, width, width, GL_RGB, GL_UNSIGNED_SHORT, data.size() * sizeof(Pixel), data.data());
    } else {
        glReadPixels(0, 0, width, width, GL_RGB, GL_FLOAT, data);
    }

    if (auto err = glGetError())
        throw std::runtime_error("GL error: " + std::to_string(err));
}


struct PngPixel
{
    uint16_t r = 0;
    uint16_t g = 0;
    uint16_t b = 0;
};
static_assert(sizeof(PngPixel) == 2*3);

void writePNG( const unsigned short width, const unsigned short margin,
               const std::string fp, std::vector<PngPixel> pixels,
               const uint16_t png_zero_level )
{
    std::vector<bool> zero_mask(width * width);
    for (unsigned int x = 0; x < width; x++)
    {
        for (unsigned int y = 0; y < width; y++)
        {
            const unsigned int offset = y * width + x;
            const auto & pixel = pixels[offset];
            zero_mask[offset] = (pixel.r == 0 && pixel.g == 0 && pixel.b == 0);
        }
    }

    if (margin > 0)
    {
        auto is_filled = [&](const unsigned int x, const unsigned int y)
        {
            if (x < 0 || x >= width || y < 0 || y >= width)
                return false;
            return !zero_mask[y * width + x];
        };

        unsigned short weight[9];
        weight[0] = 1;
        weight[1] = 2;
        weight[2] = 1;
        weight[3] = 2;
        weight[4] = 0;
        weight[5] = 2;
        weight[6] = 1;
        weight[7] = 2;
        weight[8] = 1;

        bool cannot_early_out = true;
        std::vector<bool> zero_mask_dst = zero_mask;
        std::vector<PngPixel> pixels_dst = pixels;

        for (unsigned short r = 0; cannot_early_out && r < margin; r++)
        {
            cannot_early_out = false;

            for (unsigned int x = 0; x < width; x++)
            {
                for (unsigned int y = 0; y < width; y++)
                {
                    if (zero_mask[y * width + x])
                    {
                        if ( is_filled(x-1, y) || is_filled(x+1, y) ||
                            is_filled(x, y-1) || is_filled(x, y+1) )
                        {
                            uint32_t r = 0, g = 0, b = 0;
                            uint32_t wsum = 0;
                            unsigned short k = 0;

                            for (short i = -1; i <= 1; i++)
                            {
                                for (short j = -1; j<= 1; j++)
                                {
                                    if (i != 0 || j != 0)
                                    {
                                        if ( is_filled(x + i, y + j) )
                                        {
                                            const auto & pixel = pixels[ (y+j) * width + x+i ];
                                            r += pixel.r * weight[k];
                                            g += pixel.g * weight[k];
                                            b += pixel.b * weight[k];
                                            wsum += weight[k];
                                        }
                                    }
                                    k++;
                                }
                            }

                            const unsigned int offset = y * width + x;
                            auto & pixel_dst = pixels_dst[offset];
                            pixel_dst.r = r / wsum;
                            pixel_dst.g = g / wsum;
                            pixel_dst.b = b / wsum;
                            zero_mask_dst[offset] = false;
                            cannot_early_out = true;
                        }
                    }
                }
            }
            zero_mask = zero_mask_dst;
            pixels = pixels_dst;
        }
    }


    for (unsigned int x = 0; x < width; x++)
    {
        for (unsigned int y = 0; y < width; y++)
        {
            if (zero_mask[y * width + x])
            {
                auto & pixel = pixels[y * width + x];
                pixel.r = png_zero_level;  // 32768; // == 0.5 * max(uint16_t) == 0.5 * 65535 (midgray)
                pixel.g = png_zero_level;  // 32768;
                pixel.b = png_zero_level;  // 32768;
            }
        }
    }

    /* Write the PNG. */
    std::cout << fmt::format("Writing '{}'...\n", fp);

    auto f = fopen(fp.c_str(), "wb");
    assert(f);

    auto png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
    assert(png_ptr);

    auto info_ptr = png_create_info_struct(png_ptr);
    assert(info_ptr);

    png_init_io(png_ptr, f);

    png_set_IHDR(
        png_ptr, info_ptr, width, width,
        16, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
        PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

    png_write_info(png_ptr, info_ptr);

    png_byte * row_pointers[width];
    for (unsigned int i = 0; i < width; i++)
        row_pointers[i] = ((png_byte *) pixels.data()) + (width - i - 1) * width * 6;

    png_set_swap(png_ptr);
    png_write_image(png_ptr, row_pointers);
    png_write_end(png_ptr, nullptr);
    png_destroy_read_struct(&png_ptr, &info_ptr, nullptr);

    fclose(f);
}


struct ExrPixel
{
    float r = 0;
    float g = 0;
    float b = 0;
};
static_assert(sizeof(ExrPixel) == 4*3);

void writeEXR( const unsigned short width, const unsigned short margin,
               const std::string fp, std::vector<ExrPixel> pixels )
{
    std::vector<bool> zero_mask(width * width);
    for (unsigned int x = 0; x < width; x++)
    {
        for (unsigned int y = 0; y < width; y++)
        {
            const unsigned int offset = y * width + x;
            const auto & pixel = pixels[offset];
            zero_mask[offset] = (pixel.r < 1e-6 && pixel.g < 1e-6 && pixel.b < 1e-6);
        }
    }

    if (margin > 0)
    {
        auto is_filled = [&](const unsigned int x, const unsigned int y)
        {
            if (x < 0 || x >= width || y < 0 || y >= width)
                return false;
            return !zero_mask[y * width + x];
        };

        unsigned short weight[9];
        weight[0] = 1;
        weight[1] = 2;
        weight[2] = 1;
        weight[3] = 2;
        weight[4] = 0;
        weight[5] = 2;
        weight[6] = 1;
        weight[7] = 2;
        weight[8] = 1;

        bool cannot_early_out = true;
        std::vector<bool> zero_mask_dst = zero_mask;
        std::vector<ExrPixel> pixels_dst = pixels;

        for (unsigned short r = 0; cannot_early_out && r < margin; r++)
        {
            cannot_early_out = false;

            for (unsigned int x = 0; x < width; x++)
            {
                for (unsigned int y = 0; y < width; y++)
                {
                    if (zero_mask[y * width + x])
                    {
                        if ( is_filled(x-1, y) || is_filled(x+1, y) ||
                            is_filled(x, y-1) || is_filled(x, y+1) )
                        {
                            float r = 0, g = 0, b = 0;
                            uint32_t wsum = 0;
                            unsigned short k = 0;

                            for (short i = -1; i <= 1; i++)
                            {
                                for (short j = -1; j<= 1; j++)
                                {
                                    if (i != 0 || j != 0)
                                    {
                                        if ( is_filled(x + i, y + j) )
                                        {
                                            const auto & pixel = pixels[ (y+j) * width + x+i ];
                                            r += pixel.r * weight[k];
                                            g += pixel.g * weight[k];
                                            b += pixel.b * weight[k];
                                            wsum += weight[k];
                                        }
                                    }
                                    k++;
                                }
                            }

                            const unsigned int offset = y * width + x;
                            auto & pixel_dst = pixels_dst[offset];
                            pixel_dst.r = r / wsum;
                            pixel_dst.g = g / wsum;
                            pixel_dst.b = b / wsum;
                            zero_mask_dst[offset] = false;
                            cannot_early_out = true;
                        }
                    }
                }
            }

            zero_mask = zero_mask_dst;
            pixels = pixels_dst;
        }
    }

    for (unsigned int x = 0; x < width; x++)
    {
        for (unsigned int y = 0; y < width; y++)
        {
            if (zero_mask[y * width + x])
            {
                auto & pixel = pixels[y * width + x];
                pixel.r = 0;
                pixel.g = 0;
                pixel.b = 0;
            }
        }
    }

    /* Write the EXR. */
    std::cout << fmt::format("Writing '{}'...\n", fp);

    EXRHeader header;
    InitEXRHeader(&header);

    EXRImage image;
    InitEXRImage(&image);
    image.num_channels = 3;

    std::vector<float> images[3];
    images[0].resize(pixels.size());
    images[1].resize(pixels.size());
    images[2].resize(pixels.size());

    // Split RGBRGBRGB... into R, G and B layer
    for (unsigned int i = 0; i < width; i++) {
        const unsigned int row_start_p = (unsigned int) (width-1-i) * width;
        const unsigned int row_start_i = i * width;
        for (unsigned int j = 0; j < width; j++) {
            const unsigned int element_i = row_start_i + j;
            const auto & pixel = pixels[row_start_p + j];
            images[0][element_i] = pixel.r;
            images[1][element_i] = pixel.g;
            images[2][element_i] = pixel.b;
        }
    }

    float* image_ptr[3];
    image_ptr[0] = &(images[2].at(0)); // B
    image_ptr[1] = &(images[1].at(0)); // G
    image_ptr[2] = &(images[0].at(0)); // R

    image.images = (unsigned char**) image_ptr;
    image.width = width;
    image.height = width;

    header.num_channels = 3;
    header.channels = (EXRChannelInfo *)malloc(sizeof(EXRChannelInfo) * header.num_channels);
    // Must be (A)BGR order, since most of EXR viewers expect this channel order.
    strncpy(header.channels[0].name, "B", 255); header.channels[0].name[strlen("B")] = '\0';
    strncpy(header.channels[1].name, "G", 255); header.channels[1].name[strlen("G")] = '\0';
    strncpy(header.channels[2].name, "R", 255); header.channels[2].name[strlen("R")] = '\0';

    header.pixel_types = (int *)malloc(sizeof(int) * header.num_channels);
    header.requested_pixel_types = (int *)malloc(sizeof(int) * header.num_channels);
    for (int i = 0; i < header.num_channels; i++) {
        header.pixel_types[i] = TINYEXR_PIXELTYPE_FLOAT; // pixel type of input image
        header.requested_pixel_types[i] = TINYEXR_PIXELTYPE_FLOAT; // pixel type of output image to be stored in .EXR
    }
    header.compression_type = TINYEXR_COMPRESSIONTYPE_ZIP;

    const char* err = nullptr; // or nullptr in C++11 or later.
    int ret = SaveEXRImageToFile(&image, &header, fp.c_str(), &err);
    if (ret != TINYEXR_SUCCESS) {
        std::cout << fmt::format("Save EXR err: {}\n", err);
        FreeEXRErrorMessage(err); // free's buffer for an error message
    }

    free(header.channels);
    free(header.pixel_types);
    free(header.requested_pixel_types);
}


void MeshDiff::writeToImage( const std::string & outputDirpath,
                             const std::set<unsigned short> & selected_tiles_set,
                             std::vector<std::future<void>> & asyncs,
                             std::ofstream & f_fileslist )
{
    render_context.updateVertexBuffer(triangles);
    const unsigned short width = render_context.width;
    const unsigned short margin = render_context.texture_margin;
    const bool has_selected_tiles = (selected_tiles_set.size() > 0);
    const bool is_png = (render_context.image_type == RenderContext::ImageType::PNG);

    /* [textures_dir]/[base_name]-VDISP-[T|O]-[size]_[tile_n].[png|exr] */
    std::string fp_prefix = fmt::format( "{}/{}-VDISP-", outputDirpath, base_name );
    if (space == Space::tangent)
        fp_prefix += fmt::format("T-{}_", width);
    else
        fp_prefix += fmt::format("O-{}_", width);

    for (auto & tile : tiles)
    {
        const unsigned short tile_n = tile + 1001;
        if (has_selected_tiles && selected_tiles_set.count(tile_n) == 0)
            continue;
        std::string fp;

        if (is_png) {
            std::vector<PngPixel> pixels(width * width);
            render_context.renderTile(tile, pixels.data());
            fp = fmt::format("{}{}.png", fp_prefix, tile_n);
            asyncs.push_back(std::async( std::launch::async | std::launch::deferred,
                                         writePNG,
                                         width, margin,
                                         fp,
                                         std::move(pixels),
                                         png_zero_level ));
        } else {
            std::vector<ExrPixel> pixels(width * width);
            render_context.renderTile(tile, pixels.data());
            fp = fmt::format("{}{}.exr", fp_prefix, tile_n);
            asyncs.push_back(std::async( std::launch::async | std::launch::deferred,
                                         writeEXR,
                                         width, margin,
                                         fp,
                                         std::move(pixels) ));
        }
        createJson( fp + ".json", is_png, width, margin, tile_n );
        f_fileslist << fp + ";";
    }
}
