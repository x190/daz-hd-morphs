---------------------------------------------------
Modified and expanded from: https://github.com/edolstra/dhdm

---------------------------
Originally compiled with MinGW on Code::Blocks 20.03 in windows x64, with C++17 support
(https://www.codeblocks.org/downloads/binaries/).

Sources:

    Compiled libraries:
        glew32
            http://glew.sourceforge.net

        glfw3
            https://www.glfw.org

        fmt
            https://github.com/fmtlib/fmt

        libpng
            http://www.libpng.org/pub/png/libpng.html

        osdCPU
            https://graphics.pixar.com/opensubdiv/docs/cmake_build.html

        The iostreams and zlib part of boost:
            https://www.boost.org/doc/libs/1_75_0/more/getting_started/index.html

        The following libraries included with Code::Blocks MinGW compiler:
            glu32
            opengl32
            gdi32
            user32
            kernel32

    Headers only libraries:

        https://github.com/g-truc/glm
        https://github.com/nlohmann/json
        https://github.com/syoyo/tinyexr

---------------------------
