#include <iostream>
#include <fstream>
#include <future>

#include "main.hh"
#include "diff.hh"
#include "utils_vdisp.hh"
#include "utils.hh"

DLL_EXPORT int generate_disp_blender( const MeshInfo* mesh_info,
                                      const TextureInfo* texture_info,
                                      const char* base_name )
{
    try{
        const std::string outputDirpath(texture_info->output_dirpath);
        const std::string fp_filelist = outputDirpath + "/files_list.txt";
        std::ofstream f_fileslist( fp_filelist, std::ofstream::out | std::ofstream::trunc );
        if ( !f_fileslist )
            throw std::runtime_error("Can't open file \"" + fp_filelist + "\"");

        dhdm::gScale = 1;
        const std::string fp_obj_base = std::string(mesh_info->base_exportedf) + ".obj";
        const std::string fp_obj_hd = std::string(mesh_info->base_exportedf) + "_2.obj";

        dhdm::Mesh baseMesh = dhdm::Mesh::fromObj( fp_obj_base,
                                                   true,
                                                   false,
                                                   nullptr );

        dhdm::Mesh hdMesh = dhdm::Mesh::fromObj( fp_obj_hd,
                                                 false,
                                                 false,
                                                 nullptr );

        if ( mesh_info->hd_level > 0 )
            baseMesh.subdivide_simple( mesh_info->hd_level );

        if ( baseMesh.vertices.size() != hdMesh.vertices.size() )
        {
            std::cout << "ERROR: subdivided base mesh and hd mesh don't match.\n";
            return -1;
        }

        std::set<unsigned short> selected_tiles_set;
        load_tiles_set(selected_tiles_set, texture_info);

        std::vector<std::future<void>> asyncs;

        std::cout << "Triangulating...\n";
        baseMesh.triangulate();
        hdMesh.triangulate();

        MeshDiff::renderContextInit( texture_info->image_type,
                                     texture_info->texture_size,
                                     texture_info->texture_margin );

        MeshDiff diff( texture_info, baseMesh, hdMesh,
                       base_name );

        diff.writeToImage( outputDirpath, selected_tiles_set, asyncs, f_fileslist );
        MeshDiff::renderContextFinish();

        std::cout << "Waiting...\n";
        for (auto & async : asyncs)
            async.get();
        f_fileslist.close();
        return 0;
    } catch (std::exception & e) {
        std::cout << "-Error in DLL: " << e.what() << std::endl;
        return -1;
    }
}
