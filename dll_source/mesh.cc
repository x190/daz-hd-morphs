#include <optional>
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <glm/gtx/io.hpp>
#include <fmt/format.h>

#include "mesh.hh"
#include "utils.hh"


double dhdm::gScale = 0.01;


dhdm::DhdmFile::DhdmFile(std::string filepath) :
    filepath(filepath), baseVertexCount(0), geo_data(nullptr)
{
}

void dhdm::Mesh::triangulate()
{
    auto nrFaces = faces.size();
    faces.reserve(nrFaces * 2);
    for (size_t i = 0; i < nrFaces; ++i) {
        auto & face = faces[i];
        if (face.vertices.size() == 3) continue;
        // assert(face.vertices.size() == 4);
        faces.push_back(Face { .vertices = { face.vertices[2], face.vertices[3], face.vertices[0] } });
        face.vertices.pop_back();
    }
}

void dhdm::Mesh::set_subd_only_deltas(const dhdm::Mesh * originalMesh)
{
    this->originalMesh = originalMesh;
    subd_only_deltas = true;
}


bool dhdm::Mesh::load_geo_file(const char* geograft_file)
{
    if (geograft_file == nullptr)
        return false;
    const nlohmann::json j = readJSON(std::string(geograft_file));

    for (auto & e : j.items())
    {
        const int32_t nverts = std::stoi(e.key());
        const auto j2 = e.value();
        max_vcount = std::max(max_vcount, nverts);

        std::unordered_map<unsigned int, unsigned int> vmapping;
        for (auto & e2 : j2["vn"])
            vmapping[ e2[0] ] = e2[1];

        std::unordered_map<unsigned int, unsigned int> fmapping;
        for (auto & e2 : j2["fn"])
            fmapping[ e2[0] ] = e2[1];

        vertex_mapping[nverts] = std::move(vmapping);
        face_mapping[nverts] = std::move(fmapping);
    }

    has_geograft = true;
    return true;
}

void dhdm::Mesh::add_vertices_delta(const std::vector<dhdm::Vertex> & deltas)
{
    if (vertices.size() != deltas.size())
    {
        throw std::runtime_error(fmt::format("Vertex count mismatch: {} - {}",
                                            vertices.size(), deltas.size()));
    }
    for (size_t i = 0; i < vertices.size(); i++)
        vertices[i].pos += deltas[i].pos;
}
