#include <fstream>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <fmt/format.h>

#include "utils.hh"


nlohmann::json readJSON(const std::string & fp)
{
    auto fs = std::fstream(fp, std::fstream::in | std::fstream::binary);
    if (!fs)
        throw std::runtime_error("Can't open file");

    unsigned char magic[2];
    fs.read((char *) magic, sizeof(magic));
    fs.seekg(0, fs.beg);

    nlohmann::json json;

    if (magic[0] == 0x1f && magic[1] == 0x8b) {
        boost::iostreams::filtering_streambuf<boost::iostreams::input> in;
        in.push(boost::iostreams::gzip_decompressor());
        in.push(fs);
        std::istream str(&in);
        str >> json;
    } else {
        fs >> json;
    }

    fs.close();
    return json;
}


void load_morphs_group_info( std::vector<MorphFileInfo> & morphs_group_info,
                             const MorphsInfo* morphs_info,
                             const unsigned int group_number )
{
    for (size_t j=0; j < morphs_info->groups_counts[group_number]; j++)
    {
        const std::string morph_type(morphs_info->morphs_types[group_number][j]);
        const bool use_hd = (morph_type == "BOTH" || morph_type == "HD");
        const bool use_base = (morph_type == "BOTH" || morph_type == "BASE");

        morphs_group_info.push_back( { .weight = morphs_info->morphs_weights[group_number][j],
                                       .filepath = std::string(morphs_info->morphs_filepaths[group_number][j]),
                                       .use_base = use_base,
                                       .use_hd = use_hd
                                     } );
    }
}


void load_tiles_set( std::set<unsigned short> & selected_tiles_set,
                     const TextureInfo* texture_info )
{
    for (unsigned short i=0; i < texture_info->tiles_count; i++)
        selected_tiles_set.insert(texture_info->tiles[i]);
}


void applySubdivision( dhdm::Mesh & loadedBaseMesh,
                       const MorphsInfo* morphs_info,
                       const unsigned short hd_level )
{
    if (hd_level < 1) return;

    if (morphs_info->groups_n == 0 || morphs_info->groups_counts[0] == 0) {
        loadedBaseMesh.subdivide_simple(hd_level);
        //loadedBaseMesh.subdivide(hd_level, {}, -1);
    } else {
        std::vector<MorphFileInfo> morphs_group_info;
        load_morphs_group_info(morphs_group_info, morphs_info, 0);

        std::vector<dhdm::DhdmFile> dhdms;
        applyBaseMorphs(loadedBaseMesh, morphs_group_info, dhdms);

        loadedBaseMesh.subdivide(hd_level, dhdms, -1);
    }
}

unsigned short applyBaseMorphs( dhdm::Mesh & mesh,
                                std::vector<MorphFileInfo> & morphs_group_info,
                                std::vector<dhdm::DhdmFile> & dhdms )
{
    unsigned short level = 0;

    if (mesh.subd_only_deltas) {
        for (size_t i = 0; i < mesh.vertices.size(); i++)
            mesh.vertices[i].pos = glm::dvec3(0,0,0);
    }

    for (auto & mi : morphs_group_info) {
        dhdm::DhdmFile dhdm_file("");

        if (mi.filepath.size() >= 4 && std::string(mi.filepath, mi.filepath.size() - 4) == ".dsf")
        {
            if (auto dhdm_file2 = mesh.applyMorph(mi))
                dhdm_file = std::move(*dhdm_file2);
        }
        else
        {
            throw std::runtime_error(fmt::format("Invalid morph file: \"{}\"", mi.filepath));
        }

        if (dhdm_file.filepath.size() > 0) {
            dhdm_file.dhdm_data.reset( new Dhdm(dhdm_file.filepath) );
            dhdm_file.weight = mi.weight;
            level = std::max(level, (unsigned short) dhdm_file.dhdm_data->levels.size());
            dhdms.push_back( std::move(dhdm_file) );
        }
    }

    if (mesh.has_geograft)
    {
        for (auto & dhdm_file : dhdms) {
            if (mesh.face_mapping.count(dhdm_file.baseVertexCount) > 0)
                dhdm_file.geo_data = &(mesh.face_mapping[dhdm_file.baseVertexCount]);
        }
    }

    return level;
}

void print_vertex(std::vector<dhdm::Vertex> & vertices, const size_t i)
{
    std::cout << "  Vertex " << i << ": " << "(";
    std::cout << vertices[i].pos[0] << ", ";
    std::cout << vertices[i].pos[1] << ", ";
    std::cout << vertices[i].pos[2] << ")\n";
}

std::vector<std::string> split( const std::string & p, const std::string & d )
{
    std::vector<std::string> r;
    size_t i = 0;
    for (size_t j = p.find(d); j != std::string::npos; j = p.find(d, i))
    {
        r.emplace_back( p.begin() + i, p.begin() + j ) ;
        i = j + d.size();
    }
    if (i != p.size())
        r.emplace_back( p.begin() + i, p.end() );
    return r;
}

bool endswith ( const std::string & p, const std::string & suf ) {
    if ( suf.size() > p.size() )
        return false;
    return p.compare( p.size() - suf.size(), suf.size(), suf ) == 0;
}


std::vector<dhdm::Vertex> get_vertices_delta( const dhdm::Mesh & mesh0,
                                              const dhdm::Mesh & mesh1 )
{
    if (mesh0.vertices.size() != mesh1.vertices.size())
    {
        throw std::runtime_error(fmt::format("Vertex count mismatch: {} - {}",
                                             mesh0.vertices.size(), mesh1.vertices.size()));
    }
    std::vector<dhdm::Vertex> deltas;
    for (size_t i=0; i < mesh0.vertices.size(); i++)
        deltas.push_back({ .pos = mesh1.vertices[i].pos - mesh0.vertices[i].pos });
    return deltas;
}

