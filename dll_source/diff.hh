#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <future>
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtx/normal.hpp>
#include <nlohmann/json.hpp>

#include "mesh.hh"
#include "shared.hh"


struct MeshDiff
{
    public:
        MeshDiff( const TextureInfo* texture_info,
                  const dhdm::Mesh & baseMesh, const dhdm::Mesh & hdMesh,
                  const char* baseName );

        static void renderContextInit( const char image_type,
                                       const unsigned short width,
                                       const unsigned short margin );
        static void renderContextFinish();

        void writeToImage( const std::string & outputDirpath,
                           const std::set<unsigned short> & selected_tiles_set,
                           std::vector<std::future<void>> & asyncs,
                           std::ofstream & f_fileslist );

        void createJson( const std::string & fp, const bool is_png,
                         const unsigned short size, const unsigned short margin,
                         const unsigned short tile_n );

    private:
        struct GLVertex {
            float x, y;
            float r, g, b;
        };
        static_assert(sizeof(GLVertex) == sizeof(float)*5);

        struct GLTriangle {
            GLVertex v0, v1, v2;
        };
        //static_assert(sizeof(GLTriangle) == sizeof(GLVertex)*3);
        static_assert(sizeof(GLTriangle) == 4 * 5 * 3);

        enum class Space {tangent, object};

        class RenderContext
        {
            public:
                enum class ImageType {PNG, EXR};

                unsigned short width;
                ImageType image_type;
                unsigned short texture_margin;
                bool is_initialized;

                RenderContext();
                ~RenderContext();
                void initializeGL();
                void terminateGL();
                void renderTile(const unsigned short tile, GLvoid* data);
                void updateVertexBuffer(const std::vector<MeshDiff::GLTriangle>& triangles);

            private:
                bool is_GLinitialized;
                GLsizei num_elements;
                GLuint vao, vertexBuffer;
                GLuint shaderProgram, posAttrib, colAttrib, tileAttrib;
                GLuint frameBuffer, texColorBuffer, renderBuffer;
        };


        std::vector<GLTriangle> triangles;
        std::set<unsigned short> tiles;

        Space space;
        bool auto_settings;
        double scale, midlevel;
        const double scale_ratio;   // (Blender's scale)/(dll scale)
        bool out_of_bounds;
        double suggested_scale, suggested_midlevel;
        uint16_t png_zero_level;
        const std::string base_name;

        std::vector<glm::dmat3x3> tangent_mats;
        std::vector< std::vector<glm::dvec3> > triangles_disps;

        inline static RenderContext render_context;


        void recalculate_settings( const double maxDispl_negative, const double maxDispl_positive );

        void print_settings( const double maxDispl_negative, const double maxDispl_positive,
                             const double maxDispl_length );

        GLVertex toVertex(const glm::dvec3 & displ, const glm::dvec2 & uv);
        GLVertex toVertex2a(const glm::dvec2 & uv);
        void toVertex2b(GLVertex & v, const glm::dvec3 & displ);

        void calculateDiff( const dhdm::Mesh & baseMesh,
                            const dhdm::Mesh & hdMesh );
};
