#include <iostream>
#include <fstream>

#include "utils_vdisp.hh"


SubdivisionVdisp::SubdivisionVdisp(const bool create_subdividers, const dhdm::Mesh * baseMeshPointer) :
    create_subdividers(create_subdividers), baseMeshPointer(baseMeshPointer)
{
}


void SubdivisionVdisp::subdivide_simple(const unsigned int level, dhdm::Mesh & mesh)
{
    if (level < 1) return;

    if (!create_subdividers) {
        mesh.subdivide_simple(level);
        return;
    }

    auto it = subdividers.find(level);
    if (it == subdividers.end())
        subdividers.insert( { level,
                              std::unique_ptr<dhdm::MeshSubdivider>( new dhdm::MeshSubdivider(baseMeshPointer, level) )
                            } );
    subdividers[level] -> subdivide_simple(mesh);
}

void SubdivisionVdisp::subdivide( const unsigned int level, dhdm::Mesh & mesh,
                                  std::vector<dhdm::DhdmFile> & dhdms,
                                  const int apply_edits_until )
{
    if (level < 1) return;

    if (!create_subdividers) {
        mesh.subdivide(level, dhdms, apply_edits_until);
        return;
    }

    auto it = subdividers.find(level);
    if (it == subdividers.end())
        subdividers.insert( { level,
                              std::unique_ptr<dhdm::MeshSubdivider>( new dhdm::MeshSubdivider(baseMeshPointer, level) )
                            } );

    subdividers[level] -> subdivide(mesh, dhdms, apply_edits_until);
}

