#include <limits>
#include <cmath>
#include <iomanip>
#include <fmt/format.h>
#include <nlohmann/json.hpp>

#include "diff.hh"
#include "mesh.hh"


constexpr double clamp(const double d) {
    return (d < 0.0)? 0 : ((d > 1.0)? 1 : d);
}

std::vector<glm::dmat3x3> calculate_tangent_mats(const dhdm::Mesh & baseMesh)
{
    /* Compute base mesh vertex normals, tangents and bitangents. */
    std::vector<glm::dvec3> vertexNormals(baseMesh.vertices.size(), glm::dvec3(0.0, 0.0, 0.0));
    std::vector<glm::dvec3> vertexTangents(baseMesh.vertices.size(), glm::dvec3(0.0, 0.0, 0.0));
    std::vector<glm::dvec3> vertexBitangents(baseMesh.vertices.size(), glm::dvec3(0.0, 0.0, 0.0));

    const std::vector<dhdm::UV> & uv_layer = baseMesh.uv_layers[0];

    for (dhdm::FaceId faceId = 0; faceId < baseMesh.faces.size(); ++faceId) {
        auto & face = baseMesh.faces[faceId];
        // assert(face.vertices.size() == 3);

        auto vi0 = face.vertices[0].vertex;
        auto vi1 = face.vertices[1].vertex;
        auto vi2 = face.vertices[2].vertex;

        auto & v0 = baseMesh.vertices[vi0].pos;
        auto & v1 = baseMesh.vertices[vi1].pos;
        auto & v2 = baseMesh.vertices[vi2].pos;

        auto normal = glm::triangleNormal(v0, v1, v2);
        vertexNormals[vi0] += normal;
        vertexNormals[vi1] += normal;
        vertexNormals[vi2] += normal;

        auto & uv0 = uv_layer[face.vertices[0].uv].pos;
        auto & uv1 = uv_layer[face.vertices[1].uv].pos;
        auto & uv2 = uv_layer[face.vertices[2].uv].pos;

        auto deltaPos1 = v1 - v0;
        auto deltaPos2 = v2 - v0;
        auto deltaUV1 = uv1 - uv0;
        auto deltaUV2 = uv2 - uv0;

        auto r = 1.0 / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);

        auto tangent = (deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y) * r;
        vertexTangents[vi0] += tangent;
        vertexTangents[vi1] += tangent;
        vertexTangents[vi2] += tangent;

        auto bitangent = (deltaPos2 * deltaUV1.x - deltaPos1 * deltaUV2.x) * r;
        vertexBitangents[vi0] += bitangent;
        vertexBitangents[vi1] += bitangent;
        vertexBitangents[vi2] += bitangent;
    }

    for (auto & v : vertexNormals)
        v = glm::normalize(v);

    for (auto & v : vertexTangents)
        v = glm::normalize(v);

    for (auto & v : vertexBitangents)
        v = glm::normalize(v);

    std::vector<glm::dmat3x3> mats( baseMesh.vertices.size(), glm::dmat3x3(0.0) );

    std::set<unsigned int> calculated_vids;

    for (dhdm::FaceId faceId = 0; faceId < baseMesh.faces.size(); ++faceId) {
        auto & face = baseMesh.faces[faceId];

        for (unsigned short i=0; i<3; i++) {
            auto vi = face.vertices[i].vertex;
            if (calculated_vids.count(vi) > 0)
                continue;
            calculated_vids.insert(vi);
            glm::dvec3 & tangent = vertexTangents[vi];
            glm::dvec3 & bitangent = vertexBitangents[vi];
            glm::dvec3 & normal = vertexNormals[vi];

            // Gram-Schmidt orthogonalization.
            tangent = glm::normalize(tangent - normal * glm::dot(normal, tangent));
            bitangent = glm::normalize(bitangent - normal * glm::dot(normal, bitangent) - tangent * glm::dot(tangent, bitangent));

            if (glm::dot(glm::cross(normal, tangent), bitangent) < 0.0)
                tangent = tangent * -1.0;

            // Convert displacement from object to normal space.
            mats[vi] = glm::dmat3x3(
                tangent.x, bitangent.x, normal.x,   // first column
                tangent.y, bitangent.y, normal.y,   // second column
                tangent.z, bitangent.z, normal.z    // third column
            );
        }
    }

    return mats;
}

MeshDiff::MeshDiff( const TextureInfo* texture_info,
                    const dhdm::Mesh & baseMesh, const dhdm::Mesh & hdMesh,
                    const char* baseName )
                    : auto_settings(texture_info->auto_settings),
                      scale(texture_info->scale),
                      midlevel(texture_info->midlevel),
                      scale_ratio(dhdm::gScale),
                      base_name(std::string(baseName))
{
    if (!render_context.is_initialized)
        throw std::runtime_error("RenderContext is not initialized");

    assert(baseMesh.vertices.size() == hdMesh.vertices.size());
    assert(baseMesh.faces.size() == hdMesh.faces.size());

    switch(texture_info->target_space) {
        case 'T': { space = Space::tangent;
                    tangent_mats = calculate_tangent_mats(baseMesh);
                    break;
                  }
        case 'O': { space = Space::object;
                    break;
                  }
        default:  { throw std::runtime_error(fmt::format("Invalid target_space: '{}'", texture_info->target_space));
                    break;
                  }
    }

    if (render_context.image_type == RenderContext::ImageType::EXR) {
        this -> auto_settings = false;
        this -> midlevel = 0;
        this -> scale = 1;
    }

    calculateDiff(baseMesh, hdMesh);
    tangent_mats.clear();
}


void MeshDiff::recalculate_settings( const double maxDispl_negative, const double maxDispl_positive )
{
    const double margin = 1 - 0.9888;
    const double t = maxDispl_positive - maxDispl_negative;
    if (t > 1e-6) {
        scale = (1.0-margin) / t;
        midlevel = -maxDispl_negative * scale + (margin*0.5);
    }
    else{
        scale = 1;
        midlevel = 0.5;
    }

    for (unsigned int i=0; i < triangles.size(); i++) {
        auto &triangle = triangles[i];
        const auto & tri_disps = triangles_disps[i];

        toVertex2b(triangle.v0, tri_disps[0]);
        toVertex2b(triangle.v1, tri_disps[1]);
        toVertex2b(triangle.v2, tri_disps[2]);
    }
    triangles_disps.clear();
}


void MeshDiff::print_settings( const double maxDispl_negative, const double maxDispl_positive,
                               const double maxDispl_length )
{
    const double t = maxDispl_positive - maxDispl_negative;
    if (t > 1e-6) {
        suggested_scale = 1.0 / t;
        suggested_midlevel = -maxDispl_negative * suggested_scale;
    }
    else{
        suggested_scale = std::numeric_limits<double>::infinity();
        suggested_midlevel = 0.5;
    }

    double suggested_scale_05 = std::numeric_limits<double>::infinity();
    if (maxDispl_positive > 0.0)
        suggested_scale_05 = 0.5/maxDispl_positive;
    if (maxDispl_negative < 0.0)
        suggested_scale_05 = std::min(suggested_scale_05, -0.5/maxDispl_negative);

    if (render_context.image_type == RenderContext::ImageType::PNG) {
        double zero_pixel_val;
        if (maxDispl_positive > 0 && maxDispl_negative < 0) {
            zero_pixel_val = midlevel;
        }
        else {
            zero_pixel_val = std::min(abs(maxDispl_negative), abs(maxDispl_positive));
            zero_pixel_val = zero_pixel_val * scale + midlevel;
        }
        png_zero_level = (uint16_t) std::round( clamp(zero_pixel_val) * 65535 );
    }

    const double max_pixel_val = maxDispl_positive * scale + midlevel;
    const double min_pixel_val = maxDispl_negative * scale + midlevel;
    out_of_bounds = (max_pixel_val > 1.0 || min_pixel_val < 0.0);

    std::cout << "--------\n";
    std::cout << fmt::format("--Maximum 3d displacement length (in Blender units): {}\n", maxDispl_length * scale_ratio);
    if (auto_settings)
        std::cout << fmt::format("--Auto set operator midlevel and scale: ({}, {})\n", midlevel, scale);
    else
        std::cout << fmt::format("--Operator midlevel and scale: ({}, {})\n", midlevel, scale);

    std::cout << fmt::format("--Largest pixel component value: {}\n", max_pixel_val);
    std::cout << fmt::format("--Smallest pixel component value: {}\n", min_pixel_val);
    if (out_of_bounds && (render_context.image_type == RenderContext::ImageType::PNG)) {
        std::cout << "--[WARNING] 16-bit PNG OUT OF BOUNDS: "
                     "It is recommended to re-run the operator using recommended settings below.\n";
    }

    std::cout << "--Settings for Vector Displacement node in Blender:\n";
    if (render_context.image_type == RenderContext::ImageType::PNG)
        std::cout << "  16-bit PNG:\n";
    else
        std::cout << "  32-bit EXR:\n";
    std::cout << fmt::format("    midlevel = {}\n", midlevel);
    std::cout << fmt::format("    scale = {}\n", scale_ratio/scale);

    std::cout << "--Operator settings for 16-bit PNG to maximize range of pixel values:\n";
    std::cout << fmt::format("  Scale with a fixed midlevel of 0.5: {}\n", suggested_scale_05);
    std::cout << fmt::format("  Midlevel and scale: ({}; {})\n", suggested_midlevel, suggested_scale);
    std::cout << "--------\n";
}

void MeshDiff::createJson( const std::string & fp, const bool is_png,
                           const unsigned short size, const unsigned short margin,
                           const unsigned short tile_n )
{
    std::ofstream f (fp, std::ofstream::out | std::ofstream::trunc );
    if (!f)
        throw std::runtime_error("Can't open file \"" + fp + "\"");

    std::cout << fmt::format("Writing '{}'...\n", fp);
    nlohmann::json j;

    j["id"] = base_name;
    j["format"] = is_png ? "png" : "exr";
    j["size"] = size;
    j["space"] = (space == Space::tangent) ? "tangent" : "object";
    j["margin"] = margin;
    j["tile"] = tile_n;
    j["blender_settings"] = {midlevel, scale_ratio/scale};
    if (is_png)
    {
        if (out_of_bounds)
        {
            j["png_out_of_bounds"] = true;
            j["png_op_settings_suggested"] = {suggested_midlevel, suggested_scale};
        }
        else
        {
            j["png_out_of_bounds"] = false;
        }
    }

    f << std::setw(4) << j << std::endl;
    f.close();
}

void MeshDiff::toVertex2b(GLVertex & v, const glm::dvec3 & displ)
{
    const float dx = (float) displ.x * scale + midlevel;
    const float dy = (float) displ.y * scale + midlevel;
    const float dz = (float) displ.z * scale + midlevel;

    if (space == Space::tangent) {
        // Blender's displacement in tangent space:
        // (r, g, b) == (tan, normal, bitan)
        v.r = dx;
        v.g = dz;
        v.b = dy;
    }
    else {
        // Blender's displacement in modifier (object space):
        // (r, g, b) == (x, y, z)
        v.r = dx;
        v.g = dy;
        v.b = dz;
    }
}

MeshDiff::GLVertex MeshDiff::toVertex2a(const glm::dvec2 & uv) {
    GLVertex v;
    v.x = (float) uv.x;
    v.y = (float) uv.y;
    return v;
}

MeshDiff::GLVertex MeshDiff::toVertex(const glm::dvec3 & displ, const glm::dvec2 & uv) {
    GLVertex v;
    v.x = (float) uv.x;
    v.y = (float) uv.y;

    const float dx = (float) displ.x * scale + midlevel;
    const float dy = (float) displ.y * scale + midlevel;
    const float dz = (float) displ.z * scale + midlevel;

    if (space == Space::tangent) {
        // Blender's displacement in tangent space:
        // (r, g, b) == (tan, normal, bitan)
        v.r = dx;
        v.g = dz;
        v.b = dy;
    }
    else {
        // Blender's displacement in modifier (object space):
        // (r, g, b) == (x, y, z)
        v.r = dx;
        v.g = dy;
        v.b = dz;
    }
    return v;
}


void MeshDiff::calculateDiff( const dhdm::Mesh & baseMesh,
                              const dhdm::Mesh & hdMesh )
{
    double maxDispl_positive = 0.0;
    double maxDispl_negative = 0.0;
    double maxDispl_length = 0.0;

    auto updateDispl = [&](glm::dvec3 & displ, const glm::dvec2 & uv)
    {
        const auto length = glm::length(displ);
        maxDispl_length = std::max( maxDispl_length, length );

        const auto maxp = std::max(displ.x, std::max(displ.y, displ.z));
        maxDispl_positive = std::max(maxDispl_positive, maxp);

        const auto maxn = std::min(displ.x, std::min(displ.y, displ.z));
        maxDispl_negative = std::min(maxDispl_negative, maxn);

        if (length > 1e-4) {
            unsigned short tile = uv.x / 1.0;
            tiles.insert(tile);
        }
        else{
            displ *= 0.0;
        }
    };

    const std::vector<dhdm::UV> & uv_layer = baseMesh.uv_layers[0];

    for (dhdm::FaceId faceId = 0; faceId < baseMesh.faces.size(); ++faceId) {
        const auto & face = baseMesh.faces[faceId];
        const auto vi0 = face.vertices[0].vertex;
        const auto vi1 = face.vertices[1].vertex;
        const auto vi2 = face.vertices[2].vertex;

        const auto uv0 = uv_layer[ face.vertices[0].uv ].pos;
        const auto uv1 = uv_layer[ face.vertices[1].uv ].pos;
        const auto uv2 = uv_layer[ face.vertices[2].uv ].pos;

        /* ------------------ */
        /* Compute the object space displacements. */
        /*
        auto & face2 = hdMesh.faces[faceId];
        // assert(face2.vertices.size() == 3);
        auto v2i0 = face2.vertices[0].vertex;
        auto v2i1 = face2.vertices[1].vertex;
        auto v2i2 = face2.vertices[2].vertex;

        glm::dvec3 d0 = hdMesh.vertices[v2i0].pos - baseMesh.vertices[vi0].pos);
        glm::dvec3 d1 = hdMesh.vertices[v2i1].pos - baseMesh.vertices[vi1].pos);
        glm::dvec3 d2 = hdMesh.vertices[v2i2].pos - baseMesh.vertices[vi2].pos);
        */
        /* ------- */

        glm::dvec3 d0 = hdMesh.vertices[vi0].pos - baseMesh.vertices[vi0].pos;
        glm::dvec3 d1 = hdMesh.vertices[vi1].pos - baseMesh.vertices[vi1].pos;
        glm::dvec3 d2 = hdMesh.vertices[vi2].pos - baseMesh.vertices[vi2].pos;
        /* ------------------ */

        if (space == Space::tangent) {
            d0 = tangent_mats[vi0] * d0;
            d1 = tangent_mats[vi1] * d1;
            d2 = tangent_mats[vi2] * d2;
        }

        updateDispl(d0, uv0);
        updateDispl(d1, uv1);
        updateDispl(d2, uv2);

        if (auto_settings) {
            triangles_disps.push_back( {d0, d1, d2} );
            triangles.push_back( GLTriangle { .v0 = toVertex2a(uv0),
                                              .v1 = toVertex2a(uv1),
                                              .v2 = toVertex2a(uv2) } );
        }
        else {
            triangles.push_back( GLTriangle { .v0 = toVertex(d0, uv0),
                                              .v1 = toVertex(d1, uv1),
                                              .v2 = toVertex(d2, uv2) } );
        }
    }

    if (auto_settings)
        recalculate_settings(maxDispl_negative, maxDispl_positive);
    print_settings(maxDispl_negative, maxDispl_positive, maxDispl_length);
}
